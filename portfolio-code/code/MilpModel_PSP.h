#pragma once

#include "Common.h"
#include "Instance_PSP.h"
#include "MyCallback_PSP.h"

//Gurobi status
//https://www.gurobi.com/documentation/9.0/refman/optimization_status_codes.html#sec:StatusCodes

class MilpModel_PSP {

	//ATTRIBUTES
	//Model variables
	Instance_PSP* instance;
	MyCallback_PSP* myCallback;
	GRBEnv* environment;
	GRBModel* model;

	//Model variables
	vector<GRBVar> x;
	vector<GRBVar> y;
	GRBVar q;

	//Constraints variables
	vector<GRBConstr> quantileConstraints;
	vector<GRBConstr> strongVI;
	GRBConstr probabilityConstraint;
	GRBConstr upperObjectiveConstraint;

	//Counters
	int strongViCounter;

	//Gurobi stats
	int gurobiStatus;
	int modelBaBNodes;
	double modelObjVal;
	double modelObjBound;
	double modelMipGap;
	double modelTime;

public:

	////CONSTRUCTORS AND DESTRUCTORS////
	MilpModel_PSP(Instance_PSP* instance);
	MilpModel_PSP(const MilpModel_PSP& milpModel);
	~MilpModel_PSP();

	/**
	* create Gurobi environment and model
	* initialize Gurobi parameters
	* build variables, objective, constraints
	*/
	void buildModel();

	/**
	* add x, y and q variables
	*/
	void buildXvariables();
	void buildYvariables();
	void buildQvariable();
	/**
	* remove all y variables
	*/
	void removeYvariables();

	/**
	* add objective
	*/
	void buildObjective();

	/**
	* all wealth is allocated to securities
	*/
	void buildBudgetConstraint();
	/**
	* impose min ptf return
	*/
	void buildMinReturnConstraint();
	/**
	* quantile constraints
	*/
	void buildQuantileConstraints();
	/**
	* probabilities constraints
	*/
	void buildProbabilityConstraint();
	/**
	* if reduced is on, add strong VI if there is a cluster with probability strictly greater than 1-tau
	*/
	void buildStrongVI();
	/**
	* build constraint for upper bounding the objective
	*/
	void buildUpperObjectiveConstraint(double bound);
	/**
	* build variables and constraints needed for risk computation
	*/
	void buildRiskComputation();
	/**
	* remove quantile constraints
	*/
	void removeQuantileConstraints();
	/**
	* remove probabilities constraints
	*/
	void removeProbabilityConstraint();
	/**
	* remove probabilities constraints
	*/
	void removeStrongVI();
	/**
	* remove constraint for upper bounding the objective
	*/
	void removeUpperObjectiveConstraint();
	/**
	* remove variables and constraints needed for risk computation
	*/
	void removeRiskComputation();

	/**
	* get the values of the variables
	*/
	vector<double> getXValues();
	vector<double> getYValues();
	double getQValues();

	/**
	* create the callback obj
	*/
	void initializeCallback();

	/**
	* warm start variables
	*/
	void warmStartXVariable(const vector<double>& xValues);
	void warmStartYVariable(const vector<double>& yValues);
	void warmStartQVariable(const double& qValue);

	//Solve, write and extract attributes of the model
	void solveModel(const double& timeLimit, const double& gap, const bool& isLogFilePrint);
	void writeModel(string name);

	/**
	* extract model status, obj, bound, time
	*/
	void extractSolutionAttributes();

	/**
	* @return a string with the solution
	*/
	string solutionPrinting();

	////GET-SET ATTRIBUTES////
	int getGurobiStatus();
	int getModelBaBNodes();
	double getModelObjectiveBound();
	double getModelObjectiveValue();
	double getModelMipGap();
	double getModelTime();

};
#pragma once
