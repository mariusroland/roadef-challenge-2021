#include "MilpModel_PSP.h"

///////////////////////CONSTRUCTORS AND DESTRUCTORS//////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////

MilpModel_PSP::MilpModel_PSP(Instance_PSP* instance) {

	this->instance = instance;
	this->environment = nullptr;
	this->model = nullptr;
	this->myCallback = nullptr;
	this->gurobiStatus = -1;
	this->modelBaBNodes = -1;
	this->modelObjVal = -1;
	this->modelObjBound = -1;
	this->modelMipGap = -1;
	this->modelTime = -1;
	this->strongViCounter = 0;

}

MilpModel_PSP::MilpModel_PSP(const MilpModel_PSP& milpModel) {

	this->instance = milpModel.instance;
	this->environment = milpModel.environment;
	this->model = milpModel.model;
	this->x = milpModel.x;
	this->y = milpModel.y;
	this->q = milpModel.q;
	this->quantileConstraints = milpModel.quantileConstraints;
	this->probabilityConstraint = milpModel.probabilityConstraint;
	this->gurobiStatus = milpModel.gurobiStatus;
	this->modelObjVal = milpModel.modelObjVal;
	this->modelObjBound = milpModel.modelObjBound;
	this->modelMipGap = milpModel.modelMipGap;
	this->modelTime = milpModel.modelTime;

}

MilpModel_PSP::~MilpModel_PSP() {
	delete this->environment;
	delete this->model;
	delete this->myCallback;
}

//////////////////////////////METHODS///////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////solveRMP////////////////////////////////////////////////////

void MilpModel_PSP::buildModel() {

	//Gurobi environment, model and solver definition
	this->environment = new GRBEnv();
	this->model = new GRBModel(*environment);

	//Computational parameters
	this->model->set(GRB_DoubleParam_NodefileStart, 0.5);
	if (!this->instance->getIsNoVI()) {
		this->model->set(GRB_IntParam_LazyConstraints, 1);
		//this->model->set(GRB_IntParam_DualReductions, 0);
	}

	//VARIABLES INITIALIZATION
	this->buildXvariables();
	this->buildYvariables();
	this->buildQvariable();

	this->buildObjective();

	this->buildBudgetConstraint();
	this->buildMinReturnConstraint();
	this->buildProbabilityConstraint();
	this->buildQuantileConstraints();

	if (this->instance->getIsReduced()){
		this->buildStrongVI();
	}
	
}

void MilpModel_PSP::buildXvariables() {

	this->x = vector<GRBVar>(this->instance->getAssetNumber());
	for (int i = 0; i < this->instance->getAssetNumber(); i++) {

		string name = "x[" + to_string(i) + "]";
		this->x.at(i) = this->model->addVar(0, GRB_INFINITY, 0, GRB_CONTINUOUS, name);

	}

}

void MilpModel_PSP::buildYvariables() {

  this->y = vector<GRBVar>(this->instance->getScenarioNumber());
  for (int i = 0; i < this->instance->getScenarioNumber(); i++) {
    string name = "y[" + to_string(i) + "]";
    this->y.at(i) = this->model->addVar(0, 1, 0, GRB_BINARY, name);
    
  }

}

void MilpModel_PSP::buildQvariable() {

  string name = "q";
  this->q = this->model->addVar(0, GRB_INFINITY, 0, GRB_CONTINUOUS, name);

}

void MilpModel_PSP::removeYvariables() {

	for (int i = 0; i < this->y.size(); i++) {
		this->model->remove(this->y.at(i));
	}
	this->y.clear();

}

void MilpModel_PSP::buildObjective() {

	GRBLinExpr firstTerm = 0;
	double constant1 = this->instance->getAlpha();
	double constant2 = (1 - this->instance->getAlpha());

	//cout << "FIRTS TERM\n";
	for (int i = 0; i < this->instance->getAssetNumber(); i = i + 1) {
		for (int t = 0; t < this->instance->getScenarioNumber(true); t++) {
			firstTerm += this->instance->getProbabilities(true).at(t) * this->instance->getReturns(true).at(i).at(t) * this->x.at(i);
		}
	}

	firstTerm *= constant1;
	firstTerm += constant2 * this->q;
	this->model->setObjective(firstTerm, GRB_MAXIMIZE);

}

void MilpModel_PSP::buildBudgetConstraint() {

	string str = "budget";
	GRBLinExpr constraint = 0;
	for (int i = 0; i < this->instance->getAssetNumber(); i++) {
		constraint += this->x.at(i);
	}
	this->model->addConstr(constraint == 1, str);

}

void MilpModel_PSP::buildMinReturnConstraint() {

	string str = "portfolioReturn";
	GRBLinExpr constraint = 0;
	for (int i = 0; i < this->instance->getAssetNumber(); i++) {
		constraint += this->instance->getExpectedReturns().at(i) * this->x.at(i);
	}
	this->model->addConstr(constraint >= this->instance->getRho(), str);

}

void MilpModel_PSP::buildQuantileConstraints() {

	string str;
	this->quantileConstraints = vector<GRBConstr>(this->instance->getScenarioNumber(), GRBConstr());

	for (int j = 0; j < this->instance->getScenarioNumber(); j++) {

		str = "quantile_" + to_string(j);
		GRBLinExpr constraint = this->q;
		for (int i = 0; i < this->instance->getAssetNumber(); i++) {
			constraint -= this->instance->getReturns().at(i).at(j) * this->x.at(i);
		}
		constraint -= this->instance->getBigMs().at(j) * this->y.at(j);
		this->quantileConstraints.at(j) = this->model->addConstr(constraint <= 0, str);

	}

}

void MilpModel_PSP::buildProbabilityConstraint() {

	string str = "probability";
	GRBLinExpr constraint = 0;

	for (int j = 0; j < this->instance->getScenarioNumber(); j++) {
		constraint += this->instance->getProbabilities().at(j) * this->y.at(j);
	}

	this->probabilityConstraint = this->model->addConstr(constraint <= this->instance->getTau(), str);

}

void MilpModel_PSP::buildStrongVI() {

	string str;
	for (unordered_map<int, vector<double>>::const_iterator it = this->instance->getStrongVICoefficients().begin(); it != this->instance->getStrongVICoefficients().end(); it++) {
		str = "strongVI_" + to_string(this->strongViCounter);
		GRBLinExpr constraint = 0;
		constraint += (this->instance->getProbabilities().at(it->first) - this->instance->getTau()) * this->q;
		for (int i = 0; i < this->instance->getAssetNumber(); i++) {
			constraint -= it->second.at(i) * this->x.at(i);
		}
		this->strongVI.push_back(this->model->addConstr(constraint <= 0, str));
		this->strongViCounter++;
	}

}

void MilpModel_PSP::buildUpperObjectiveConstraint(double bound) {

	GRBLinExpr firstTerm = 0;
	string nameConstraint = "upperObjective";
	double constant1 = this->instance->getAlpha();
	double constant2 = (1 - this->instance->getAlpha());

	//cout << "FIRTS TERM\n";
	for (int i = 0; i < this->instance->getAssetNumber(); i = i + 1) {
		for (int t = 0; t < this->instance->getScenarioNumber(true); t++) {
			firstTerm += this->instance->getProbabilities(true).at(t) * this->instance->getReturns(true).at(i).at(t) * this->x.at(i);
		}
	}

	firstTerm *= constant1;
	firstTerm += constant2 * this->q;
	this->upperObjectiveConstraint = this->model->addConstr(firstTerm <= bound+epsilonValue, nameConstraint);

}

void MilpModel_PSP::buildRiskComputation() {

	this->buildYvariables();
	this->buildProbabilityConstraint();
	this->buildQuantileConstraints();
	this->buildStrongVI();
}


void MilpModel_PSP::removeQuantileConstraints() {

	for (int i = 0; i < this->quantileConstraints.size(); i++) {
		this->model->remove(this->quantileConstraints.at(i));
	}
	this->quantileConstraints.clear();

}

void MilpModel_PSP::removeProbabilityConstraint() {

	this->model->remove(this->probabilityConstraint);

}

void MilpModel_PSP::removeStrongVI() {

	for (int i = 0; i < this->strongVI.size(); i++) {
		this->model->remove(this->strongVI.at(i));
	}
	this->strongVI.clear();

}

void MilpModel_PSP::removeUpperObjectiveConstraint() {

	this->model->remove(this->upperObjectiveConstraint);	

}

void MilpModel_PSP::removeRiskComputation() {

	this->removeYvariables();
	this->removeProbabilityConstraint();
	this->removeQuantileConstraints();
	// this->removeStrongVI();
}

vector<double> MilpModel_PSP::getXValues() {

	vector<double> xValues(this->instance->getAssetNumber());
	for (int i = 0; i < this->instance->getAssetNumber(); i++) {
		xValues.at(i) = this->x.at(i).get(GRB_DoubleAttr_X);
	}
	return xValues;

}

vector<double> MilpModel_PSP::getYValues() {

	vector<double> yValues(this->instance->getScenarioNumber());
	for (int i = 0; i < this->instance->getScenarioNumber(); i++) {
		yValues.at(i) = this->y.at(i).get(GRB_DoubleAttr_X);
	}
	return yValues;

}

double MilpModel_PSP::getQValues() {

	return this->q.get(GRB_DoubleAttr_X);

}

void MilpModel_PSP::initializeCallback() {

	this->myCallback = new MyCallback_PSP(this->instance, *this->model, this->x, this->y, this->q);
	this->model->setCallback(this->myCallback);

}

void MilpModel_PSP::warmStartXVariable(const vector<double>& xValues) {

	for (int i = 0; i < xValues.size(); i++) {
		this->x.at(i).set(GRB_DoubleAttr_Start, xValues.at(i));
	}

}

void MilpModel_PSP::warmStartYVariable(const vector<double>& yValues) {

	for (int i = 0; i < this->instance->getScenarioNumber(); i++) {
		this->y.at(i).set(GRB_DoubleAttr_Start, yValues.at(i));
	}

}

void MilpModel_PSP::warmStartQVariable(const double& qValue) {

	this->q.set(GRB_DoubleAttr_Start, qValue);

}

void MilpModel_PSP::solveModel(const double& timeLimit, const double& gap, const bool& isLogFilePrint) {

	//Parameters
	this->model->set(GRB_DoubleParam_TimeLimit, timeLimit);
	this->model->set(GRB_DoubleParam_MIPGap, gap);

	//Solve
	if (isLogFilePrint) {
		string logFileName = "Output_" + this->instance->getName() + "_TAU_" + to_string(this->instance->getTau()) + "_ALPHA_" + to_string(this->instance->getAlpha()) + "_" + to_string(this->instance->getAreWeakVI()) + "_" + to_string(this->instance->getAreStrongVI()) + ".txt";
		this->model->set(GRB_StringParam_LogFile, logFileName);
		//this->writeModel(this->instance->getName() + ".lp");
	}
	this->model->optimize();

	/*cout << "MILP Q " << this->getQValues() << endl;
	cout << "REC Q " << this->instance->computeRealQuantile(this->getXValues()) << endl;
	cout << "REC OBJ " << this->instance->computeRealObjective(this->getXValues()) << endl;*/

	if (isLogFilePrint) {
		string logFileName = "Output_" + this->instance->getName() + "_TAU_" + to_string(this->instance->getTau()) + "_ALPHA_" + to_string(this->instance->getAlpha()) + "_" + to_string(this->instance->getAreWeakVI()) + "_" + to_string(this->instance->getAreStrongVI()) + ".txt";
		string ss = solutionPrinting();
		ofstream logFile;
		logFile.open(logFileName, ofstream::out | ofstream::app);
		logFile << "\n\n\n" << ss;
		logFile.close();
	}

	

}

void MilpModel_PSP::writeModel(string name) {
	this->model->write(name);
}

void MilpModel_PSP::extractSolutionAttributes() {

	this->gurobiStatus = this->model->get(GRB_IntAttr_Status);
	this->modelBaBNodes = this->model->get(GRB_DoubleAttr_NodeCount);
	this->modelObjVal = this->model->get(GRB_DoubleAttr_ObjVal);
	this->modelObjBound = this->model->get(GRB_DoubleAttr_ObjBound);
	this->modelMipGap = this->model->get(GRB_DoubleAttr_MIPGap) * 100;
	this->modelTime = this->model->get(GRB_DoubleAttr_Runtime);

}




///////////////////////////SOLUTION PRINTING///////////////////////////////////////////////////

string MilpModel_PSP::solutionPrinting() {

	string ss;
	ss += "\n\nQUANTILE PTF MODEL\nPORTFOLIO COMPOSITION:\n";
	for (int i = 0; i < this->instance->getAssetNumber(); i++) {
		if (this->x.at(i).get(GRB_DoubleAttr_X) > epsilonValue) {
			ss.append("ASSET: " + to_string(i) + "\tVALUE: " + to_string(this->x.at(i).get(GRB_DoubleAttr_X)) + "\tMEAN RETURN: " + to_string(this->instance->getExpectedReturns().at(i)) + "\n");
		}
	}

	int scenariosToDisactivate = ceil((1.0 - this->instance->getTau()) * static_cast<double>(this->instance->getScenarioNumber()));
	ss.append("TAU: " + to_string(this->instance->getTau()) + "\t#DEACTIVATED SCENARIOS: " + to_string(scenariosToDisactivate) + "\tSCENARIO SELECTION:\n");
	for (int i = 0; i < this->instance->getScenarioNumber(); i++) {
		if (this->y.at(i).get(GRB_DoubleAttr_X) > epsilonValue) {
			ss.append("SCENARIO: " + to_string(i) + "\tVALUE: " + to_string(this->y.at(i).get(GRB_DoubleAttr_X)) + "\n");
		}
	}

	double ret = 0.0;
	for (int i = 0; i < this->instance->getAssetNumber(); i++) {
		ret += this->instance->getExpectedReturns().at(i) * this->x.at(i).get(GRB_DoubleAttr_X);
	}
	ss.append("RETURN: " + to_string(ret) + "\tTHRESHOLD: " + to_string(this->instance->getRho()) + "\n");
	ss.append("QUANTILE:\t" + to_string(this->q.get(GRB_DoubleAttr_X)) + "\n");
	ss.append("OBJECTIVE VALUE:\t" + to_string(this->model->get(GRB_DoubleAttr_ObjVal)) + "\n");

	return ss;

}

//////////////////////////GET-SET ATTRIBUTES/////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////


////////////////////////////////GET/////////////////////////////////////////////////////

int MilpModel_PSP::getGurobiStatus() {

	return this->gurobiStatus;

}

int MilpModel_PSP::getModelBaBNodes() {

	return this->modelBaBNodes;

}

double MilpModel_PSP::getModelObjectiveValue() {

	return this->modelObjVal;

}

double MilpModel_PSP::getModelObjectiveBound() {

	return this->modelObjBound;

}

double MilpModel_PSP::getModelMipGap() {

	return this->modelMipGap;

}

double MilpModel_PSP::getModelTime() {

	return this->modelTime;

}

////////////////////////////////SET/////////////////////////////////////////////////////

