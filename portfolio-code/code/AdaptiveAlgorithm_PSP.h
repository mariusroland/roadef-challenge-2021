#pragma once

#include "Common.h"
#include "Instance_PSP.h"
#include "MilpModel_PSP.h"

using namespace std;

class AdaptiveAlgorithm_PSP {

private:

	//ATTRIBUTES
	
	//Instance and milpmodel used through the algorithm
	Instance_PSP* instance;
	MilpModel_PSP* milpModel;

	//Clustering information
	int clusterNumber;
	vector<int> clusteredScenarios;

	//Solutions
	vector<double> xNew;
	vector<double> xLB;
	vector<double> xUB;
	double vLB;
	double vUB;

	//Computation Criteria
	double timeLimit;
	double gap;

	//Algorithm information
	double adaptiveTime;

public:

	//CONSTRUCTORS AND DESTRUCTORS
	AdaptiveAlgorithm_PSP(Instance_PSP* instance, double timeLimit);
	~AdaptiveAlgorithm_PSP();
	
	//METHODS
	void initializeFirstModel();
	void initializeNextClustering();
	double kernelFunction(vector<int>& scenarioRepartition, vector<double>& realReturnPerScenario, double bandwidth, double x);
	void splitCluster(int clusterIndex, vector<int>& scenarioRepartition, vector<double>& realReturnPerScenario);
	void solve(const bool& isLogFilePrint);
	double availableComputationTime();
	double gapUBLB ();
	double roundNumber(double value);
	void writeStatistics(string fileName);

	////GET-SET ATTRIBUTES////
	const double& getTimeLimit() const;
	
};
