#pragma once

#include "Common.h"
#include "Instance_PSP.h"

class MyCallback_PSP : public GRBCallback {

private:

	//ATTRIBUTES
	Instance_PSP* instance;
	GRBModel& model;
	vector<GRBVar>& x;
	vector<GRBVar>& y;
	GRBVar& Q;

	//container for solution at a BaB node
	vector<double> xNodeSolution;
	vector<double> yNodeSolution;
	double QNodeSolution;

public:

	//CONSTRUCTORS AND DESTRUCTORS
	MyCallback_PSP(Instance_PSP* instance, GRBModel& model_, vector<GRBVar>& x_, vector<GRBVar>& y_, GRBVar& Q_);
	MyCallback_PSP(const MyCallback_PSP& myCallback_PSP);
	~MyCallback_PSP();

	//METHODS
	void getNodeSolutionX_Jacques();
	void getNodeSolutionQ_Jacques();
	void addVI22_violated_Jacques();
	void addVI22_violated_Jacques_strong();
	bool isVI22_violated(vector<int>& subset, double& probabilitySubset);
	bool isVI22_violated_strong(vector<int>& complement, double& probabilitySubset, vector<double>& coefficients);
	double buildCoefficient(vector<int>& complement, double& probabilitySubset, const int& asset);
	//void addVI22_2violated_Jacques();

	//GET-SET ATTRIBUTES

protected:
	void callback();

};
