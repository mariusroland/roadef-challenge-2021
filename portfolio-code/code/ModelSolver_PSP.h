#pragma once

#include "Common.h"
#include "Instance_PSP.h"
#include "MilpModel_PSP.h"

using namespace std;

class ModelSolver_PSP {

private:

	//ATTRIBUTES
	//Data + Solve stuff
	Instance_PSP* instance;
	MilpModel_PSP* milpModel;

	double timeLimit;
	double gap;

	double modelSolverTime;

public:

	//CONSTRUCTORS AND DESTRUCTORS
	ModelSolver_PSP(Instance_PSP* instance, double timeLimit);
	~ModelSolver_PSP();

	//METHODS
	void solve(const bool& isLogFilePrint);
	double availableComputationTime();
	double roundNumber(double value);
	void writeStatistics(string fileName);

	////GET-SET ATTRIBUTES////
	const double& getTimeLimit() const;
	
};
