#include "ModelSolver_PSP.h"

///////////////////////CONSTRUCTORS AND DESTRUCTORS//////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////

ModelSolver_PSP::ModelSolver_PSP(Instance_PSP* instance, double timeLimit) {
	this->instance = instance;
	this->timeLimit = timeLimit;
	this->gap = 10e-10;
	this->milpModel = new MilpModel_PSP(this->instance);
}

ModelSolver_PSP::~ModelSolver_PSP() {
	delete this->milpModel;
}

///////////////////////////METHODS//////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////

void ModelSolver_PSP::solve(const bool& isLogFilePrint) {

	this->milpModel->buildModel();				
	this->milpModel->initializeCallback();
	this->milpModel->solveModel(this->availableComputationTime(), this->gap, isLogFilePrint);
	chrono::time_point<chrono::system_clock> currentTime = chrono::system_clock::now();
	this->modelSolverTime = (chrono::duration_cast<chrono::nanoseconds>	(currentTime - this->instance->getStartTime()).count())*1e-9;
	vector<double> x = this->milpModel->getXValues();
	cout<<"Real Objective: "<<this->instance->computeRealObjective(x)<<endl;
	this->milpModel->extractSolutionAttributes();

}

double ModelSolver_PSP::availableComputationTime() {

	chrono::time_point<chrono::system_clock> currentTime = chrono::system_clock::now();
	return (this->timeLimit  - (chrono::duration_cast<chrono::nanoseconds>	(currentTime - this->instance->getStartTime()).count())*1e-9);
	
}

double ModelSolver_PSP::roundNumber(double value){
	
  return round(value * 100.0)/100.0;
  
}

void ModelSolver_PSP::writeStatistics(string fileName) {

	ofstream mystream;
	mystream.open(fileName, ios::app);

	mystream << this->instance->getName() << "," << this->instance->getAssetNumber() << "," << this->instance->getScenarioNumber() << "," << this->instance->getAlpha() << "," << this->instance->getTau() << ",";
	mystream << this->instance->getAreWeakVI() << "," << this->instance->getAreStrongVI() << ",";
	mystream <<  this->milpModel->getModelObjectiveValue() << "," << this->milpModel->getModelObjectiveBound() << ",";
	if (this->milpModel->getModelMipGap() <= 1e-5 ){
		mystream<< this->roundNumber(this->modelSolverTime) << "s" << "\n";
	}
	else{
		mystream<< this->roundNumber(this->milpModel->getModelMipGap()) << "\%" << "\n";
	}
	mystream.close();

}

////////////////////////////GET-SET ATTRIBUTES//////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////GET///////////////////////////////////////////////////

const double& ModelSolver_PSP::getTimeLimit() const {

	return this->timeLimit;

}
