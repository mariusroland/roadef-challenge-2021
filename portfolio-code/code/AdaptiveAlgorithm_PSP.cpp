#include "AdaptiveAlgorithm_PSP.h"

///////////////////////CONSTRUCTORS AND DESTRUCTORS//////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////

AdaptiveAlgorithm_PSP::AdaptiveAlgorithm_PSP(Instance_PSP* instance, double timeLimit) {
	this->instance = instance;
	this->timeLimit = timeLimit;
	this->gap = 1e-12;
	this->milpModel = new MilpModel_PSP(this->instance);
	//
	this->vLB = 0.0;
	this->vUB = maxConstant;
}

AdaptiveAlgorithm_PSP::~AdaptiveAlgorithm_PSP() {
	delete this->milpModel;
}

///////////////////////////METHODS//////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////

void AdaptiveAlgorithm_PSP::initializeFirstModel(){
	this->clusterNumber = 1;
	this->clusteredScenarios = vector<int> (this->instance->getScenarioNumber(true),0);
	this->instance->buildReducedInstance(false, this->clusterNumber, this->clusteredScenarios);
}

void AdaptiveAlgorithm_PSP::initializeNextClustering(){

	//Getting the real instance values
	double realQuantile = this->instance->computeRealQuantile(this->xNew);
	vector<double> realReturnPerScenario = this->instance->computeRealReturnPerScenario(this->xNew);

	//Getting cluster values
	vector<int> clusterSize (this->clusterNumber,0);
	vector<double> maxReturnCluster (this->clusterNumber,epsilonValue);
	vector<double> minReturnCluster (this->clusterNumber,maxConstant);
	for (int i = 0; i < this->instance->getScenarioNumber(true); i++){

		// cout<<realReturnPerScenario.at(i)<<", ";

		//Computing cluster size
		clusterSize.at(this->clusteredScenarios.at(i)) += 1;

		//Saving max and min value
		if (maxReturnCluster.at(this->clusteredScenarios.at(i)) < realReturnPerScenario.at(i)){
			maxReturnCluster.at(this->clusteredScenarios.at(i)) = realReturnPerScenario.at(i);
		}
		if (minReturnCluster.at(this->clusteredScenarios.at(i)) > realReturnPerScenario.at(i)){
			minReturnCluster.at(this->clusteredScenarios.at(i)) = realReturnPerScenario.at(i);
		}
	}

	// cout<<endl;
	// cout<<"Min: "<<minReturnCluster.at(0)<<", Max: "<<maxReturnCluster.at(0)<<endl;
	//Getting cluster repartition
	vector<int> iterationCounter (this->clusterNumber,0);
	vector<vector<int>> scenarioRepartition (this->clusterNumber,vector<int>());
	for (int i = 0; i < this->instance->getScenarioNumber(true); i++){
		scenarioRepartition.at(this->clusteredScenarios.at(i)).push_back(i);
	}

	cout<<"Started splitting"<<endl;
	cout<<"Amount of clusters before: "<<this->clusterNumber<<endl;
	//Splitting the important clusters
	int splitCounter = 0;
	int oldClusterNumber = this->clusterNumber;
	for (int i = 0; i < oldClusterNumber; i++){
		bool isMin = minReturnCluster.at(i) < realQuantile + epsilonValue;
		bool isMax = maxReturnCluster.at(i) > realQuantile - epsilonValue;
		int tempCluster = this->clusterNumber;

		if (isMin && isMax && (clusterSize.at(i) == 2)){

			this->clusteredScenarios.at(scenarioRepartition.at(i).at(1)) = this->clusterNumber;
			this->clusterNumber++;
			splitCounter++;

		}
		else if (isMin && isMax && (clusterSize.at(i) > 2)){

			//Get ordered scenarios inside the cluster
			sort(scenarioRepartition.at(i).begin(),scenarioRepartition.at(i).end(), [&](int j,int k){return realReturnPerScenario.at(j)<realReturnPerScenario.at(k);} );
			// for (int j = 0; j <  clusterSize.at(i); j++){
			// 	cout<<scenarioRepartition.at(i).at(j)<<", ";
			// }
			// cout<<endl;
			this->splitCluster(i,scenarioRepartition.at(i),realReturnPerScenario);
			if (this->clusterNumber == tempCluster) {
				cout<<"Cluster "<<i<<endl;
				cout<<"Size = "<<scenarioRepartition.at(i).size()<<endl;
				for (int j = 0; j <  clusterSize.at(i); j++){
					cout<<scenarioRepartition.at(i).at(j)<<", ";
				}
				cout<<endl;
			}
			splitCounter++;

		}

	}
	// If we never split we have a problem need to recluster
	if (splitCounter == 0){
		cout<<"We never split :(, taking biggest cluster"<<endl;
		int indexBiggestCluster = 0;
		int sizeBiggestCluster = clusterSize.at(0);
		for (int i = 1; i < oldClusterNumber; i++){
			if (clusterSize.at(i) > sizeBiggestCluster + epsilonValue){
				indexBiggestCluster = i;
				sizeBiggestCluster = clusterSize.at(i);
			}
		}

		if (clusterSize.at(indexBiggestCluster) == 2){

			this->clusteredScenarios.at(scenarioRepartition.at(indexBiggestCluster).at(1)) = this->clusterNumber;
			this->clusterNumber++;

		}
		else if (clusterSize.at(indexBiggestCluster) > 2){

			//Get ordered scenarios inside the cluster
			sort(scenarioRepartition.at(indexBiggestCluster).begin(),scenarioRepartition.at(indexBiggestCluster).end(), [&](int j,int k){return realReturnPerScenario.at(j)<realReturnPerScenario.at(k);} );
			// for (int j = 0; j <  clusterSize.at(i); j++){
			// 	cout<<scenarioRepartition.at(i).at(j)<<", ";
			// }
			// cout<<endl;
			this->splitCluster(indexBiggestCluster,scenarioRepartition.at(indexBiggestCluster),realReturnPerScenario);
		}
	}

cout<<"Finished splitting"<<endl;
cout<<"Amount of clusters after: "<<this->clusterNumber<<endl<<endl;
}

double AdaptiveAlgorithm_PSP::kernelFunction(vector<int>& scenarioRepartition, vector<double>& realReturnPerScenario, double bandwidth, double x){

	//variables for storing stuff
	double value = 0.0;
	double pi = 2*acos(0.0);

	//Iterate over every scenario
	for(int s  = 0; s < scenarioRepartition.size(); s++){
		value += (1.0/sqrt(2*pi))*exp(-pow((realReturnPerScenario.at(scenarioRepartition.at(s)) - x)/bandwidth,2)/2);
	}

	value *= (1.0/scenarioRepartition.size()/bandwidth);

	return value;

}

void AdaptiveAlgorithm_PSP::splitCluster(int clusterIndex, vector<int>& scenarioRepartition, vector<double>& realReturnPerScenario){

	//Bandwidth computation
	double sum = 0.0, mean = 0.0, sqDiffSum = 0.0, stDev = 0.0;
	for(int s  = 0; s < scenarioRepartition.size(); s++){
		sum +=  realReturnPerScenario.at(scenarioRepartition.at(s));
	}
	mean = sum/scenarioRepartition.size();
	for(int s  = 0; s < scenarioRepartition.size(); s++){
		sqDiffSum +=  pow(realReturnPerScenario.at(scenarioRepartition.at(s)) - mean, 2);
	}
	stDev = sqrt(sqDiffSum / scenarioRepartition.size());
	double bandwidth  = 0.5*stDev*pow(scenarioRepartition.size(),-0.2);


	//Setting stepsize
	double maxReturn = realReturnPerScenario.at(scenarioRepartition.at(scenarioRepartition.size()-1));
	double minReturn = realReturnPerScenario.at(scenarioRepartition.at(0));
	double gridSize = 10.0*this->instance->getScenarioNumber(true);
	double deltaX = (maxReturn - minReturn)/gridSize;

	//Setting a parameters of minima search
	bool isIncreasing = true;
	double oldIterate = this->kernelFunction(scenarioRepartition, realReturnPerScenario, bandwidth, minReturn);
	double iterate = this->kernelFunction(scenarioRepartition, realReturnPerScenario, bandwidth, minReturn+deltaX); ;
	vector<double> minima;
	// cout<<isIncreasing<<", ";

	// cout<<"Finding minima"<<endl;
	//Finding minima
	for(int i  = 1; i < gridSize+1; i++){
		bool isNewIncreasing = (iterate - oldIterate) > 0.0;
		if ((!isIncreasing) && isNewIncreasing) {

			minima.push_back(minReturn+(i-1)*deltaX);
			isIncreasing = isNewIncreasing;
			// cout<<"Minima: "<<minReturn+(i-1)*deltaX<<", ";
		}
		isIncreasing = isNewIncreasing;
		oldIterate = iterate;
		// cout<<isIncreasing<< ", ";
		iterate = this->kernelFunction(scenarioRepartition, realReturnPerScenario, bandwidth, (minReturn+(i+1)*deltaX));
	}
	// cout<<endl;

	//Setting parameters for splitting
	int iterateMinima = 0;
	bool firstMinimaVisited = false;

	// cout<<"Splitting over minima"<<endl;
	//Splitting over every minima
	for(int s  = 0; s < scenarioRepartition.size(); s++){

		//If we did not reach the end
		if (iterateMinima < minima.size()){
			//If we passed by a minima icrease cluster size and go to next minima
			if (realReturnPerScenario.at(scenarioRepartition.at(s)) > minima.at(iterateMinima)){
				this->clusterNumber++;
				iterateMinima++;
			}
		}

		//If we passed the first minima wee add the current scenario to the new cluster
		if (iterateMinima>0){
			this->clusteredScenarios.at(scenarioRepartition.at(s)) = this->clusterNumber-1;
		}

	}
	// for (int i = 0; i < this->instance->getScenarioNumber(true); i++){
	// 	cout<<this->clusteredScenarios.at(i)<<", ";
	// }
	// cout<<endl;

	// cout<<"Amount of clusters: "<<this->clusterNumber<<endl;
}


void AdaptiveAlgorithm_PSP::solve(const bool& isLogFilePrint) {

	//Solving first ASC model and get X values
	this->initializeFirstModel();
	this->milpModel->buildModel();
	this->milpModel->solveModel(this->timeLimit, this->gap, isLogFilePrint);
	this->xNew = this->milpModel->getXValues();

	//Saving solutions
	this->xLB = this->xNew;
	this->xUB = this->xNew;
	this->vLB = this->instance->computeRealObjective(this->xLB);

	//We start ASCA with kappa
	bool kappa = false; //We stay in ASC

	while ((this->availableComputationTime() > epsilonValue) && (this->gapUBLB() > 1e-7)){

		//Start next clustering, remove old constraints, build new reduced instance, add new constraints
		this->initializeNextClustering();
		this->milpModel->removeRiskComputation();
		this->instance->buildReducedInstance(kappa, this->clusterNumber, this->clusteredScenarios);
		this->milpModel->buildRiskComputation();
		cout<<"Reduced Objective last point: "<<this->instance->computeReducedObjective(this->xNew)<<endl<<endl;

		//Case destinction between max or avg clustering
		if (kappa){
			this->milpModel->warmStartXVariable(this->xUB);
			this->milpModel->warmStartYVariable(this->instance->computeYVariables(this->xUB));
			this->milpModel->solveModel(this->availableComputationTime(), this->gap, isLogFilePrint);
			this->xNew = this->milpModel->getXValues();
			this->milpModel->extractSolutionAttributes();
			cout<<endl<<"Max clustering"<<endl;
			cout<<"Best UB: "<<this->vUB<<endl;
			cout<<"New UB: "<<this->milpModel->getModelObjectiveBound()<<endl<<endl;
			if (this->milpModel->getModelObjectiveBound() < this->vUB - epsilonValue) {
				this->xUB = this->xNew;
				this->vUB = this->milpModel->getModelObjectiveBound();
			}
			else{
				kappa = false;
			}
			if (this->vLB + 0.01 < this->instance->computeRealObjective(this->xNew)){
				this->xLB = this->xNew;
				this->vLB = this->instance->computeRealObjective(this->xNew);
			}
		}
		else{
			this->milpModel->warmStartXVariable(this->xLB);
			this->milpModel->warmStartYVariable(this->instance->computeYVariables(this->xLB));
			this->milpModel->solveModel(this->availableComputationTime(), this->gap, isLogFilePrint);
			this->xNew = this->milpModel->getXValues();
			cout<<endl<<"Avg clustering"<<endl;
			cout<<"Best LB: "<<this->vLB<<endl;
			cout<<"New LB: "<<this->instance->computeRealObjective(this->xNew)<<endl<<endl;
			if (this->vLB + epsilonValue < this->instance->computeRealObjective(this->xNew)){
				this->xLB = this->xNew;
				this->vLB = this->instance->computeRealObjective(this->xNew);
			}
			else{
				kappa = true;
			}
		}

		cout<<"FINISHED ITERATION"<<endl;
		cout<<"Current best UB: "<<this->vUB<<endl;
		cout<<"Current best LB: "<<this->vLB<<endl;
		cout<<"Current Gap: "<<this->gapUBLB()*100.0<<endl<<endl;
	}

	//Saving the adaptive time
	chrono::time_point<chrono::system_clock> currentTime = chrono::system_clock::now();
	this->adaptiveTime = (chrono::duration_cast<chrono::nanoseconds>	(currentTime - this->instance->getStartTime()).count())*1e-9;
}


double AdaptiveAlgorithm_PSP::availableComputationTime() {

	chrono::time_point<chrono::system_clock> currentTime = chrono::system_clock::now();
	return (this->timeLimit  - (chrono::duration_cast<chrono::nanoseconds>	(currentTime - this->instance->getStartTime()).count())*1e-9);

}

double AdaptiveAlgorithm_PSP::gapUBLB () {

	return (abs(this->vLB-this->vUB)/this->vLB);

}

double AdaptiveAlgorithm_PSP::roundNumber(double value){

  return ceil(value * 100.0)/100.0;

}

void AdaptiveAlgorithm_PSP::writeStatistics(string fileName) {

	ofstream mystream;
	mystream.open(fileName, ofstream::out | ofstream::app);
	mystream << this->instance->getName() << "," << this->instance->getAssetNumber() << "," << this->instance->getScenarioNumber(true) << "," << this->instance->getAlpha() << "," << this->instance->getTau();
	mystream << fixed << setprecision(2);
	mystream << "," << this->vLB << "," << this->vUB << ",";
	if (this->gapUBLB() <= 1e-7 ){
		mystream<< this->adaptiveTime << "s" << "\n";
	}
	else{
		mystream<< this->gapUBLB()*100.0 << "\%" << "\n";
	}
	mystream.close();

}

////////////////////////////GET-SET ATTRIBUTES//////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////GET///////////////////////////////////////////////////

const double& AdaptiveAlgorithm_PSP::getTimeLimit() const {

	return this->timeLimit;

}
