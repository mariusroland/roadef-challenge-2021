#include "Common.h"
#include "gurobi_c++.h"
#include "Instance_PSP.h"
#include "ModelSolver_PSP.h"
#include "AdaptiveAlgorithm_PSP.h"


int main(int argc, char* argv[]) {

	// FILE PATH
	string path = "/home/marius/Documents/trier/competitions/roadef2020/portfolio-code/src/";

	//INSTANCE TYPE
	bool isRealData = false;
	double timeLimit = 100.0;
	vector<double> alphas = {0,0.25,0.5,0.75};
	vector<double> tau = {0.005,0.01,0.05,0.1};

	// FILE NAMES
	vector<string> nameArray;
	if (isRealData) {
		nameArray = {"FTSE100","SP500","EuroStoxx50","DWJIA"};
	}
	else {
		nameArray = {"Wolsey_20_200_1","Wolsey_20_200_2","Wolsey_20_200_3","Wolsey_20_200_4","Wolsey_20_200_5","Wolsey_20_200_6",
		"Wolsey_20_200_7","Wolsey_20_200_8","Wolsey_20_200_9","Wolsey_20_200_10","Wolsey_20_200_11","Wolsey_20_200_12",
		"Wolsey_400_200_1","Wolsey_400_200_2","Wolsey_400_200_3","Wolsey_400_200_4","Wolsey_400_200_5","Wolsey_400_200_6",
		"Wolsey_400_200_7","Wolsey_400_200_8","Wolsey_400_200_9","Wolsey_400_200_10","Wolsey_400_200_11","Wolsey_400_200_12"};
	}

	//TECHNIQUE
	bool isAdaptiveAlgorithm = true;

	//LOGS
	bool isLogFilePrint = false;

	//OUTPUT FILE & VI
	string outputFileName;
	vector<bool> areWeakVI;
	vector<bool> areStrongVI;
	if (isAdaptiveAlgorithm){
		areWeakVI = { false };
		areStrongVI = { false };
		outputFileName = "ASCA-results.txt";
	}
	else {
		areWeakVI = { true,false,false };
		areStrongVI = { false,true,false };
		outputFileName = "MILP-results.txt";
	}
	ofstream mystream;
	mystream.open(outputFileName, ios::out | ios::trunc);
	mystream.close();


	//ITERATION OVER ALL CONFIGURATIONS
	for(int viIndex = 0; viIndex < areWeakVI.size(); viIndex++) {


		for (int fileIndex = 0; fileIndex < nameArray.size(); fileIndex++) {


			for(int alphaIndex = 0; alphaIndex < alphas.size(); alphaIndex++){


				for(int scenIndex = 0; scenIndex < tau.size(); scenIndex++){

					string fileNumber = nameArray[fileIndex];

					cout << "Setting up file " + fileNumber << "\n";

					Instance_PSP instance(path, fileNumber, isRealData, isAdaptiveAlgorithm, areWeakVI.at(viIndex), areStrongVI.at(viIndex), alphas.at(alphaIndex), tau.at(scenIndex));
					instance.readFile();

					try {
						if (isAdaptiveAlgorithm){
							AdaptiveAlgorithm_PSP adaptiveAlgorithm(&instance, timeLimit);
							adaptiveAlgorithm.solve(isLogFilePrint);
							adaptiveAlgorithm.writeStatistics(outputFileName);
						}
						else {
							ModelSolver_PSP ms(&instance, timeLimit);
							ms.solve(isLogFilePrint);
							ms.writeStatistics(outputFileName);
						}
					}
					catch (GRBException& e) {
						cerr << "Concert exception caught: " << e.getErrorCode() << endl;
						cerr << e.getMessage() << endl;
					}
				}
			}
		}
	}
	return 0;
}
