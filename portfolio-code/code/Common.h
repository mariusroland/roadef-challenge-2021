
//ALPHABETIC ORDER
#include <algorithm>
#include <cassert>
#include <chrono>
#include <cmath>
#include <cstdlib>
#include <fstream>
#include <functional>
#include "gurobi_c++.h"
#include <iostream>
#include <iomanip>
#include <stdexcept>
#include <iterator>
#include <list>
#include <map>
#include <numeric>
#include <random>
#include <regex>
#include <set>
#include <string>
#include <sstream>
#include <time.h>
#include <unordered_map>
#include <unordered_set>
#include <utility>
#include <vector>
#include <csignal>

#define epsilonValue 0.00001
#define maxConstant 10000000//1.7*pow(10,100)
#define refactor 100

using namespace std;
