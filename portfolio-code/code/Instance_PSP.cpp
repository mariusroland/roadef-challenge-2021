#include "Instance_PSP.h"

// Initialize static variables
int Instance_PSP::generatedCuts = 0;
double Instance_PSP::minViolation = INFINITY;
double Instance_PSP::maxViolation = -1;
double Instance_PSP::separationTime = 0.0;
double Instance_PSP::rootNodeGAP = INFINITY;
double Instance_PSP::rootNodeUB = INFINITY;
double Instance_PSP::violationTime = 0.0;

///////////////////////CONSTRUCTORS AND DESTRUCTORS//////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////

Instance_PSP::Instance_PSP() {

}

Instance_PSP::Instance_PSP(string path, string name, bool isMoises, bool isReduced, bool areWeakVI, bool areStrongVI, double alpha, double tau) {

	this->name = name;
	this->path = path;
	this->isMoises = isMoises;
	this->isReduced = isReduced;
	this->areStrongVI = areStrongVI;
	this->areWeakVI = areWeakVI;
	this->isNoVI = !(this->areStrongVI || this->areWeakVI);
	this->alpha = alpha;
	this->tau = tau;

}

Instance_PSP::Instance_PSP(string name, int assetNumber, int scenarioNumber, double minReturn, double maxReturn) {

	WolseyInstanceGenerator(name, assetNumber, scenarioNumber, minReturn, maxReturn);

}

Instance_PSP::Instance_PSP(string path, string name) {

	this->path = path;
	MoisesInstanceTransformer(name);

}

Instance_PSP::Instance_PSP(const Instance_PSP& instance) {

	this->name = instance.name;
	this->weakVIcoeffs = instance.weakVIcoeffs;
	this->strongVICoefficients = instance.strongVICoefficients;

}

Instance_PSP::~Instance_PSP() {

}


/////////////////////////////METHODS////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////readFile////////////////////////////////////////////////////


void Instance_PSP::readFile() {

	try {

		//OPEN FILE
		string comment = this->path + this->name + ".txt";
		cout << comment << "\n";

		readAssetScenarioNumber();
		readReturns();

		if (!this->isMoises) {
			this->rho = 1.1;
		}
		this->rho *= refactor;
		this->probabilities = vector<double>(this->scenarioNumber, (1.0 / static_cast<double>(this->scenarioNumber)));

		//Start time
		this->startTime = chrono::system_clock::now();

		if (this->areWeakVI) {
			computeWeakVICoefficients();
		}

		if (!this->isReduced) {
			setBigMs();
		}

	}
	catch (const exception& ex) {
		cerr << "C++ exception caught in instance: " << ex.what() << endl;
		exit(EXIT_FAILURE);
	}
	catch (...) {
		cerr << "Unknown exception caught in instance\n";
		exit(EXIT_FAILURE);
	}

}

void Instance_PSP::readAssetScenarioNumber() {

	string line;
	int number;
	ifstream myfile(this->path + this->name + ".txt");
	if (!myfile.is_open()) {
		cerr << "Unable to open file " << this->path + this->name + ".txt" << endl;
	}
	else {

		getline(myfile, line);
		istringstream iss(line);
		iss >> this->assetNumber;
		iss >> this->scenarioNumber;
		if (this->isMoises) {
			iss >> this->rho;
		}
		myfile.close();
	}

}

void Instance_PSP::readReturns() {

	this->returns = vector<vector<double>>(this->assetNumber, vector<double>(this->scenarioNumber, -1.0));
	this->expectedReturns = vector<double>(this->assetNumber, -1);

	string line;
	ifstream myfile(this->path + this->name + ".txt");
	if (!myfile.is_open()) {
		cerr << "Unable to open file " << this->path + this->name + ".txt" << endl;
	}
	else {

		getline(myfile, line);

		vector<double> numbers;
		double number;
		while (getline(myfile, line)) {
			if (line.find_first_of("0123456789") != string::npos) {
				istringstream iss(line);
				while (iss >> number)
					numbers.push_back(number);
			}
		}
		//cout << "EXPECTED RETURNS" << endl;
		for (int i = 0; i < this->assetNumber; i++) {
			int time = 0;
			double er = 0;
			for (int t = i * this->scenarioNumber; t < (i + 1) * this->scenarioNumber; t++) {
				this->returns.at(i).at(time) = numbers.at(t) * refactor;
				er += this->returns.at(i).at(time);
				time++;
			}
			//er /= this->timeHorizon;
			this->expectedReturns.at(i) = er / static_cast<double>(this->scenarioNumber);
			//cout << "asset " << i - 1 << "\t" << er << endl;
		}

		myfile.close();
	}

}

void Instance_PSP::computeWeakVICoefficients() {

	this->weakVIcoeffs = vector<double>(this->assetNumber, 0);
	int scenariosToActivate = ceil((1.0 - this->tau) * static_cast<double>(this->scenarioNumber) - epsilonValue);

	for (int i = 0; i < this->assetNumber; i++) {
		vector<double> assetValues = this->returns.at(i);
		sort(assetValues.begin(), assetValues.end(), greater<double>());
		int counter = 0;
		for (int j = 0; j < scenariosToActivate; j++) {
			this->weakVIcoeffs.at(i) += (this->probabilities.at(j) * assetValues.at(j));
			counter++;

		}

	}

}

vector<double> Instance_PSP::computeRealReturnPerScenario(const vector<double>& x) {

	vector<double> returnPerScenarios(this->scenarioNumber, 0.0);
	for (int i = 0; i < this->scenarioNumber; i++) {
		for (int j = 0; j < this->assetNumber; j++) {
			returnPerScenarios.at(i) += x.at(j) * this->returns.at(j).at(i);
		}
	}
	return returnPerScenarios;

}

vector<double> Instance_PSP::computeReducedReturnPerScenario(const vector<double>& x) {

	vector<double> returnPerScenarios(this->scenarioNumberReduced, 0.0);
	for (int i = 0; i < this->scenarioNumberReduced; i++) {
		for (int j = 0; j < this->assetNumber; j++) {
			returnPerScenarios.at(i) += x.at(j) * this->returnsReduced.at(j).at(i);
		}
	}
	return returnPerScenarios;

}

double Instance_PSP::computeRealQuantile(const vector<double>& x) {

	//Here, we want to deactivate the this->scenarioNumber*this->tau lowest returns
	//hence, sorting the returns in increasing order, the "quantile" is at position this->scenarioNumber*this->tau
	vector<double> returnPerScenarios = computeRealReturnPerScenario(x);
	vector<int> indexes(returnPerScenarios.size());
	iota(indexes.begin(),indexes.end(),0);
	sort(indexes.begin(),indexes.end(), [&](int i,int j){return returnPerScenarios[i]<returnPerScenarios[j];} );

	double probabilitySum = 0.0;
	for (int i = 0; i < returnPerScenarios.size(); i++) {
		probabilitySum += this->probabilities.at(indexes.at(i));
		if (probabilitySum > this->tau + epsilonValue) {
			return returnPerScenarios.at(indexes.at(i));
		}
	}
	cout << "NO REDUCED QUANTILE FOUND: " << probabilitySum << "\tTAU: " << this->tau << endl;
}

vector<double> Instance_PSP::computeYVariables(const vector<double>& x) {

	vector<double> returnPerScenarios = computeReducedReturnPerScenario(x);
	vector<double> yVariables(returnPerScenarios.size(),0.0);
	vector<int> indexes(returnPerScenarios.size());
	iota(indexes.begin(),indexes.end(),0); //Initializing
	sort(indexes.begin(),indexes.end(), [&](int i,int j){return returnPerScenarios[i]<returnPerScenarios[j];} );

	double probabilitySum = 0.0;
	for (int i = 0; i < returnPerScenarios.size(); i++) {
		probabilitySum += this->probabilitiesReduced.at(indexes.at(i));
		if (probabilitySum <= this->tau + epsilonValue) {
			yVariables.at(indexes.at(i)) = 1.0;
		}
	}
	return yVariables;

}

double Instance_PSP::computeReducedQuantile(const vector<double>& x) {

	if (!this->isReduced) {
		cout << "PIG! YOU WANT THE REDUCED INSTANCE, BUT IsReduced IS FALSE\n";
		exit(EXIT_FAILURE);
	}

	vector<double> returnPerScenarios = computeReducedReturnPerScenario(x);
	vector<int> indexes(returnPerScenarios.size());
	iota(indexes.begin(),indexes.end(),0); //Initializing
	sort(indexes.begin(),indexes.end(), [&](int i,int j){return returnPerScenarios[i]<returnPerScenarios[j];} );

	double probabilitySum = 0.0;
	for (int i = 0; i < returnPerScenarios.size(); i++) {
		probabilitySum += this->probabilitiesReduced.at(indexes.at(i));
		if (probabilitySum > this->tau + epsilonValue) {
			return returnPerScenarios.at(indexes.at(i));
		}
	}

	cout << "NO REDUCED QUANTILE FOUND: " << probabilitySum << "\tTAU: " << this->tau << endl;
	exit(EXIT_FAILURE);

}

double Instance_PSP::computeRealObjective(const vector<double>& x) {

	double objective = 0.0;
	for (int i = 0; i < this->assetNumber; i++) {
		for (int j = 0; j < this->scenarioNumber; j++) {
			objective += this->probabilities.at(j) * this->returns.at(i).at(j) * x.at(i);
		}
	}
	objective *= this->alpha;
	objective += (1.0 - this->alpha) * computeRealQuantile(x);
	return objective;

}

double Instance_PSP::computeReducedObjective(const vector<double>& x) {

	if (!this->isReduced) {
		cout << "PIG! YOU WANT THE REDUCED INSTANCE, BUT IsReduced IS FALSE\n";
		exit(EXIT_FAILURE);
	}

	double objective = 0.0;
	for (int i = 0; i < this->assetNumber; i++) {
		for (int j = 0; j < this->scenarioNumberReduced; j++) {
			objective += this->probabilitiesReduced.at(j) * this->returnsReduced.at(i).at(j) * x.at(i);
		}
	}
	// cout<<"Exected Value is: "<<objective<< endl;
	objective *= this->alpha;
	objective += (1.0 - this->alpha) * computeReducedQuantile(x);
	return objective;

}

void Instance_PSP::buildReducedInstance(const bool& isMaxAvg, const int& clusterNumber, const vector<int>& clusteredScenarios) {

	if (!this->isReduced) {
		cout << "PIG! YOU WANT THE REDUCED INSTANCE, BUT IsReduced IS FALSE\n";
		exit(EXIT_FAILURE);
	}

	this->scenarioNumberReduced = clusterNumber;

	this->probabilitiesReduced = vector<double>(clusterNumber, 0.0);
	vector<vector<int>> scenariosPerCluster(clusterNumber);
	for (int j = 0; j < this->scenarioNumber; j++) {
		int cluster = clusteredScenarios.at(j);
		this->probabilitiesReduced.at(cluster) += this->probabilities.at(j);
		scenariosPerCluster.at(cluster).push_back(j);
	}

	if (isMaxAvg) {
		computeMaxReturnClusters(scenariosPerCluster);
	}
	else {
		computeAvgReturnClusters(scenariosPerCluster);
	}
	this->strongVICoefficients.clear();
	computeStrongVIcoeffs(scenariosPerCluster);
	setBigMs();

}

void Instance_PSP::computeAvgReturnClusters(const vector<vector<int>>& scenariosPerCluster) {

	this->returnsReduced = vector<vector<double>>(this->assetNumber, vector<double>(this->scenarioNumberReduced, 0.0));
	for (int i = 0; i < this->assetNumber; i++) {
		for (int j = 0; j < scenariosPerCluster.size(); j++) {
			for (int k = 0; k < scenariosPerCluster.at(j).size(); k++) {
				int scenario = scenariosPerCluster.at(j).at(k);
				this->returnsReduced.at(i).at(j) += this->returns.at(i).at(scenario);
			}
			this->returnsReduced.at(i).at(j) /= static_cast<double>(scenariosPerCluster.at(j).size());
		}
	}

}

void Instance_PSP::computeMaxReturnClusters(const vector<vector<int>>& scenariosPerCluster) {

	this->returnsReduced = vector<vector<double>>(this->assetNumber, vector<double>(this->scenarioNumberReduced, epsilonValue));
	for (int i = 0; i < this->assetNumber; i++) {
		for (int j = 0; j < scenariosPerCluster.size(); j++) {
			for (int k = 0; k < scenariosPerCluster.at(j).size(); k++) {
				int scenario = scenariosPerCluster.at(j).at(k);
				this->returnsReduced.at(i).at(j) = max(this->returnsReduced.at(i).at(j), this->returns.at(i).at(scenario));
			}
		}
	}

}

void Instance_PSP::computeStrongVIcoeffs(const vector<vector<int>>& scenariosPerCluster) {

	for (int i = 0; i < this->scenarioNumberReduced; i++) {
		if (this->probabilitiesReduced.at(i) > this->tau + epsilonValue) {
			this->strongVICoefficients.insert(make_pair(i, computeCoeffs(i, scenariosPerCluster.at(i))));
		}
	}

}

vector<double> Instance_PSP::computeCoeffs(const int& scenario, const vector<int>& cluster) {

	vector<double> coeffs(this->assetNumber, -1);

	int scenariosToActivate = ceil((this->probabilitiesReduced.at(scenario) - this->tau) * static_cast<double>(this->scenarioNumber) - epsilonValue);
	// cout<<"Cluster "<<scenario<<", Probability "<<this->probabilitiesReduced.at(scenario)<<", Activated scenarios "<<scenariosToActivate<<endl;

	for (int j = 0; j < this->assetNumber; j++) {
		vector<pair<double, int>> assetValues(cluster.size());
		for (int i = 0; i < cluster.size(); i++) {
			assetValues.at(i) = make_pair(this->returns.at(j).at(cluster.at(i)), cluster.at(i));
		}
		sort(assetValues.rbegin(), assetValues.rend());

		double coeff = 0.0;
		for (int i = 0; i < scenariosToActivate; i++) {
			coeff += this->probabilities.at(assetValues.at(i).second) * assetValues.at(i).first;
		}
		coeffs.at(j) = coeff;
		// cout<<"Asset "<<j<<", Coefficient "<<coeff<<endl;
	}
	return coeffs;

}

void Instance_PSP::WolseyInstanceGenerator(string name, int assetNumber, int scenarioNumber, double minReturn, double maxReturn) {

	string streamName = name + ".txt";
	ofstream mystream;

	mystream.open(streamName, ios::app);

	mystream << assetNumber << " " << scenarioNumber << "\n";

	random_device r;
	default_random_engine generator{ r() };
	uniform_real_distribution<double> distribution(minReturn, maxReturn + epsilonValue);

	for (int i = 0; i < assetNumber; i++) {
		for (int t = 0; t < scenarioNumber; t++) {
			double assetPrice = distribution(generator);
			mystream << assetPrice << " ";
			//cout << "Draw " << this->assetPrices.at(i).at(t) << endl;
		}
		mystream << "\n";
	}

	mystream.close();

}

void Instance_PSP::MoisesInstanceTransformer(const string& name) {

	//READ
	string fToOpen = name + "_Weekly_assetPrices";
	vector<vector<double>> data = readMoises_assetPrices(fToOpen);
	fToOpen = name + "_Weekly_indexPrices";
	vector<double> indexPrices = readMoises_indexPrices(fToOpen);

	//PROCESS
	this->assetNumber = data.at(0).size();
	this->scenarioNumber = data.size() - 1;
	this->returns = vector<vector<double>>(this->assetNumber, vector<double>(this->scenarioNumber, maxConstant));
	for (int i = 0; i < this->assetNumber; i++) {
		for (int t = 0; t < this->scenarioNumber; t++) {
			this->returns.at(i).at(t) = 1.0 + ((data.at(t + 1).at(i) - data.at(t).at(i)) / data.at(t).at(i));
		}
	}

	double avgIndexReturn = 0.0;
	for (int t = 0; t < this->scenarioNumber; t++) {
		avgIndexReturn += 1.0 + ((indexPrices.at(t + 1) - indexPrices.at(t)) / indexPrices.at(t));
	}
	avgIndexReturn /= static_cast<double>(this->scenarioNumber);

	//WRITE
	string fToWrite = "Moises_" + name + ".txt";
	ofstream mystream;
	mystream.open(fToWrite, ios::app);

	mystream << this->assetNumber << " " << this->scenarioNumber << " " << avgIndexReturn << "\n";

	for (int i = 0; i < this->assetNumber; i++) {
		for (int t = 0; t < this->scenarioNumber; t++) {
			mystream << this->returns.at(i).at(t) << " ";
		}
		mystream << "\n";
	}

	mystream.close();

}

vector<vector<double>> Instance_PSP::readMoises_assetPrices(const string& fToOpen) {

	ifstream myfile(this->path + fToOpen + ".txt");
	if (!myfile.is_open()) {
		cerr << "Unable to open file " << this->path + fToOpen + ".txt" << endl;
		exit(EXIT_FAILURE);
	}

	string line;
	vector<vector<double>> data;

	double number;
	while (getline(myfile, line)) {
		vector<double> numbers;
		if (line.find_first_of("0123456789") != string::npos) {
			istringstream iss(line);
			while (iss >> number) {
				numbers.push_back(number);
			}
		}
		data.push_back(numbers);
	}

	myfile.close();

	return data;

}

vector<double> Instance_PSP::readMoises_indexPrices(const string& fToOpen) {

	ifstream myfile(this->path + fToOpen + ".txt");
	if (!myfile.is_open()) {
		cerr << "Unable to open file " << this->path + fToOpen + ".txt" << endl;
		exit(EXIT_FAILURE);
	}

	string line;
	vector<double> indexPrices;

	double number;
	while (getline(myfile, line)) {
		if (line.find_first_of("0123456789") != string::npos) {
			istringstream iss(line);
			while (iss >> number) {
				indexPrices.push_back(number);
			}
		}
	}

	myfile.close();

	return indexPrices;

}

////////////////////////GET-SET ATTRIBUTES///////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////GET///////////////////////////////////////////////////////////

string Instance_PSP::getName() {

	return this->name;

}

int& Instance_PSP::getAssetNumber() {

	return this->assetNumber;

}

int& Instance_PSP::getScenarioNumber(bool forceReal) {

	if (!this->isReduced || forceReal) {
		return this->scenarioNumber;
	}
	else {
		return this->scenarioNumberReduced;
	}

}

double& Instance_PSP::getRho() {

	return this->rho;

}

double& Instance_PSP::getTau() {

	return this->tau;

}

double& Instance_PSP::getAlpha() {

	return this->alpha;

}

vector<vector<double>>& Instance_PSP::getReturns(bool forceReal) {

	if (!this->isReduced || forceReal) {
		return this->returns;
	}
	else {
		return this->returnsReduced;
	}

}

vector<double>& Instance_PSP::getExpectedReturns() {

	return this->expectedReturns;

}

vector<double>& Instance_PSP::getProbabilities(bool forceReal) {

	if (!this->isReduced || forceReal) {
		return this->probabilities;
	}
	else {
		return this->probabilitiesReduced;
	}

}

vector<double>& Instance_PSP::getBigMs() {

	return this->bigMs;
}

bool Instance_PSP::getIsReduced() {

	return this->isReduced;

}

bool& Instance_PSP::getIsNoVI() {

	return this->isNoVI;

}

bool& Instance_PSP::getAreWeakVI() {

	return this->areWeakVI;

}

bool& Instance_PSP::getAreStrongVI() {

	return this->areStrongVI;

}

vector<double>& Instance_PSP::getWeakVIcoeffs() {

	return this->weakVIcoeffs;

}

unordered_map<int, vector<double>>& Instance_PSP::getStrongVICoefficients() {

	return this->strongVICoefficients;

}

const chrono::time_point<chrono::system_clock>& Instance_PSP::getStartTime() const {

	return this->startTime;

}

void Instance_PSP::setBigMs() {

	vector<double> minRetPerScenario(this->getScenarioNumber(), INFINITY);
	vector<double> maxRetPerScenario(this->getScenarioNumber(), 0.0);

	for (int i = 0; i < this->getScenarioNumber(); i++) {
		for (int j = 0; j < this->assetNumber; j++) {
			minRetPerScenario.at(i) = min(minRetPerScenario.at(i), this->getReturns().at(j).at(i));
			maxRetPerScenario.at(i) = max(maxRetPerScenario.at(i), this->getReturns().at(j).at(i));
		}

	}

	sort(maxRetPerScenario.begin(), maxRetPerScenario.end());
	double probabilitySum = 0.0;
	double quantile = maxConstant;
	for (int i = 0; i < this->getScenarioNumber(); i++) {
		probabilitySum += this->getProbabilities().at(i);
		if (probabilitySum > this->tau - epsilonValue) {
			quantile = maxRetPerScenario.at(i);
			break;
		}
	}

	this->bigMs = vector<double>(this->getScenarioNumber(), quantile);

	for (int i = 0; i < this->getScenarioNumber(); i++) {
		this->bigMs.at(i) -= minRetPerScenario.at(i);
	}
}

void Instance_PSP::setIsReduced(const bool& isReduced) {

	this->isReduced = isReduced;

}
