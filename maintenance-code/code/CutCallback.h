#pragma once

#include "Common.h"
#include "Instance.h"
#include "Intervention.h"
#include "Season.h"

class CutCallback : public GRBCallback {

private:

	//ATTRIBUTES
	Instance* instance;
	GRBModel& model;
	vector<unordered_map<int, GRBVar>>& x;
	vector<unordered_map<int, GRBVar>>& y;
	vector<GRBVar>& Q;
	vector<GRBVar>& eps;

	//container for solution at a BaB node
	vector<unordered_map<int, double>> xNodeSolution;
	vector<double> QNodeSolution;

public:

	//CONSTRUCTORS AND DESTRUCTORS
	CutCallback(Instance* instance, GRBModel& model_, vector<unordered_map<int, GRBVar>>& x_, vector<unordered_map<int, GRBVar>>& y_, vector<GRBVar>& Q_, vector<GRBVar>& eps_);
	CutCallback(const CutCallback& cutCallback);
	~CutCallback();
	
	//VALID INEQUALITIES
	void addVI();
	bool isVI_violated(int t); 

	//SOLUTION GETTERS
	void getNodeSolutionX();
	void getNodeSolutionQ();
	

protected:
	void callback();
};
