#include "CutCallback2.h"

///////////////////////CONSTRUCTORS AND DESTRUCTORS//////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////

CutCallback2::CutCallback2(Instance* instance_, GRBModel& model_, vector<unordered_map<int, GRBVar>>& x_, vector<unordered_map<int, GRBVar>>& y_, vector<GRBVar>& Q_, vector<GRBVar>& eps_)
	: model(model_), x(x_), y(y_), Q(Q_), eps(eps_), instance(instance_) {
	this->enteredVI = false;
	this->nodeCount = -1;
	this->sBarComplementScenarios = unordered_map<int, bool>();
	this->xNodeSolution = vector<unordered_map<int, double>>(this->instance->getInterventions().size(), unordered_map<int, double>());
	this->QNodeSolution = vector<double>(this->instance->getTimeHorizon());
}

CutCallback2::CutCallback2(const CutCallback2& cutCallback)
	: model(cutCallback.model), x(cutCallback.x), y(cutCallback.y), Q(cutCallback.Q), eps(cutCallback.eps), instance(cutCallback.instance) {
}

CutCallback2::~CutCallback2() {
}

///////////////////////////METHODS//////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////exclusionsCallback//////////////////////////////////////////

void CutCallback2::callback() {
	chrono::time_point<chrono::system_clock> start, end;
	start = chrono::system_clock::now();

	//VALID INEQUALITIES: INCLUDE THEM IN A MIP NODE, IF THE SOLUTION IS OPTIMAL
	if (where == GRB_CB_MIPNODE && getIntInfo(GRB_CB_MIPNODE_STATUS) == GRB_OPTIMAL && getDoubleInfo(GRB_CB_MIPNODE_NODCNT) == 0) {

		try {

			cout<<"Added cut"<<endl;
			addVIs();
			this->enteredVI = true;
			this->nodeCount = getDoubleInfo(GRB_CB_MIPNODE_NODCNT);

		}
		catch (GRBException& e) {

			cerr << "Concert exception caught: " << e.getErrorCode() << endl;
			cerr << e.getMessage() << endl;

		}
		catch (...) {

			cerr << "Unknown exception caught" << endl;

		}

	}


}

////////////////////////////THECUT//////////////////////////////////////////

//The cut function in itself
void CutCallback2::addVIs() {

	//Get solution
	getNodeSolutionX();
	getNodeSolutionQ();

	//Iterate over all timesteps
	for (int t = 0; t < this->instance->getTimeHorizon(); t++) {

		//If we can add a cut we do it
		if (extractScenarios(t)) {

			GRBLinExpr constraint = -this->sBarComplementProbability*this->Q[t];

			for (int i = 0; i < this->instance->getInterventions().size(); i++) {

				Intervention* intervention = &this->instance->getInterventions().at(i);

				for (int s = 0; s < intervention->getInProcess().at(t).size(); s++) {

					int tPrime = intervention->getInProcess().at(t).at(s);
					
					constraint += this->computeCoefficient(t,tPrime,intervention->getId())*this->x[intervention->getId()][tPrime];
					
				}

			}

			addLazy(constraint <= 0);
			
		}

	}

}

//The checker that makes sure the cut is violated
bool CutCallback2::extractScenarios(int t) {

	//Clearing the container and initializing the size + probability
	this->sBarComplementScenarios.clear();
	this->sBarComplementSize = 0;
	this->sBarComplementProbability = this->instance->getQuantile();
	
	//Parameters for checking validity
	double riskValue;
	double quantileValue = this->QNodeSolution[t];

	//Computing the important scenarios
	for (int j = 0; j < this->instance->getScenarioNumber().at(t); j++) {

		riskValue = 0.0;
		
		for (int i = 0; i < this->instance->getInterventions().size(); i++) {

			Intervention* intervention = &this->instance->getInterventions().at(i);

			for (int s = 0; s < intervention->getInProcess().at(t).size(); s++) {

				int start = intervention->getInProcess().at(t).at(s);
				riskValue += this->instance->getRiskValues()[intervention->getId()][t][start][j] * this->xNodeSolution[intervention->getId()][start];

			}

		}

		//If the Q is greater then add the scenario to Sbar
		if (quantileValue > riskValue) {
			this->sBarComplementScenarios[j] = false;
			this->sBarComplementProbability -= this->instance->getScenarioProbabilities().at(t).at(j);
			
		}
		else {
			this->sBarComplementScenarios[j] = true;
			this->sBarComplementSize++;
		}

	}

	// cout<< "Time "<<t<<": ";
	// cout<< this->sBarComplementProbability <<endl;
	
	//Checking validity of the cut
	if (this->sBarComplementProbability > 0.0) {
		return true;
	}
	else{
		return false;
	}

}

double CutCallback2::computeCoefficient(int t, int tPrime, int intervention){

	//Saved values for the Cut
	vector<double> riskValues (this->sBarComplementSize,0.0);
	vector<double> probabilityValues (this->sBarComplementSize,0.0);

	//Counter for oing through the scenarios
	int counter = 0;

	//Extracting the risk and probabilities of the scenarios used in the cut
	for (int j = 0; j < this->instance->getScenarioNumber().at(t); j++) {
		
		if (this->sBarComplementScenarios[j]) {
			riskValues[counter] = this->instance->getRiskValues()[intervention][t][tPrime][j];
			probabilityValues[counter] = this->instance->getScenarioProbabilities()[t][j];
			counter++;
		}

	}

	//Sorting the risks from small to large
	vector<double> riskIndexVector(this->sBarComplementSize);
	iota(riskIndexVector.begin(),riskIndexVector.end(),0);
	stable_sort(riskIndexVector.begin(),riskIndexVector.end(), [&](int i,int j) {return riskValues[i] < riskValues[j];});
	

	//Probabilitysum used for computing the coefficients
	double probabilitySum, coefficient = 0.0;
	counter = 0;

	// Running while loop for greedy coefficient computation
	while (probabilitySum < this->sBarComplementProbability) {

		//Check if we no not exceed the allowed probability
		if (probabilitySum + probabilityValues[riskIndexVector[counter]] <= this->sBarComplementProbability){
			coefficient += probabilityValues[riskIndexVector[counter]]*riskValues[riskIndexVector[counter]];	
		}
		//Else add only what is possible
		else {			
			coefficient += (this->sBarComplementProbability - probabilitySum)*riskValues[riskIndexVector[counter]];			
		}

		//Increment the probability and counter
		probabilitySum += probabilityValues[riskIndexVector[counter]];
		counter++;
			
	}

	return coefficient;
}


////////////////////////////GET-SET ATTRIBUTES//////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////GET///////////////////////////////////////////////////

void CutCallback2::getNodeSolutionX() {

	this->xNodeSolution = vector<unordered_map<int, double>>(this->instance->getInterventions().size(), unordered_map<int, double>());

	//get solution at the current node of the B&B search tree: x, y and Q
	for (int i = 0; i < this->instance->getInterventions().size(); i = i + 1) {

		Intervention* intervention = &this->instance->getInterventions().at(i);
		unordered_map<int, double> valuesPerStartTime;

		for (int j = 0; j < intervention->getFeasibleStartingTimes().size(); j = j + 1) {
			int start = intervention->getFeasibleStartingTimes().at(j);

			valuesPerStartTime.insert(make_pair(start, getNodeRel(this->x[intervention->getId()][start])));
		}

		this->xNodeSolution[intervention->getId()] = valuesPerStartTime;
	}

}

void CutCallback2::getNodeSolutionQ() {

	this->QNodeSolution = vector<double>(this->instance->getTimeHorizon());

	for (int t = 0; t < this->instance->getTimeHorizon(); t++) {

		this->QNodeSolution[t] = getNodeRel(this->Q[t]);

	}

}


