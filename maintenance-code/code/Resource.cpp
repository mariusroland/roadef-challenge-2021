#include "Resource.h"

///////////////////////CONSTRUCTORS AND DESTRUCTORS//////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////

Resource::Resource(int id, string name, vector<double> lowerBounds, vector<double> upperBounds) {

	this->id = id;
	this->name = name;
	this->lowerBounds = lowerBounds;
	this->upperBounds = upperBounds;

}

Resource::Resource(const Resource& resource) {

	this->id = resource.id;
	this->name = resource.name;
	this->lowerBounds = resource.lowerBounds;
	this->upperBounds = resource.upperBounds;

}

Resource::~Resource() {

}

///////////////////////////METHODS//////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////toString////////////////////////////////////////////////////

string Resource::toString() {

	string str;

	str.append("id: ");
	str.append(to_string(id));

	str.append("\tname: ");
	str.append(name);

	str.append("\t lowerBounds size: ");
	str.append(to_string(lowerBounds.size()));

	str.append("\t upperBounds size: ");
	str.append(to_string(upperBounds.size()));

	return str;

}

////////////////////////////GET-SET ATTRIBUTES//////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////GET///////////////////////////////////////////////////

int Resource::getId() {

	return this->id;

}

string Resource::getName() {

	return this->name;

}

vector<double>& Resource::getLowerBounds() {

	return this->lowerBounds;

}

vector<double>& Resource::getUpperBounds() {

	return this->upperBounds;

}