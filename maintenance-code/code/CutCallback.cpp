#include "CutCallback.h"

///////////////////////CONSTRUCTORS AND DESTRUCTORS//////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////

CutCallback::CutCallback(Instance* instance_, GRBModel& model_, vector<unordered_map<int, GRBVar>>& x_, vector<unordered_map<int, GRBVar>>& y_, vector<GRBVar>& Q_, vector<GRBVar>& eps_)
	: model(model_), x(x_), y(y_), Q(Q_), eps(eps_), instance(instance_) {
  this->xNodeSolution = vector<unordered_map<int, double>>(this->instance->getInterventions().size(), unordered_map<int, double>());
  this->QNodeSolution = vector<double>(this->instance->getTimeHorizon());
}

CutCallback::CutCallback(const CutCallback& cutCallback)
	: model(cutCallback.model), x(cutCallback.x), y(cutCallback.y), Q(cutCallback.Q), eps(cutCallback.eps), instance(cutCallback.instance) {
}

CutCallback::~CutCallback() {

}

///////////////////////////METHODS//////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////exclusionsCallback//////////////////////////////////////////

void CutCallback::callback() {
	chrono::time_point<chrono::system_clock> start, end;
	start = chrono::system_clock::now();

	//VALID INEQUALITIES: INCLUDE THEM IN A MIP NODE, IF THE SOLUTION IS OPTIMAL
	if (where == GRB_CB_MIPNODE && getIntInfo(GRB_CB_MIPNODE_STATUS) == GRB_OPTIMAL && getDoubleInfo(GRB_CB_MIPNODE_NODCNT) == 0) {

		try {

			addVI();

		}
		catch (GRBException& e) {

			cerr << "Concert exception caught: " << e.getErrorCode() << endl;
			cerr << e.getMessage() << endl;

		}
		catch (...) {

			cerr << "Unknown exception caught" << endl;

		}

	}


}

////////////////////////////THECUT//////////////////////////////////////////

//The cut function itself
void CutCallback::addVI() {

	//get solution
	getNodeSolutionX();
	getNodeSolutionQ();

	for (int t = 0; t < this->instance->getTimeHorizon(); t++) {

		if (isVI_violated(t)) {

			GRBLinExpr constraint = 0;

			//BUILD THE SECOND TERM OF THE RHS
			for (int j = 0; j < this->instance->getScenarioNumber().at(t); j++) {

				double coeff = this->instance->getScenarioProbabilities().at(t).at(j);
				double secondTermValuePerScenario = this->QNodeSolution[t];
				GRBLinExpr secondTermRhs = this->Q[t];

				for (int i = 0; i < this->instance->getInterventions().size(); i++) {

					Intervention* intervention = &this->instance->getInterventions().at(i);

					for (int s = 0; s < intervention->getInProcess().at(t).size(); s++) {

						int start = intervention->getInProcess().at(t).at(s);

						secondTermRhs -= this->instance->getRiskValues()[intervention->getId()][t][start][j] * this->x[intervention->getId()][start];
						secondTermValuePerScenario -= this->instance->getRiskValues()[intervention->getId()][t][start][j] * this->xNodeSolution[intervention->getId()][start];

					}

				}

				//if the term of the sum is positive->add it to the constraint
				if (secondTermValuePerScenario > epsilonValue) {
					secondTermRhs *= coeff;
					constraint += secondTermRhs;
				}

			}

			//BUILD THE ENTIRE CONSTRAINT IF THE SECOND TERM OF THE RHS IS ADDED
			if (constraint.size() > 0) {

				//ADD THE FIRST TERM OF THE RHS
				for (int i = 0; i < this->instance->getInterventions().size(); i++) {

					Intervention* intervention = &this->instance->getInterventions().at(i);

					for (int s = 0; s < intervention->getInProcess().at(t).size(); s++) {

						int start = intervention->getInProcess().at(t).at(s);
						constraint += this->instance->getOldVICoefficients()[intervention->getId()][t][start] * this->x[intervention->getId()][start];

					}

				}

				//REMOVE THE QUANTILE
				constraint -= this->Q[t];

				//if the constraint is violated, ADD THE CONSTRAINT TO THE MODEL
				addLazy(constraint <= 0);
				// cout<<"Time "<<t<<endl;
				// cout<<"Added cut with "<<counter<<" scenarios"<<endl;
				// cout<<"We had "<<ceil(this->instance->getScenarioNumber().at(t)*this->instance->getQuantile())<<" scenarios"<<endl<<endl;
				//Instance::generatedCuts++;

			}

		}

	}

}

//The checker that makes sure the cut is violated
bool CutCallback::isVI_violated(int t) {

	double constraintValue = 0;

	//BUILD THE SECOND TERM OF THE RHS
	for (int j = 0; j < this->instance->getScenarioNumber().at(t); j++) {

		double coeff = this->instance->getScenarioProbabilities().at(t).at(j);
		double secondTermValuePerScenario = this->QNodeSolution[t];

		for (int i = 0; i < this->instance->getInterventions().size(); i++) {

			Intervention* intervention = &this->instance->getInterventions().at(i);

			for (int s = 0; s < intervention->getInProcess().at(t).size(); s++) {

				int start = intervention->getInProcess().at(t).at(s);
				secondTermValuePerScenario -= this->instance->getRiskValues()[intervention->getId()][t][start][j] * this->xNodeSolution[intervention->getId()][start];

			}

		}

		//if the term of the sum is positive->add it to the constraint
		if (secondTermValuePerScenario > epsilonValue) {
			constraintValue += coeff * secondTermValuePerScenario;
		}

	}

	if (constraintValue < epsilonValue) {
		return false;
	}

	//BUILD THE ENTIRE CONSTRAINT IF THE SECOND TERM OF THE RHS IS ADDED
	//ADD THE FIRST TERM OF THE RHS
	for (int i = 0; i < this->instance->getInterventions().size(); i++) {

		Intervention* intervention = &this->instance->getInterventions().at(i);

		for (int s = 0; s < intervention->getInProcess().at(t).size(); s++) {

			int start = intervention->getInProcess().at(t).at(s);
			constraintValue += this->instance->getOldVICoefficients()[intervention->getId()][t][start] * this->xNodeSolution[intervention->getId()][start];

		}

	}

	//REMOVE THE QUANTILE
	constraintValue -= this->QNodeSolution[t];

	//if the constraint is violated, ADD THE CONSTRAINT TO THE MODEL
	if (constraintValue > epsilonValue) {
		return true;
	}
	return false;

}


////////////////////////////GET-SET ATTRIBUTES//////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////GET///////////////////////////////////////////////////

void CutCallback::getNodeSolutionX() {

	this->xNodeSolution = vector<unordered_map<int, double>>(this->instance->getInterventions().size(), unordered_map<int, double>());

	//get solution at the current node of the B&B search tree: x, y and Q
	for (int i = 0; i < this->instance->getInterventions().size(); i = i + 1) {

		Intervention* intervention = &this->instance->getInterventions().at(i);
		unordered_map<int, double> valuesPerStartTime;

		for (int j = 0; j < intervention->getFeasibleStartingTimes().size(); j = j + 1) {
			int start = intervention->getFeasibleStartingTimes().at(j);

			valuesPerStartTime.insert(make_pair(start, getNodeRel(this->x[intervention->getId()][start])));
		}

		this->xNodeSolution[intervention->getId()] = valuesPerStartTime;
	}

}

void CutCallback::getNodeSolutionQ() {

	this->QNodeSolution = vector<double>(this->instance->getTimeHorizon());

	for (int t = 0; t < this->instance->getTimeHorizon(); t++) {

		this->QNodeSolution[t] = getNodeRel(this->Q[t]);

	}

}


