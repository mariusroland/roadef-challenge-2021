#pragma once

#include "Common.h"
#include "Instance.h"
#include "Intervention.h"
#include "Season.h"

class CutCallback2 : public GRBCallback {

private:

	//MODEL ATTRIBUTES
	Instance* instance;
	GRBModel& model;
	vector<unordered_map<int, GRBVar>>& x;
	vector<unordered_map<int, GRBVar>>& y;
	vector<GRBVar>& Q;
	vector<GRBVar>& eps;

	//CUT ATTRIBUTES
	bool enteredVI;
	int nodeCount;
	unordered_map<int, bool> sBarComplementScenarios;
	double sBarComplementProbability;
	int sBarComplementSize;
	vector<unordered_map<int, double>> xNodeSolution;
	vector<double> QNodeSolution;

public:

	//CONSTRUCTORS AND DESTRUCTORS
	CutCallback2(Instance* instance, GRBModel& model_, vector<unordered_map<int, GRBVar>>& x_, vector<unordered_map<int, GRBVar>>& y_, vector<GRBVar>& Q_, vector<GRBVar>& eps_);
	CutCallback2(const CutCallback2& cutCallback);
	~CutCallback2();
	
	//VALID INEQUALITIES
	void addVIs();
	bool extractScenarios(int t);
	double computeCoefficient(int t, int tPrime, int intervention);

	//SOLUTION GETTERS
	void getNodeSolutionX();
	void getNodeSolutionQ();
	

protected:
	void callback();
};
