#include "Exclusion.h"

///////////////////////CONSTRUCTORS AND DESTRUCTORS//////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////

Exclusion::Exclusion(int id, string name, Intervention* intervention1, Intervention* intervention2, Season* season) {

	this->id = id;
	this->name = name;
	this->intervention1 = intervention1;
	this->intervention2 = intervention2;
	this->season = season;

}

Exclusion::Exclusion(const Exclusion& exclusion) {

	this->id = exclusion.id;
	this->name = exclusion.name;
	this->intervention1 = exclusion.intervention1;
	this->intervention2 = exclusion.intervention2;
	this->season = exclusion.season;

}

Exclusion::~Exclusion() {

}

///////////////////////////METHODS//////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////toString////////////////////////////////////////////////////

string Exclusion::toString() {

	string str;

	str.append("\tintervention1: ");
	str.append(intervention1->getName());

	str.append("\tintervention2: ");
	str.append(intervention2->getName());

	str.append("\t season: ");
	str.append(season->getName());

	return str;

}

////////////////////////////GET-SET ATTRIBUTES//////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////GET///////////////////////////////////////////////////

int Exclusion::getId() {

	return this->id;

}

string Exclusion::getName() {

	return this->name;

}

Intervention* Exclusion::getIntervention1() {

	return this->intervention1;

}

Intervention* Exclusion::getIntervention2() {

	return this->intervention2;

}

Season* Exclusion::getSeason() {

	return this->season;

}