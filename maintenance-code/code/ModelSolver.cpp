#include "ModelSolver.h"

///////////////////////CONSTRUCTORS AND DESTRUCTORS//////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////

ModelSolver::ModelSolver(Instance* instance, double givenTimeLimit, bool useCuts, bool useOldVI, bool branchingPriorityX, bool fixThreads, int numThreads) {

	//Model
	this->instance = instance;
	this->milpModel = new MilpModel(this->instance, false, false, false, false, branchingPriorityX, fixThreads, numThreads);

	//Time
	this->startTime = this->instance->getStartTimeInitialization();
	this->givenTimeLimit = givenTimeLimit;

	//Solution
	this->bestXSolution = vector<unordered_map<int, double>> (this->instance->getInterventions().size());
	this->bestYSolution = vector<unordered_map<size_t, bool>> (this->instance->getTimeHorizon(),unordered_map<size_t, bool> {});
	this->bestObjectiveValue = maxConstant;
	this->bestObjectiveBound = 0.0;

	//Computational parameters
	this->useCuts = useCuts;
	this->useOldVI = useOldVI;
}

ModelSolver::ModelSolver(const ModelSolver& adaptiveAlgorithm) {

}

ModelSolver::~ModelSolver() {
	delete this->milpModel;
}

///////////////////////////METHODS//////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////

void ModelSolver::solve(){

	//Initial solve without quantile
	this->milpModel->initializeGeneralMILP();
	chrono::time_point<chrono::system_clock> start = chrono::system_clock::now();
	if (this->availableComputationTime() > 0){
		if (this->useCuts){
			this->milpModel->initializeCutCallback(this->useOldVI);
		}
		this->milpModel->solveModel(this->availableComputationTime(),this->requiredOptimalityGap());
	}
	chrono::time_point<chrono::system_clock> stop = chrono::system_clock::now();
	this->modelSolveTime = (chrono::duration_cast<chrono::nanoseconds>	(stop - start).count())*1e-9;
	this->milpModel->extractSolutionAttributes();

	//Saving files if we have improved solutions
	if (this->milpModel->getModelObjectiveValue() < maxConstant - epsilonValue){
		this->milpModel->getXValues(this->bestXSolution);
		this->milpModel->getYValues(this->bestYSolution);
	}

}

double ModelSolver::availableComputationTime(){

	chrono::time_point<chrono::system_clock> currentTime = chrono::system_clock::now();
	return (this->givenTimeLimit - (chrono::duration_cast<chrono::nanoseconds>	(currentTime - this->startTime).count())*1e-9);

}

double ModelSolver::requiredOptimalityGap(){
	return 0.0;
}

////////////////////////////GET-SET ATTRIBUTES//////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////GET///////////////////////////////////////////////////

double ModelSolver::roundNumber(double value){
  return round(value * 100.0)/100.0;
}

void ModelSolver::writeStatistics(string fileNumber, string fileName){

	//Print solution if available
	if (this->milpModel->getModelObjectiveValue() < maxConstant){
		this->milpModel->solutionPrinting(this->bestXSolution,this->bestYSolution,fileName+"-solution");
	}

	//Save run data
	ofstream mystream;
	mystream.open(fileName, ofstream::out | ofstream::trunc);
	mystream << fileNumber << ",";
	mystream << this->requiredOptimalityGap() << ",";
	mystream << this->roundNumber(this->givenTimeLimit) << ",";
	mystream << fixed << setprecision(2);
	mystream << this->instance->getReadTime() << ",";
	mystream << this->milpModel->getModelBuildTime() << ",";
	mystream << this->instance->getBigMComputationTime() << ",";
	// if (this->instance->getIsReduced()) {
	// 	mystream << this->roundNumber(this->instance->getReductionTime()) << ",";
	// }
	// else {
	// 	mystream << "-" << ",";
	// }
	mystream << this->roundNumber(this->modelSolveTime) << ",";
	if (this->milpModel->getModelObjectiveValue() < maxConstant){
		this->bestObjectiveValue = this->milpModel->getModelObjectiveValue();
			mystream << this->bestObjectiveValue << ",";
	}
	else{
		mystream << "-" << ",";
	}
	if (this->milpModel->getModelObjectiveBound() > 0){
		this->bestObjectiveBound = this->milpModel->getModelObjectiveBound();
		mystream << this->bestObjectiveBound << ",";
	}
	else{
		mystream << "-" << ",";
	}
	if (this->milpModel->getModelMipGap() < 100){
		mystream << ((this->bestObjectiveValue - this->bestObjectiveBound)/this->bestObjectiveValue*100.0);
	}
	else{
		mystream << "-";
	}

	mystream << "\n";
	mystream.close();

}

vector<unordered_map<int, double>>& ModelSolver::getBestX(){
	return this->bestXSolution;
}

double ModelSolver::getBestObjectiveValue(){
	return this->bestObjectiveValue;
}
