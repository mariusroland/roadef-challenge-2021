
#pragma once

#include "Common.h"
#include "Resource.h"
#include "Season.h"
#include "Intervention.h"
#include "Exclusion.h"

using namespace std;

class Instance {

	////ATTRIBUTES////

	//Path and instance name
	string path;
	string name;

	//Computational parameters
	bool isReduced;
	bool isAdaptiveAlgorithm;
	bool computeBetterBigM;
	bool fixThreads;
	int numThreads;

	//Computation time
	chrono::time_point<chrono::system_clock> startTimeInitialization;
	double computationalTime = 0;
	double readTime = 0.0;
	double reductionTime = 0;
	double bigMComputationTime;

	//Instance parameters
	int timeHorizon;
	double averageScenarioNumber;
	double quantile;
	double alpha;
	int xVariablesNumber = 0;
	int maxScenarioNumber;

	//Reduced instance parameters
	int minK;
	double reductionPercentage;

	//Instance vectors
	vector<Resource> resources;
	vector<Season> seasons;
	vector<Intervention> interventions;
	vector<unordered_map<int, unordered_map<int, unordered_map<int, double>>>> riskValues;
	vector<unordered_map<int, unordered_map<int, unordered_map<int, double>>>> resourceConsumptions;
	vector<Exclusion> exclusions;
	vector<int> scenarioNumber;

	//Reduced instance data
	vector<unordered_map<int, unordered_map<int, unordered_map<int, double>>>> reducedRiskValues;
	vector<int> reducedScenarioNumber;
	vector<vector<double>> scenarioProbabilities;
	vector<vector<double>> reducedScenarioProbabilities;
	vector<unordered_map<size_t,size_t>> sizeReducedScenarios;
	vector<unordered_map<size_t,size_t>> scenarioRepartition;
	vector<size_t> reclusteredTimeSteps;
	unordered_map<size_t,vector<size_t>> newClusters;

	//Valid Inequalities
	bool oldVICoefficientsComputed;
	bool newVICoefficientsComputed;
	vector<unordered_map<int, unordered_map<int, double>>> oldVICoefficients;
	vector<unordered_map<int, unordered_map<int, double>>> oldReducedVICoefficients;
	unordered_map<int, unordered_map<int, unordered_map<int, unordered_map<int, double>>>> newVICoefficients;

	//BigM for scenario constraints
	vector<vector<double>> bigM;
	vector<vector<double>> reducedBigM;
	vector<int> timeHorizonSeasonLink;
	unordered_map<int, unordered_map<int, set<int>>> exclusionsMap;

public:

	////CONSTRUCTORS AND DESTRUCTORS////

	Instance();
	Instance(string path, string name, bool isAdaptiveAlgorithm, bool computeBetterBigM, bool isReduced, double reductionPercentage, bool fixThreads, int numThreads);
	Instance(const Instance& instance);
	~Instance();

	////METHODS////

	//Reading data simdjson
	void readFile();
	void readMiscellaneous_simdjson(ondemand::document& doc);
	void readScenarioNumbers_simdjson(ondemand::document& doc);
	void readResources_simdjson(map<string, int>& resourceLinks, ondemand::document& doc);
	void readSeasons_simdjson(map<string, int>& seasonLinks, ondemand::document& doc);
	void readInterventions_simdjson(map<string, int>& resourceLinks, map<string, int>& interventionLinks, ondemand::document& doc);
	void readExclusions_simdjson(map<string, int>& seasonLinks, map<string, int>& interventionLinks, ondemand::document& doc);

	//Extract data using the files that were read
	void extractInterventionData();
	void extractProbabilities();
	void extractBigM();
	void extractReducedBigM();
	void extractStrongerBigM();
	void extractBigMKnapsack();
	void extractReducedBigMKnapsack();

	//Adaptive algorithm
	void initializeAdaptiveData(bool averageClustering);
	void apply1DimensionalKMeans(int t, int c, int newScenarioNumber, int givenK, vector<double>& realRisksPerScenario, vector<size_t>& newScenarioRanking);
	void applyKernelDensityEstimation(int t, int c, int newScenarioNumber, vector<double>& realRisksPerScenario, vector<size_t>& newScenarioRanking);
	double gaussianKernel(double x);
	void adaptiveReduction(vector<vector<double>>& realRisksPerTimestepPerScenario, vector<vector<size_t>>& scenarioRankingPerTimestep, vector<vector<double>>& risksPerScenarioPerCluster, vector<size_t>& reclusteredTimeSteps);
	void reclusterUsingNewSplitting(vector<size_t>& reclusteredTimeSteps, bool averageClustering);
	void reduceModel(double percentage);

	//Cut callback VI coefficients
	void computeOldReducedVICoefficients();
	void computeOldVICoefficients();
	void prepareOldVICoefficients(bool forceRecomputation = true);
	void computeNewVICoefficientReducedScenarioSet(vector<size_t> timeSteps, unordered_map<size_t,vector<size_t>>& clusters);
	double computeNewVICoefficientReducedScenario(int t, int c, int tPrime, int intervention);

	//Build the LP program to set better  bigMs
	void addVariables_x(GRBModel& model, vector<unordered_map<int, GRBVar>>& x);
	void addObjective(GRBModel& model, vector<unordered_map<int, GRBVar>>& x, int timeInstant, int scenario);
	void addConstraint_allInterventionsScheduled(GRBModel& model, vector<unordered_map<int, GRBVar>>& x, int timeInstant);
	void addConstraint_resourceBounds(GRBModel& model, vector<unordered_map<int, GRBVar>>& x, int timeInstant);
	void addConstraint_exclusions(GRBModel& model, vector<unordered_map<int, GRBVar>>& x, int timeInstant);

	////GET ATTRIBUTES////

	//File
	string getName();

	//Computational parameters
	bool getIsReduced();

	//Computation time
	double getComputationalTime();
	chrono::time_point<chrono::system_clock> getStartTimeInitialization();
	double getReadTime();
	double getReductionTime();
	double getBigMComputationTime();

	//Instance parameters
	double getQuantile();
	double getAlpha();
	double getMinK();
	int getTimeHorizon();
	double getAverageScenarioNumber();
	int getMaxScenarioNumber();
	int getXVariablesNumber();

	//Instance data
	vector<Resource>& getResources();
	vector<Season>& getSeasons();
	vector<Intervention>& getInterventions();
	vector<Exclusion>& getExclusions();
	vector<unordered_map<int, unordered_map<int, unordered_map<int, double>>>>& getResourceConsumptions();
	vector<unordered_map<int, unordered_map<int, unordered_map<int, double>>>>& getRiskValues(bool returnNonReduced = false);
	vector<int>& getScenarioNumber(bool returnNonReduced = false);
	vector<vector<double>>& getScenarioProbabilities(bool returnNonReduced = false);

	//Big M
	vector<vector<double>>& getBigM(bool returnNonReduced = false);

	//Adaptive algorithm
	unordered_map<size_t,vector<size_t>>& getNewClusters();
	vector<unordered_map<int, unordered_map<int, double>>>& getOldVICoefficients();
	unordered_map<int, unordered_map<int, unordered_map<int, unordered_map<int, double>>>>& getNewVICoefficients();

	////SET ATTRIBUTES////

	//Computational parameters
	void setIsReduced(bool isReduced);

};
