#include "Instance.h"
#include <omp.h>
// #include <mlpack/methods/kmeans/kmeans.hpp>
// #include <mlpack/methods/kmeans/random_partition.hpp>
// #include <mlpack/methods/kmeans/refined_start.hpp>
// #include <armadillo>

///////////////////////CONSTRUCTORS AND DESTRUCTORS//////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////

Instance::Instance() {

}

Instance::Instance(string path, string name, bool isAdaptiveAlgorithm, bool computeBetterBigM, bool isReduced, double reductionPercentage, bool fixThreads, int numThreads) {

	this->name = name;
	this->path = path;
	this->isReduced = isReduced;
	this->reductionPercentage = reductionPercentage;
	this->isAdaptiveAlgorithm = isAdaptiveAlgorithm;
	this->computeBetterBigM = computeBetterBigM;
	this->fixThreads = fixThreads;
	this->numThreads = numThreads;
	this->oldVICoefficientsComputed = false;
}

Instance::Instance(const Instance& instance) {

	this->name = instance.name;

}

Instance::~Instance() {

}


/////////////////////////////METHODS////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////readFile////////////////////////////////////////////////////


void Instance::readFile() {

	chrono::time_point<chrono::system_clock> start1,end1;
	start1 = chrono::system_clock::now();

	//maps to connect names with position in the vectors
	map<string, int> resourceLinks;
	map<string, int> seasonLinks;
	map<string, int> interventionLinks;

	// File parsing
	string comment = this->path + this->name + ".json";

	chrono::time_point<chrono::system_clock> startPars, endPars;
	double parsTime;

	startPars = chrono::system_clock::now();
	ondemand::parser fastParser;
	auto json = padded_string::load(comment);
	ondemand::document doc = fastParser.iterate(json);

	readMiscellaneous_simdjson(doc);
	readResources_simdjson(resourceLinks, doc);
	readSeasons_simdjson(seasonLinks, doc);
	readScenarioNumbers_simdjson(doc);
	readInterventions_simdjson(resourceLinks, interventionLinks, doc);
	readExclusions_simdjson(seasonLinks, interventionLinks, doc);


	endPars = chrono::system_clock::now();
	parsTime = (chrono::duration_cast<chrono::nanoseconds>(endPars - startPars).count()) * 1e-9;
	cout << endl << "PARSE TIME: " << parsTime << endl << endl;

	//Set Intervention data
	extractInterventionData();

	extractProbabilities();

	end1 = chrono::system_clock::now();
	this->readTime = (chrono::duration_cast<chrono::nanoseconds>(end1 - start1).count()) * 1e-9;

	this->startTimeInitialization = end1;

	if (this->fixThreads) {
	  omp_set_num_threads(this->numThreads);
	}

	if (!this->isAdaptiveAlgorithm){
		// if (isReduced) {
		// 	this->reduceModel(this->reductionPercentage);
		// 	start1 = chrono::system_clock::now();
		// 	extractReducedBigM();
		// 	if (this->computeBetterBigM) {
		// 		extractReducedBigMKnapsack();
		// 	}
		// 	end1 = chrono::system_clock::now();
		// }
		// else {
		start1 = chrono::system_clock::now();
		extractBigM();
		if (this->computeBetterBigM) {
			extractBigMKnapsack();
		}
		end1 = chrono::system_clock::now();
		// }
		this->bigMComputationTime = (chrono::duration_cast<chrono::nanoseconds>(end1 - start1).count()) * 1e-9;
	}
	else {
		this->bigMComputationTime = 0.0;
	}



}

////////////////////////////Reading procedure simdjson/////////////////////////////////////////////

void Instance::readMiscellaneous_simdjson(ondemand::document& doc) {

	this->timeHorizon = doc["T"].get_int64();
	this->alpha = doc["Alpha"].get_double();
	this->quantile = doc["Quantile"].get_double();
	this->computationalTime = doc["ComputationTime"].get_double();
	return;

}

void Instance::readScenarioNumbers_simdjson(ondemand::document& doc) {

	this->averageScenarioNumber = 0;
	this->scenarioNumber = vector<int>(this->timeHorizon);
	int time = 0;

	for (auto scenarios : doc["Scenarios_number"].get_array()) {

		this->scenarioNumber[time] = scenarios.get_int64();
		this->averageScenarioNumber += this->scenarioNumber.back();

		if (this->maxScenarioNumber < this->scenarioNumber.back()) {

			this->maxScenarioNumber = this->scenarioNumber.back();

		}

		time++;

	}

	this->averageScenarioNumber /= (double)this->timeHorizon;

	return;

}

void Instance::readResources_simdjson(map<string, int>& resourceLinks, ondemand::document& doc) {

	//READ RESOURCES
	int resourceId = 0;

	for (auto resourceObj : doc["Resources"].get_object()) {

		vector<double> lowerBounds(this->timeHorizon);
		vector<double> upperBounds(this->timeHorizon);
		string resourceName = static_cast<string>(string_view(resourceObj.unescaped_key()));

		//cout << "name " << resourceName << "\n";
		//cout << "name " << resourceObj.value << "\n";
		int timeLB = 0;
		int timeUB = 0;
		//cout << "name " << it.key() << " val " << it.value() << "\n";
		for (auto lb : resourceObj.value()["min"].get_array()) {

			lowerBounds[timeLB] = lb.get_double();
			timeLB++;

		}

		for (auto ub : resourceObj.value()["max"].get_array()) {

			upperBounds[timeUB] = ub.get_double();
			timeUB++;

		}

		resourceLinks.insert(make_pair(resourceName, resourceId));
		Resource resource(resourceId, resourceName, lowerBounds, upperBounds);
		this->resources.push_back(resource);

		resourceId++;


		/*cout << "name " << this->resources.back().getName() << " " << this->resources.back().getId() << "\n";
		for (int i = 0; i < this->timeHorizon; i++) {

			cout << "(lb,ub) " << this->resources.back().getLowerBounds().at(i) << " " << this->resources.back().getUpperBounds().at(i) << "\n";

		}
		cout << "m\n";*/

	}

	return;

}

void Instance::readSeasons_simdjson(map<string, int>& seasonLinks, ondemand::document& doc) {

	//READ SEASONS
	int seasonId = 0;

	for (auto seasonObj : doc["Seasons"].get_object()) {

		vector<int> timeInstants;
		string seasonName = static_cast<string>(string_view(seasonObj.unescaped_key()));

		for (auto time : seasonObj.value().get_array()) {

			int ti = stoi(static_cast<string>(string_view(time))) - 1;
			timeInstants.push_back(ti);

		}

		seasonLinks.insert(make_pair(seasonName, seasonId));
		Season season(seasonId, seasonName, timeInstants);
		this->seasons.push_back(season);

		seasonId++;

		/*cout << "name " << this->seasons.back().getName() << " " << this->seasons.back().getId() << "\n";
		for (int i = 0; i < this->seasons.back().getTimeInstants().size(); i++) {

			cout << "ti " << this->seasons.back().getTimeInstants().at(i) << "\n";

		}
		cout << "\n";*/

	}

}

void Instance::readInterventions_simdjson(map<string, int>& resourceLinks, map<string, int>& interventionLinks, ondemand::document& doc) {

	//READ INTERVENTIONS
	int interventionId = 0;

	//GET INTERVENTION NUMBER
	int interventionNumber = 0;
	for (auto interventionObj : doc["Interventions"].get_object()) {
		interventionNumber++;
	}

	//initializations
	this->riskValues = vector<unordered_map<int, unordered_map<int, unordered_map<int, double>>>>(interventionNumber, unordered_map<int, unordered_map<int, unordered_map<int, double>>>());
	this->resourceConsumptions = vector<unordered_map<int, unordered_map<int, unordered_map<int, double>>>>(interventionNumber, unordered_map<int, unordered_map<int, unordered_map<int, double>>>());


	//Read elements between "..."
	regex rgx("\"([^\"]*)\"");

	for (auto interventionObj : doc["Interventions"].get_object()) {

		vector<int> timeInstants;
		//intervention name, maximum start time, processing times, starting time, time instant
		string interventionName = static_cast<string>(string_view(interventionObj.unescaped_key()));
		//cout << "name " << interventionName << "\n";
		int maximumTime = stoi(static_cast<string>(string_view(interventionObj.value()["tmax"]))) - 1;
		//cout << "tmax " << maximumTime << "\n";
		vector<double> delta(this->timeHorizon, -1);
		int startingTime = -1;
		int timeInstant = -1;
		int resourceId = -1;
		string resourceName;
		double consumption = -1;
		unordered_map<int, unordered_map<int, unordered_map<int, double>>> resourceConsumptionsPerIntervention;
		resourceConsumptionsPerIntervention.rehash(this->resources.size());
		double risk = -1;
		unordered_map<int, unordered_map<int, unordered_map<int, double>>> riskValuesPerIntervention;
		riskValuesPerIntervention.rehash(this->timeHorizon);

		//PROCESSING TIMES
		int deltaPosition = 0;
		for (auto processingTime : interventionObj.value()["Delta"].get_array()) {

			delta[deltaPosition] = processingTime.get_double();
			deltaPosition++;

		}

		Intervention intervention(interventionId, interventionName, maximumTime, delta);
		this->interventions.push_back(intervention);
		interventionLinks.insert(make_pair(interventionName, interventionId));

		//RESOURCE CONSUMPTIONS
		for (auto workload : interventionObj.value()["workload"].get_object()) {

			resourceName = static_cast<string>(string_view(workload.unescaped_key()));
			resourceId = resourceLinks[resourceName];
			//cout << "res " << resourceId << " " << resourceName << "\n";
			unordered_map<int, unordered_map<int, double>> consumptionsPerResource;
			consumptionsPerResource.rehash(this->timeHorizon);

			for (auto time : workload.value().get_object()) {

				timeInstant = stoi(static_cast<string>(string_view(time.unescaped_key()))) - 1;
				//cout << "time " << timeInstant << "\n";
				unordered_map<int, double> consumptionsPerResourcePerTime;
				consumptionsPerResourcePerTime.rehash(this->timeHorizon);

				for (auto start : time.value().get_object()) {

					startingTime = stoi(static_cast<string>(string_view(start.unescaped_key()))) - 1;
					//cout << "start " << startingTime << " val " << consumption << "\n";
					consumptionsPerResourcePerTime.insert(make_pair(startingTime, move(start.value().get_double())));

				}

				consumptionsPerResource.insert(make_pair(timeInstant, move(consumptionsPerResourcePerTime)));

			}

			resourceConsumptionsPerIntervention.insert(make_pair(resourceId, move(consumptionsPerResource)));

		}


		this->resourceConsumptions[interventionId] = move(resourceConsumptionsPerIntervention);


		//RISK
		for (auto riskObject : interventionObj.value()["risk"].get_object()) {

			timeInstant = stoi(static_cast<string>(string_view(riskObject.unescaped_key()))) - 1;
			unordered_map<int, unordered_map<int, double>> risksPerTime;
			risksPerTime.rehash(this->timeHorizon);
			//cout << "ti " << timeInstant << "\n";

			for (auto start : riskObject.value().get_object()) {

				startingTime = stoi(static_cast<string>(string_view(start.unescaped_key()))) - 1;
				unordered_map<int, double> risksPerTimePerStart;
				risksPerTimePerStart.rehash(this->scenarioNumber[timeInstant]);
				//cout << "st " << startingTime << "\n";

				int scenarioNumber = 0;
				for (auto scenarioRisk : start.value().get_array()) {

					//cout << "sce "<<scenarioNumber<<" risk "<< scenarioRisk.get_double() << "\n";
					risksPerTimePerStart.insert(make_pair(scenarioNumber, move(scenarioRisk.get_double())));

					scenarioNumber++;

				}

				risksPerTime.insert(make_pair(startingTime, move(risksPerTimePerStart)));

			}

			riskValuesPerIntervention.insert(make_pair(timeInstant, move(risksPerTime)));

		}

		this->riskValues[interventionId] = move(riskValuesPerIntervention);

		interventionId++;

	}


}

void Instance::readExclusions_simdjson(map<string, int>& seasonLinks, map<string, int>& interventionLinks, ondemand::document& doc) {

	//Diego
	this->timeHorizonSeasonLink = vector<int>(this->timeHorizon, 0);

	for (int seas = 0; seas < this->seasons.size(); ++seas) {

		this->exclusionsMap.insert(make_pair(this->seasons.at(seas).getId(), unordered_map<int, set<int>>()));

		for (int t = 0; t < this->seasons.at(seas).getTimeInstants().size(); ++t) {

			this->timeHorizonSeasonLink.at(this->seasons.at(seas).getTimeInstants().at(t)) = this->seasons.at(seas).getId();

		}

	}

	//READ EXCLUSIONS
	int exclusionId = 0;
	string exclusionName;

	for (auto exclusionObj : doc["Exclusions"].get_object()) {

		exclusionName = static_cast<string>(string_view(exclusionObj.unescaped_key()));
		vector<string> data(3);

		int position = 0;
		for (auto excl : exclusionObj.value().get_array()) {

			data[position] = static_cast<string>(string_view(excl));
			position++;

		}

		/*cout << "exc " << exclusionName << "\n";
		cout << "i1 " << data[0] << "\n";
		cout << "i2 " << data[1] << "\n";
		cout << "s " << data[2] << "\n";*/

		Exclusion exclusion(exclusionId, exclusionName, &this->interventions[interventionLinks[data[0]]], &this->interventions[interventionLinks[data[1]]], &this->seasons[seasonLinks[data[2]]]);
		this->exclusions.push_back(exclusion);

		exclusionId++;

	}

}



//////////////////////////EXTRACT///////////////////////////////////////////////////////////

void Instance::extractInterventionData() {

	//in process and feasible starting times
	for (int i = 0; i < this->interventions.size(); i = i + 1) {

		vector<vector<int>> inProcess;
		vector<int> feasibleStartingTimes;

		//for each time instant
		for (int t = 0; t < this->timeHorizon; t = t + 1) {

			if (t + this->interventions[i].getProcessingTimes().at(t) <= this->timeHorizon) {

				feasibleStartingTimes.push_back(t);
				this->xVariablesNumber++;

			}

		}

#ifdef _DEBUG
		if (feasibleStartingTimes.back() != this->interventions[i].getMaximumTime()) {

			cout << "LINE " << __LINE__ << " WARNING: intervention " << this->interventions[i].getId() << " mismatch between maximum time " << this->interventions[i].getMaximumTime() << " and feasible starting time " << feasibleStartingTimes.back() << endl;

		}
#endif // _DEBUG

		//for each time instant
		for (int t = 0; t < this->timeHorizon; t = t + 1) {

			//check among the feasible starting times
			vector<int> startingTimes;
			for (int s = 0; s < feasibleStartingTimes.size(); s = s + 1) {

				int start = feasibleStartingTimes.at(s);

				if (start <= t && t < start + this->interventions[i].getProcessingTimes().at(start)) {

					startingTimes.push_back(start);

				}

			}

			inProcess.push_back(startingTimes);

		}


		this->interventions[i].getFeasibleStartingTimes() = feasibleStartingTimes;
		this->interventions[i].getInProcess() = inProcess;

	}

}


void Instance::extractProbabilities() {
  this->scenarioProbabilities = vector<vector<double>>(this->timeHorizon,vector<double>(0,0.0));
  for (int i = 0; i < this->timeHorizon; i = i + 1) {

    this->scenarioProbabilities[i].resize(this->scenarioNumber[i],0.0);

    for (int j = 0; j < this->scenarioNumber[i]; j = j + 1) {
      this->scenarioProbabilities[i][j] = 1.0/(double)this->scenarioNumber[i];
    }
  }
}


void Instance::extractBigM() {

	for (int i = 0; i < this->timeHorizon; i = i + 1) {

		vector<double> riskPerTime;

		for (int j = 0; j < this->scenarioNumber[i]; j = j + 1) {

			double riskSum = 0;

			for (int k = 0; k < this->interventions.size(); k = k + 1) {

				Intervention* intervention = &this->interventions[k];
				double highestRisk = 0;

				for (int l = 0; l < intervention->getInProcess().at(i).size(); l = l + 1) {

					int startingTime = intervention->getInProcess().at(i).at(l);

					try {

						if (this->riskValues[intervention->getId()][i][startingTime][j] > highestRisk + epsilonValue) {

							highestRisk = this->riskValues[intervention->getId()][i][startingTime][j];

						}

					}
					catch (out_of_range ex) {



					}

				}

				riskSum += highestRisk;

			}

			riskPerTime.push_back(riskSum);

		}

		vector<double> maxBigMs(riskPerTime.size(), *max_element(riskPerTime.begin(), riskPerTime.end()));
		//this->bigM.push_back(riskPerTime);
		this->bigM.push_back(maxBigMs);

	}

}

void Instance::extractReducedBigM() {

	this->reducedBigM.clear();
	for (int i = 0; i < this->timeHorizon; i = i + 1) {

		vector<double> riskPerTime;

		for (int j = 0; j < this->reducedScenarioNumber[i]; j = j + 1) {

			double riskSum = 0;

			for (int k = 0; k < this->interventions.size(); k = k + 1) {

				Intervention* intervention = &this->interventions[k];
				double highestRisk = 0;

				for (int l = 0; l < intervention->getInProcess().at(i).size(); l = l + 1) {

					int startingTime = intervention->getInProcess().at(i).at(l);

					try {

						if (this->reducedRiskValues[intervention->getId()][i][startingTime][j] > highestRisk + epsilonValue) {

							highestRisk = this->reducedRiskValues[intervention->getId()][i][startingTime][j];

						}

					}
					catch (out_of_range ex) {



					}

				}

				riskSum += highestRisk;

			}

			riskPerTime.push_back(riskSum);

		}

		vector<double> maxBigMs(riskPerTime.size(), *max_element(riskPerTime.begin(), riskPerTime.end()));
		//this->bigM.push_back(riskPerTime);
		this->reducedBigM.push_back(maxBigMs);

	}

}

void Instance::extractStrongerBigM() {

	for (int i = 0; i < this->timeHorizon; i = i + 1) {

		vector<double> timeInstantBigM;

		for (int j = 0; j < this->scenarioNumber[i]; j = j + 1) {

			if (i == 1 && j == 0) {
				//Gurobi environment, model and solver definition
				GRBEnv environment = GRBEnv();
				GRBModel model = GRBModel(environment);

				//Variables
				vector<unordered_map<int, GRBVar>> x;
				addVariables_x(model, x);
				addObjective(model, x, i, j);
				addConstraint_allInterventionsScheduled(model, x, i);
				addConstraint_resourceBounds(model, x, i);
				addConstraint_exclusions(model, x, i);

				string name = to_string(i) + "_" + to_string(j);
				model.write("model" + name + ".lp");

				model.optimize();

				//PRINT SOLUTION
				double opt = 0;
				for (int k = 0; k < this->interventions.size(); k = k + 1) {

					Intervention* intervention = &this->interventions[k];

					for (int l = 0; l < intervention->getFeasibleStartingTimes().size(); l = l + 1) {

						int start = intervention->getFeasibleStartingTimes().at(l);

						if (x[intervention->getId()][start].get(GRB_DoubleAttr_X) > epsilonValue) {

							cout << intervention->getName() << " " << intervention->getId() << " time " << start << " val "<< x[intervention->getId()][start].get(GRB_DoubleAttr_X) << "\n";
							opt += x[intervention->getId()][start].get(GRB_DoubleAttr_X) * this->riskValues[intervention->getId()][i][start][j];

						}

					}

				}

				for (int c = 0; c < this->resources.size(); c = c + 1) {

					double res = 0;
					for (int k = 0; k < this->interventions.size(); k = k + 1) {

						Intervention* intervention = &this->interventions[k];

						for (int l = 0; l < intervention->getFeasibleStartingTimes().size(); l = l + 1) {

							int start = intervention->getFeasibleStartingTimes().at(l);

							if (x[intervention->getId()][start].get(GRB_DoubleAttr_X) > epsilonValue) {

								//cout << intervention->getName() << " " << intervention->getId() << " time " << l << "\n";
								res += x[intervention->getId()][start].get(GRB_DoubleAttr_X) * this->resourceConsumptions[intervention->getId()][this->resources.at(c).getId()][i][start];

							}

						}

					}

					cout << "RESOURCE " << this->resources.at(c).getName() << " " << this->resources.at(c).getId() << "\n";
					cout << "res " << this->resources.at(c).getLowerBounds().at(i) << " " << res << " " << this->resources.at(c).getUpperBounds().at(i) << "\n";
					if (res< this->resources.at(c).getLowerBounds().at(i) - epsilonValue) {
						cout << "LB "<< this->resources.at(c).getLowerBounds().at(i) << " " << res << " " << this->resources.at(c).getUpperBounds().at(i) << "\n";
					}
					if (res > this->resources.at(c).getUpperBounds().at(i) + epsilonValue) {
						cout << "UB " << this->resources.at(c).getLowerBounds().at(i) << " " << res << " " << this->resources.at(c).getUpperBounds().at(i) << "\n";
					}

				}

				timeInstantBigM.push_back(model.get(GRB_DoubleAttr_ObjVal));
				cout << "VALUE " << model.get(GRB_DoubleAttr_ObjVal) <<" opt "<<opt<< "\n";
				model.terminate();
			}

		}

		this->bigM.push_back(timeInstantBigM);

	}


	ofstream stream; stream.open("bigM_analysis_MILP" + this->name + ".dat", ios::app);
	stream << "********** MODEL ********** " << endl;
	double av = 0;
	int count = 0;
	for (int i = 0; i < this->bigM.size(); ++i) {
		stream << "t " << i;
		for (int j = 0; j < this->bigM.at(i).size(); ++j) {
			stream << "\t" << this->bigM.at(i).at(j);
			av += this->bigM.at(i).at(j);
			count++;
		}
		stream << endl;
	}
	stream.close();
	stream.open("bigM_analysis.dat", ios::app);
	stream << "\t" << av / count;
	stream.close();

}


void Instance::extractBigMKnapsack() {

	//this->bigM.clear();
	/*for (int t = 0; t < timeHorizon.size(); ++t) {
		this->bigM.push_back(vector<double>(scenarioNumber.at(t), DBL_MAX));
	}*/


	for (int t = 0; t < timeHorizon; ++t) {
		int seasId = this->timeHorizonSeasonLink.at(t);
		for (int c = 0; c < resources.size(); ++c) {
			for (int s = 0; s < scenarioNumber.at(t); ++s) {
				if (t == 1 && s == 0 && c == 8)
					int a = 0;
				vector<pair<double, pair<int, int>>> sigma;
				for (int i = 0; i < interventions.size(); ++i) {
					//among all the t1 during which intervention i can start to be active at t, we get the most efficient (from a KP point of view)
					//this is done to respect constraint (1b)
					if (interventions.at(i).getInProcess().at(t).size() > 0) {
						double locSig = 0;
						int loct1 = 0;
						for (int t1 = 0; t1 < interventions.at(i).getInProcess().at(t).size(); ++t1) {
							locSig = riskValues.at(i).at(t).at(interventions.at(i).getInProcess().at(t).at(t1)).at(s) / resourceConsumptions.at(i).at(c).at(t).at(interventions.at(i).getInProcess().at(t).at(t1));
							loct1 = t1;
							sigma.push_back(make_pair(locSig, make_pair(i, loct1)));
						}

					}
				}
				sort(sigma.begin(), sigma.end());

				double resCons = 0;
				double bigm = 0;
				int currPos = sigma.size() - 1;

				int currInt = sigma.at(currPos).second.first;
				int currT1 = sigma.at(currPos).second.second;

				set<int> exclIds;

				while (resCons + resourceConsumptions.at(currInt).at(c).at(t).at(interventions.at(currInt).getInProcess().at(t).at(currT1))
					<= resources.at(c).getUpperBounds().at(t)) {

					bigm += riskValues.at(currInt).at(t).at(interventions.at(currInt).getInProcess().at(t).at(currT1)).at(s);
					resCons += resourceConsumptions.at(currInt).at(c).at(t).at(interventions.at(currInt).getInProcess().at(t).at(currT1));
					currPos--;
					if (currPos == -1) break;
					currInt = sigma.at(currPos).second.first;
					currT1 = sigma.at(currPos).second.second;
				}
				double coeff = 0;
				if (currPos >= 0) {
					coeff = (resources.at(c).getUpperBounds().at(t) - resCons) / resourceConsumptions.at(currInt).at(c).at(t).at(interventions.at(currInt).getInProcess().at(t).at(currT1));
					bigm += coeff * riskValues.at(currInt).at(t).at(interventions.at(currInt).getInProcess().at(t).at(currT1)).at(s);
					resCons += coeff * resourceConsumptions.at(currInt).at(c).at(t).at(interventions.at(currInt).getInProcess().at(t).at(currT1));
				}
				if (bigm < this->bigM.at(t).at(s)) {
					this->bigM.at(t).at(s) = bigm;
				}
			}
		}
	}
	return;
}

////////////////////////////calculateReducedBigMKnapsack///////////////////////////////////////////////

void Instance::extractReducedBigMKnapsack() {

	//this->bigM.clear();
	/*for (int t = 0; t < timeHorizon.size(); ++t) {
		this->bigM.push_back(vector<double>(scenarioNumber.at(t), DBL_MAX));
	}*/


	for (int t = 0; t < timeHorizon; ++t) {
		int seasId = this->timeHorizonSeasonLink.at(t);
		for (int c = 0; c < resources.size(); ++c) {
			for (int s = 0; s < reducedScenarioNumber.at(t); ++s) {
				if (t == 1 && s == 0 && c == 8)
					int a = 0;
				vector<pair<double, pair<int, int>>> sigma;
				for (int i = 0; i < interventions.size(); ++i) {
					//among all the t1 during which intervention i can start to be active at t, we get the most efficient (from a KP point of view)
					//this is done to respect constraint (1b)
					if (interventions.at(i).getInProcess().at(t).size() > 0) {
						double locSig = 0;
						int loct1 = 0;
						for (int t1 = 0; t1 < interventions.at(i).getInProcess().at(t).size(); ++t1) {
							locSig = reducedRiskValues.at(i).at(t).at(interventions.at(i).getInProcess().at(t).at(t1)).at(s) / resourceConsumptions.at(i).at(c).at(t).at(interventions.at(i).getInProcess().at(t).at(t1));
							loct1 = t1;
							sigma.push_back(make_pair(locSig, make_pair(i, loct1)));
						}

					}
				}
				sort(sigma.begin(), sigma.end());

				double resCons = 0;
				double bigm = 0;
				int currPos = sigma.size() - 1;

				int currInt = sigma.at(currPos).second.first;
				int currT1 = sigma.at(currPos).second.second;

				set<int> exclIds;

				while (resCons + resourceConsumptions.at(currInt).at(c).at(t).at(interventions.at(currInt).getInProcess().at(t).at(currT1))
					<= resources.at(c).getUpperBounds().at(t)) {

					bigm += reducedRiskValues.at(currInt).at(t).at(interventions.at(currInt).getInProcess().at(t).at(currT1)).at(s);
					resCons += resourceConsumptions.at(currInt).at(c).at(t).at(interventions.at(currInt).getInProcess().at(t).at(currT1));
					currPos--;
					if (currPos == -1) break;
					currInt = sigma.at(currPos).second.first;
					currT1 = sigma.at(currPos).second.second;
				}
				double coeff = 0;
				if (currPos >= 0) {
					coeff = (resources.at(c).getUpperBounds().at(t) - resCons) / resourceConsumptions.at(currInt).at(c).at(t).at(interventions.at(currInt).getInProcess().at(t).at(currT1));
					bigm += coeff * reducedRiskValues.at(currInt).at(t).at(interventions.at(currInt).getInProcess().at(t).at(currT1)).at(s);
					resCons += coeff * resourceConsumptions.at(currInt).at(c).at(t).at(interventions.at(currInt).getInProcess().at(t).at(currT1));
				}
				if (bigm < this->reducedBigM.at(t).at(s)) {
					this->reducedBigM.at(t).at(s) = bigm;
				}
			}
		}
	}
	return;
}

////////////////////////////ADAPTIVE ALGORITHM/////////////////////////////////////////////////

void Instance::initializeAdaptiveData(bool averageClustering){
	//Initilize data and save it
	this->reducedScenarioNumber = vector<int>(this->timeHorizon,1);
	this->reducedScenarioProbabilities = vector<vector<double>> (this->timeHorizon,vector<double>(1,1.0));
	this->reducedRiskValues  = this->riskValues;
	this->sizeReducedScenarios = vector<unordered_map<size_t,size_t>> (this->timeHorizon, unordered_map<size_t,size_t>());
	this->scenarioRepartition = vector<unordered_map<size_t,size_t>> (this->timeHorizon, unordered_map<size_t,size_t> ());

	//Algo for optimal splitting computation
	for (int t = 0; t < this->timeHorizon; t = t + 1) {

			for (int s = 0; s < this->scenarioNumber[t]; s++){

				this->scenarioRepartition[t][s] = 0;

			}
			this->sizeReducedScenarios[t][0] = this->scenarioNumber[t];
			this->newClusters[t] = vector<size_t>(1,0.0);
	}

	vector<size_t> reclusteredTimeSteps (this->timeHorizon);
	iota (begin(reclusteredTimeSteps), end(reclusteredTimeSteps), 0);
	this->reclusterUsingNewSplitting(reclusteredTimeSteps,averageClustering);
	this->isReduced = true;

	// this->reduceModel(0.30);
	// this->extractReducedBigM();
}


void Instance::adaptiveReduction(vector<vector<double>>& realRisksPerTimestepPerScenario, vector<vector<size_t>>& scenarioRankingPerTimestep, vector<vector<double>>& risksPerTimestepPerCluster, vector<size_t>& reclusteredTimeSteps){

	this->newClusters.clear();

	#pragma omp parallel for
	for(int i = 0; i < reclusteredTimeSteps.size(); i++){

		//Identify the reclustered time step
		int t = reclusteredTimeSteps[i];
		//cout<<endl<<"Time = "<<t<<endl;

		//Prepare newcluster map
		this->newClusters[t] = vector<size_t>();

		//Identify cluster that contains quantile
		int indexPlaceQuantile = ceil(this->quantile*this->scenarioNumber[t]) - 1;
		int indexScenarioQuantile = scenarioRankingPerTimestep.at(t).at(indexPlaceQuantile);
		int clusterIndexContainingQuantileScenario = this->scenarioRepartition[t][indexScenarioQuantile];
		double QuantileValue = realRisksPerTimestepPerScenario[t][indexScenarioQuantile];

		//Apply splitting for all existing clusters
		int currentScenarioNumber = this->reducedScenarioNumber[t];
		// cout<<endl<<"Time "<<t<<endl;
		// cout<<"Quantile: "<<QuantileValue<<endl;
		// cout<<this->reducedScenarioNumber[t]<<endl;
		for (int c = 0; c < currentScenarioNumber; c++){
			// cout<<"Cluster "<< c <<": "<<endl;
			int newScenarioNumber = this->sizeReducedScenarios[t][c];
			//Variables for splitting check
			double minRisk = maxConstant;
			double maxRisk = 0.0;
			double clusterRisk = risksPerTimestepPerCluster[t][c];
			//Creating new sorted list of scenarios in the splitted cluster and seeing if needs to be splitted
			vector<size_t> newScenarioRanking (newScenarioNumber);
			size_t counterForFillingNewScenarioRanking = 0;
			// cout<<"Scenario risks:"<<endl;
			for (int s = 0; s < this->scenarioNumber[t]; s++){
				if (this->scenarioRepartition[t][scenarioRankingPerTimestep.at(t).at(s)] == c){
					newScenarioRanking[counterForFillingNewScenarioRanking] = scenarioRankingPerTimestep.at(t).at(s);
					counterForFillingNewScenarioRanking++;
					minRisk = min(minRisk,realRisksPerTimestepPerScenario[t][scenarioRankingPerTimestep.at(t).at(s)]);
					maxRisk = max(maxRisk,realRisksPerTimestepPerScenario[t][scenarioRankingPerTimestep.at(t).at(s)]);
					// cout<< realRisksPerTimestepPerScenario[t][scenarioRankingPerTimestep.at(t).at(s)]<<", ";
				}
			}
			// cout<<endl;
			// cout<<"Cluster risk: "<<clusterRisk<<endl;
			// cout<<"Min risk: "<<minRisk<<endl;
			// cout<<"Max risk: "<<maxRisk<<endl;
			bool split = (((clusterRisk <= QuantileValue) && (maxRisk >= QuantileValue)) || ((clusterRisk >= QuantileValue) && (minRisk <= QuantileValue)));

			//Applying splitting algorithm on the identified cluster
			if ((newScenarioNumber > 2) && split){

				//Apply k-means atm with k = 2
				this->applyKernelDensityEstimation(t, c, newScenarioNumber, realRisksPerTimestepPerScenario.at(t), newScenarioRanking);


			}
			else if ((newScenarioNumber == 2) && split){
				this->scenarioRepartition[t][newScenarioRanking.at(1)] = this->reducedScenarioNumber[t];
				this->sizeReducedScenarios[t][this->reducedScenarioNumber[t]] = 1;
				this->sizeReducedScenarios[t][c] = 1;
				this->newClusters[t].push_back(c);
				this->newClusters[t].push_back(this->reducedScenarioNumber[t]);
				this->reducedScenarioNumber[t]++;
				//cout<< realRisksPerTimestepPerScenario.at(t).at(newScenarioRanking.at(1))<<" ; ";
				//cout<< realRisksPerTimestepPerScenario.at(t).at(newScenarioRanking.at(0))<<endl;
				//cout<<"done with one cluster"<<endl;
			}

		}
		// cout<<this->reducedScenarioNumber[t]<<endl;
		this->reducedScenarioProbabilities[t] = vector<double>(this->reducedScenarioNumber[t],0.0);
	}

}

void Instance::apply1DimensionalKMeans(int t, int c, int newScenarioNumber, int givenK, vector<double>& realRisksPerScenario, vector<size_t>& newScenarioRanking){

	//Computing prefix sums
	vector<double> prefixSum (newScenarioNumber,0.0);
	vector<double> prefixQuadraticSum (newScenarioNumber,0.0);
	prefixSum.at(0) = realRisksPerScenario.at(newScenarioRanking.at(0));
	prefixQuadraticSum.at(0) = pow(realRisksPerScenario.at(newScenarioRanking.at(0)),2);
	for (int s = 1; s < newScenarioNumber; s++){
		prefixSum.at(s) = prefixSum.at(s-1) + realRisksPerScenario.at(newScenarioRanking.at(s));
		prefixQuadraticSum.at(s) = prefixQuadraticSum.at(s-1) + pow(realRisksPerScenario.at(newScenarioRanking.at(s)),2.0);
	}

	//Computing cluster cost
	double sum,quadraticSum;
	vector<unordered_map<size_t,double>> clusterCost (newScenarioNumber, unordered_map<size_t,double> ());
	for (int s1 = 0; s1 < newScenarioNumber; s1++){
		for (int s2 = s1; s2 < newScenarioNumber; s2++){
			if (s1 != 0){
				sum = prefixSum[s2] - prefixSum[s1-1];
				quadraticSum = prefixQuadraticSum[s2] - prefixQuadraticSum[s1-1];
			}
			else {
				sum = prefixSum[s2];
				quadraticSum = prefixQuadraticSum[s2];
			}
			clusterCost[s1][s2] = quadraticSum - pow(sum,2.0)/(double (s2-s1+1));
			//cout<<s1<<" "<<s2<<" "<<clusterCost[s1][s2]<<", ";
		}
	}
	//cout<<endl;

	//Parameter initialization
	int kReal = min(givenK,newScenarioNumber);

	//Computing optimal clusterings
	vector<unordered_map<size_t,double>> optimalClusteringCost (kReal, unordered_map<size_t,double> ());
	vector<unordered_map<size_t,size_t>> backtrackingParameter (kReal, unordered_map<size_t,size_t> ());
	for (int s = 0; s < newScenarioNumber; s++){
		optimalClusteringCost[0][s] = clusterCost[0][s];
		backtrackingParameter[0][s] = 0;
	}
	//cout << 0 << " " << optimalClusteringCost[0][newScenarioNumber-1]<< ", ";
	for (int k = 1; k < kReal;  k++){
		for (int s2 = k; s2 < newScenarioNumber; s2++){
			optimalClusteringCost[k][s2] = optimalClusteringCost[k-1][k-1] + clusterCost[k][s2];
			backtrackingParameter[k][s2] = k;
			for (int s1 = k+1; s1 <= s2; s1++){
				if (optimalClusteringCost[k-1][s1-1] + clusterCost[s1][s2] < optimalClusteringCost[k][s2]){
					optimalClusteringCost[k][s2] = optimalClusteringCost[k-1][s1-1] + clusterCost[s1][s2];
					backtrackingParameter[k][s2] = s1;
				}
			}
			if (s2 == newScenarioNumber - 1){
				//cout << k << " " << backtrackingParameter[k][s2] << " " << optimalClusteringCost[k][s2] << ", ";
			}

		}
	}
	//cout<<endl;

	//Computing the new scenario repartition
	int temporaryK = givenK-1;
	int temporaryBoundary1 = backtrackingParameter[temporaryK][newScenarioNumber-1];
	int temporaryBoundary2 = newScenarioNumber - 1;
	for (int s = newScenarioNumber - 1; (s >= 0); s--){
		if (s > temporaryBoundary1 && temporaryBoundary1 > 0){
			this->scenarioRepartition[t][newScenarioRanking.at(s)] = this->reducedScenarioNumber[t] + temporaryK - 1;
			//cout<< realRisksPerScenario.at(newScenarioRanking.at(s))<<" , ";
		}
		else if (s == temporaryBoundary1 && temporaryBoundary1 > 0){
			this->scenarioRepartition[t][newScenarioRanking.at(s)] = this->reducedScenarioNumber[t]+temporaryK-1;
			this->sizeReducedScenarios[t][this->reducedScenarioNumber[t]+temporaryK-1] = temporaryBoundary2 - temporaryBoundary1 + 1;
			//cout<< realRisksPerScenario.at(newScenarioRanking.at(s))<<" ; ";
			temporaryK--;
			temporaryBoundary2 = temporaryBoundary1 - 1;
			temporaryBoundary1 = backtrackingParameter[temporaryK][temporaryBoundary2];

		}
		else {
			//cout<< realRisksPerScenario.at(newScenarioRanking.at(s))<< " , ";
		}
	}
	//cout<<endl;
	this->sizeReducedScenarios[t][c] = temporaryBoundary2 - temporaryBoundary1 + 1;
	this->reducedScenarioNumber[t] = this->reducedScenarioNumber[t] + givenK - 1;
	//cout<<"done with one cluster"<<endl;

}


void Instance::applyKernelDensityEstimation(int t, int c, int newScenarioNumber, vector<double>& realRisksPerScenario, vector<size_t>& newScenarioRanking){

	//Extract important information from data
	double minRisk  = realRisksPerScenario.at(newScenarioRanking.at(0));
	double maxRisk  = realRisksPerScenario.at(newScenarioRanking.at(newScenarioNumber - 1));

	//Compute important parameters
	int evaluationPointsNumber = newScenarioNumber * 3;
	double deltaX = (maxRisk - minRisk)/(evaluationPointsNumber-1);

	//Std computation
	double sum, mean, sqDiffSum, stDev = 0.0;
	for(int s  = 0; s < newScenarioNumber; s++){
		sum +=  realRisksPerScenario.at(newScenarioRanking.at(s));
	}
	mean = sum/newScenarioNumber;
	for(int s  = 0; s < newScenarioNumber; s++){
		sqDiffSum +=  pow(realRisksPerScenario.at(newScenarioRanking.at(s)) - mean, 2);
	}
	stDev = sqrt(sqDiffSum / newScenarioNumber);
	//cout<<"stdev "<< stDev << endl;
	double bandwidth  = 0.5*stDev*pow(newScenarioNumber,-0.2);
	//cout<<"bandwidth "<<bandwidth<<endl;
	double threshold = 1e-20;


	//Applying simple algo for finding minima's
	double currentPoint, temporaryPoint, temporaryDensityEvaluation;
	int minimumIndex = 0;
	int temporaryIndex;
	bool continueAlgorithm;
	vector<double> densityEvaluation (evaluationPointsNumber,0.0);
	vector<double> resultingSplittingIndices;
	for (int i = 0; i < evaluationPointsNumber; i++){

		//Compute the current point
		currentPoint = minRisk + i*deltaX;

		//Starting while loop
		continueAlgorithm = true;
		temporaryIndex = minimumIndex;
		temporaryPoint = realRisksPerScenario.at(newScenarioRanking.at(temporaryIndex));
		while (continueAlgorithm){
			temporaryDensityEvaluation = (1.0/newScenarioNumber/bandwidth)*this->gaussianKernel((currentPoint - temporaryPoint)/bandwidth);
			if (temporaryDensityEvaluation > threshold){
				densityEvaluation.at(i) += temporaryDensityEvaluation;
				temporaryIndex++;
			}
			else if (temporaryDensityEvaluation < threshold && temporaryPoint < currentPoint){
				minimumIndex = temporaryIndex + 1;
				temporaryIndex++;
			}
			else if (temporaryDensityEvaluation < threshold && temporaryPoint > currentPoint){
				continueAlgorithm = false;
			}

			if (temporaryIndex > newScenarioNumber - 1){
				continueAlgorithm = false;
			}
			else {
				temporaryPoint = realRisksPerScenario.at(newScenarioRanking.at(temporaryIndex));
			}

		}

		//Check if decrease in objective and save relevant splitting
		if (i > 1){

			if ( (densityEvaluation.at(i-1) < densityEvaluation.at(i)) && (densityEvaluation.at(i-1) < densityEvaluation.at(i-2)) ){
				resultingSplittingIndices.push_back(currentPoint);
				//cout<<"found minimum "<< currentPoint << " ";
			}
		}

	}
	//cout<<endl;

	//Apply splitting if k >= 2
	if (resultingSplittingIndices.size() > 0){
		int sizeCounter = 0;
		temporaryIndex = 0;
		temporaryPoint = resultingSplittingIndices.at(temporaryIndex);
		bool reachedEndClusters = false;
		for (int s = 0; s < newScenarioNumber; s++){

			currentPoint = realRisksPerScenario.at(newScenarioRanking.at(s));

			if (currentPoint >= temporaryPoint && !reachedEndClusters){


				if (temporaryIndex != 0){
					this->sizeReducedScenarios[t][this->reducedScenarioNumber[t]] = sizeCounter;
					this->newClusters[t].push_back(this->reducedScenarioNumber[t]);
					//cout<<"size " << sizeCounter << " ; ";
					this->reducedScenarioNumber[t]++;

				}
				else {
					this->sizeReducedScenarios[t][c] = sizeCounter;
					this->newClusters[t].push_back(c);
					//cout<<"size " << sizeCounter << " ; ";
				}

				temporaryIndex++;

				if (temporaryIndex < resultingSplittingIndices.size()){
					temporaryPoint = resultingSplittingIndices.at(temporaryIndex);
				}
				else {
					reachedEndClusters = true;
				}
				sizeCounter = 0;

			}

			if (temporaryIndex != 0){
				this->scenarioRepartition[t][newScenarioRanking.at(s)] = this->reducedScenarioNumber[t];
			}

			//cout<< realRisksPerScenario.at(newScenarioRanking.at(s))<<"  ";

			sizeCounter++;

		}
		this->sizeReducedScenarios[t][this->reducedScenarioNumber[t]] = sizeCounter;
		this->newClusters[t].push_back(this->reducedScenarioNumber[t]);
		//cout<<"size " << sizeCounter << endl;
		this->reducedScenarioNumber[t]++;
		//cout<<"HMC "<< this->reducedScenarioNumber[t]<<endl;


	}
	else {
		this->newClusters[t].push_back(c);
		this->newClusters[t].push_back(this->reducedScenarioNumber[t]);
		this->apply1DimensionalKMeans(t, c, newScenarioNumber, 2, realRisksPerScenario, newScenarioRanking);
	}

	//cout<<"done with one cluster"<<endl;


}

double Instance::gaussianKernel(double x){

	double pi = 2 * acos(0.0);

	return (1/sqrt(2*pi))*exp(-pow(x,2)/2);

}


void Instance::reclusterUsingNewSplitting(vector<size_t>& reclusteredTimeSteps, bool averageClustering){

	#pragma omp parallel for
	for(int i = 0; i < reclusteredTimeSteps.size(); i++){

		//Identify the reclustered time step
		int t = reclusteredTimeSteps[i];

		//First probabilities
		for (int reducedScenarioId = 0; reducedScenarioId < this->reducedScenarioNumber[t]; reducedScenarioId++ ){
			this->reducedScenarioProbabilities[t][reducedScenarioId] = ((double)this->sizeReducedScenarios[t][reducedScenarioId])/((double)this->scenarioNumber[t]);
			// cout << this->reducedScenarioProbabilities[t][reducedScenarioId] << endl;
			// cout << endl << "Time" << t <<  " : ScenarioId " << reducedScenarioId << " : probability "<< this->reducedScenarioProbabilities[t][reducedScenarioId] << endl;
		}
		//Second risk values
		for (int scenarioId = 0; scenarioId < this->scenarioNumber[t]; scenarioId = scenarioId + 1) {

			for (int k = 0; k < this->interventions.size(); k = k + 1) {

				Intervention* intervention = &this->interventions[k];
				int interventionId = intervention->getId();
				double minValue;

				for (int l = 0; l < intervention->getInProcess().at(t).size(); l = l + 1) {
					int startingTimeId = intervention->getInProcess().at(t).at(l);

					if (averageClustering) {
						//First empty the copy we made and fill with 0.0
						if (scenarioId == 0) {
							this->reducedRiskValues[interventionId][t][startingTimeId].clear();
							for (int reducedScenarioId = 0; reducedScenarioId < this->reducedScenarioNumber[t]; reducedScenarioId++ ){
								this->reducedRiskValues[interventionId][t][startingTimeId].insert(make_pair(reducedScenarioId,0.0));
							}
						}
						//Add the value to the corresponding cluster
						this->reducedRiskValues[interventionId][t][startingTimeId][this->scenarioRepartition[t][scenarioId]] += this->riskValues[interventionId][t][startingTimeId][scenarioId];
						//Finally devide by the size for taking the mean
						if (scenarioId == this->scenarioNumber[t] - 1) {
							for (int reducedScenarioId = 0; reducedScenarioId < this->reducedScenarioNumber[t]; reducedScenarioId++ ){
								this->reducedRiskValues[interventionId][t][startingTimeId][reducedScenarioId] *= 1.0/(double)(this->sizeReducedScenarios[t][reducedScenarioId]);
							}
						}
					}
					else {
						//First empty the copy we made and fill with max value
						if (scenarioId == 0) {
							this->reducedRiskValues[interventionId][t][startingTimeId].clear();
							for (int reducedScenarioId = 0; reducedScenarioId < this->reducedScenarioNumber[t]; reducedScenarioId++ ){
								this->reducedRiskValues[interventionId][t][startingTimeId].insert(make_pair(reducedScenarioId,maxConstant));
							}
						}
						//Add the value to the corresponding cluster
						minValue = min(this->riskValues[interventionId][t][startingTimeId][scenarioId],this->reducedRiskValues[interventionId][t][startingTimeId][this->scenarioRepartition[t][scenarioId]]);
						this->reducedRiskValues[interventionId][t][startingTimeId][this->scenarioRepartition[t][scenarioId]] = minValue;
					}
				}
			}
		}
	}
	this->extractReducedBigM();
}

// void Instance::reduceModel(double percentage){

// 	cout<<"REDUCING MODEL"<<endl;
// 	chrono::time_point<chrono::system_clock> start, end;
// 	start = chrono::system_clock::now();
// 	//INIT PARAMS AND VECTORS
// 	int maxIter = 10000000;
// 	this->reducedScenarioNumber = vector<int>(this->timeHorizon,0);
// 	this->sizeReducedScenarios = vector<unordered_map<size_t,size_t>> (this->timeHorizon, unordered_map<size_t,size_t>());
// 	this->reducedScenarioProbabilities = vector<vector<double>>(this->timeHorizon,vector<double>(0,0.0));
// 	this->reducedRiskValues  = this->riskValues;
// 	this->scenarioRepartition = vector<unordered_map<size_t,size_t>> (this->timeHorizon, unordered_map<size_t,size_t> ());
// 	this->minK = ceil(1.0/(1.0-this->getQuantile()));

// 	//KMEANS OVER ALL TIMESTEPS
// 	for (int i = 0; i < this->timeHorizon; i = i + 1) {

// 		//Coputation of the number of entries in the scenario vector
// 		int howManyScenarioEntries = 0;
// 		for (int k = 0; k < this->interventions.size(); k = k + 1) {
// 			Intervention* intervention = &this->interventions[k];
// 			int howManyStartTimes = intervention->getInProcess().at(i).size();
// 			howManyScenarioEntries = howManyScenarioEntries + howManyStartTimes;
// 		}
// 		int howManyRealScenarios = this->scenarioNumber[i];
// 		int howManyReducedScenarios = max(percentage*howManyRealScenarios,ceil(1.0/(1.0-this->getQuantile())));

// 		//If the amount of requested scenarios is smaller than the real value
// 		if (howManyRealScenarios <= howManyReducedScenarios) {
// 			//Problem not enough scenarios to be reduced
// 			//The reduced vector should copy the existing scenarios
// 			this->reducedScenarioNumber[i] = howManyRealScenarios;
// 			for (int k = 0; k < this->interventions.size(); k = k + 1) {
// 				Intervention* intervention = &this->interventions[k];
// 				int interventionId = intervention->getId();

// 				for (int l = 0; l < intervention->getInProcess().at(i).size(); l = l + 1) {
// 					int startingTimeId = intervention->getInProcess().at(i).at(l);
// 					//this->reducedRiskValues[interventionId][i][startingTimeId].resize(howManyRealScenarios);
// 				}
// 			}

// 			this->reducedScenarioProbabilities[i].resize(this->scenarioNumber[i],0.0);
// 			for (int j = 0; j < this->scenarioNumber[i]; j = j + 1) {
// 				this->reducedScenarioProbabilities[i][j] = 1.0/(double)this->scenarioNumber[i];
// 			}
// 		}
// 		//If it is not the case
// 		else {
// 			//k is smaller than |St|
// 			//Here we compute and instantiate the relevant vectors and setup the sample matrix for kmeans
// 			this->reducedScenarioNumber[i] = howManyReducedScenarios;
// 			arma::mat scenarioMatrix(howManyScenarioEntries, howManyRealScenarios, arma::fill::zeros);
// 			for (int scenarioId = 0; scenarioId < this->scenarioNumber[i]; scenarioId = scenarioId + 1) {

// 				int fillerIndex = 0;

// 				for (int k = 0; k < this->interventions.size(); k = k + 1) {

// 					Intervention* intervention = &this->interventions[k];
// 					int interventionId = intervention->getId();

// 					for (int l = 0; l < intervention->getInProcess().at(i).size(); l = l + 1) {
// 						int startingTimeId = intervention->getInProcess().at(i).at(l);
// 						if (scenarioId == 0) {
// 							this->reducedRiskValues[interventionId][i][startingTimeId].clear();
// 						}
// 						scenarioMatrix(fillerIndex,scenarioId) = this->riskValues[interventionId][i][startingTimeId][scenarioId];
// 						fillerIndex++;
// 					}
// 				}
// 			}

// 			//We apply k means algo
// 				arma::Row<size_t> clusters;
// 				arma::mat centroids;
// 				mlpack::kmeans::KMeans<mlpack::metric::EuclideanDistance, mlpack::kmeans::RandomPartition> mlpack_kmeans(maxIter);
// 				mlpack_kmeans.Cluster(scenarioMatrix, howManyReducedScenarios, clusters, centroids);
// 				this->reducedScenarioProbabilities[i].resize(howManyReducedScenarios,0.0);
// 				//We extract the parameters
// 				for (int scenarioId = 0; scenarioId < this->scenarioNumber[i]; scenarioId = scenarioId + 1) {
// 					this->reducedScenarioProbabilities[i][clusters(scenarioId)]++;
// 					this->scenarioRepartition[i][scenarioId] = clusters(scenarioId);
// 				}
// 				for (int scenarioId = 0; scenarioId < howManyReducedScenarios; scenarioId++) {
// 					this->sizeReducedScenarios[i][scenarioId] = this->reducedScenarioProbabilities[i][scenarioId];
// 				}
// 				for (int scenarioId = 0; scenarioId < this->reducedScenarioNumber[i]; scenarioId = scenarioId + 1) {

// 					this->reducedScenarioProbabilities[i][scenarioId] = this->reducedScenarioProbabilities[i][scenarioId]/(double) howManyRealScenarios;
// 					int fillerIndex = 0;

// 					for (int k = 0; k < this->interventions.size(); k = k + 1) {

// 						Intervention* intervention = &this->interventions[k];
// 						int interventionId = intervention->getId();
// 						for (int l = 0; l < intervention->getInProcess().at(i).size(); l = l + 1) {
// 							int startingTimeId = intervention->getInProcess().at(i).at(l);
// 							this->reducedRiskValues[interventionId][i][startingTimeId].insert(make_pair(scenarioId, centroids(fillerIndex,scenarioId)));
// 							fillerIndex++;
// 						}
// 					}
// 				}
// 		}
// 	}
// 	this->isReduced = true;
// 	end = chrono::system_clock::now();
// 	this->reductionTime = (chrono::duration_cast<chrono::nanoseconds>	(end - start).count()) * 1e-9;
// }


////////////////////////////CUTCALLBACK/////////////////////////////////////////////////

void Instance::computeOldReducedVICoefficients() {

	//initialize container
	this->oldReducedVICoefficients = vector<unordered_map<int, unordered_map<int, double>>>(this->interventions.size(), unordered_map<int, unordered_map<int, double>>());

	//fill the container
	for (int i = 0; i < this->interventions.size(); i++) {

		Intervention* intervention = &this->interventions.at(i);
		unordered_map<int, unordered_map<int, double>> coefficientVIPerIntervention;
		coefficientVIPerIntervention.rehash(this->timeHorizon);
		//cout << intervention->getName() << "\n";

		for (int t = 0; t < this->timeHorizon; t++) {

			unordered_map<int, double> coefficientVIPerTime;
			coefficientVIPerTime.rehash(this->timeHorizon);
			int quantilePosition = ceil(this->quantile * (double)this->scenarioNumber.at(t));
			//cout << "time " << t << " " << this->scenarioNumber.at(t) << " coeff " << quantilePosition << "\n";

			for (int s = 0; s < intervention->getInProcess().at(t).size(); s++) {

				int start = intervention->getInProcess().at(t).at(s);
				//cout << "start " << start << "\n";

				vector<double> riskIndexVector(this->reducedScenarioNumber.at(t));
				iota(riskIndexVector.begin(),riskIndexVector.end(),0);
				stable_sort(riskIndexVector.begin(),riskIndexVector.end(), [&](int i,int j){return this->reducedRiskValues[intervention->getId()][t][start][i] < this->reducedRiskValues[intervention->getId()][t][start][j];} );

				//sort + average below the quantile
				double averageBelowQuantile = 0.0;
				int currentSize = 0;
				int additionalSize;
				int scenarioIndex = 0;
				bool kappa = true;

				while (kappa){
					if (currentSize + this->sizeReducedScenarios[t][riskIndexVector[scenarioIndex]] >= quantilePosition){
						additionalSize = quantilePosition - currentSize;
						kappa = false;
					}
					else{
						additionalSize = this->sizeReducedScenarios[t][riskIndexVector[scenarioIndex]];
					}
					averageBelowQuantile += ((double) additionalSize)*this->reducedRiskValues[intervention->getId()][t][start][riskIndexVector[scenarioIndex]];
					currentSize += additionalSize;
					scenarioIndex++;
				}
				averageBelowQuantile /= (double)(quantilePosition);
				coefficientVIPerTime.insert(make_pair(start, averageBelowQuantile));

			}

			coefficientVIPerIntervention.insert(make_pair(t, coefficientVIPerTime));

		}

		this->oldReducedVICoefficients[intervention->getId()] = coefficientVIPerIntervention;

	}


}

void Instance::computeOldVICoefficients() {

	//initialize container
	this->oldVICoefficients = vector<unordered_map<int, unordered_map<int, double>>>(this->interventions.size(), unordered_map<int, unordered_map<int, double>>());

	//fill the container
	for (int i = 0; i < this->interventions.size(); i++) {

		Intervention* intervention = &this->interventions.at(i);
		unordered_map<int, unordered_map<int, double>> coefficientVIPerIntervention;
		coefficientVIPerIntervention.rehash(this->timeHorizon);
		//cout << intervention->getName() << "\n";

		for (int t = 0; t < this->timeHorizon; t++) {

			unordered_map<int, double> coefficientVIPerTime;
			coefficientVIPerTime.rehash(this->timeHorizon);
			int quantilePosition = ceil(this->quantile * (double)this->scenarioNumber.at(t));
			//cout << "time " << t << " " << this->scenarioNumber.at(t) << " coeff " << quantilePosition << "\n";

			for (int s = 0; s < intervention->getInProcess().at(t).size(); s++) {

				int start = intervention->getInProcess().at(t).at(s);
				//cout << "start " << start << "\n";

				vector<double> riskVector(this->scenarioNumber.at(t), -1);

				for (int j = 0; j < this->scenarioNumber.at(t); j++) {

					riskVector[j] = this->riskValues[intervention->getId()][t][start][j];

				}

				//sort + average below the quantile
				sort(riskVector.begin(), riskVector.end());
				double averageBelowQuantile = 0;
				for (int q = 0; q < quantilePosition; q++) {
					averageBelowQuantile += riskVector[q];
				}
				averageBelowQuantile /= (double)(quantilePosition);
				coefficientVIPerTime.insert(make_pair(start, averageBelowQuantile));

			}

			coefficientVIPerIntervention.insert(make_pair(t, coefficientVIPerTime));

		}

		this->oldVICoefficients[intervention->getId()] = coefficientVIPerIntervention;
	}

}

void Instance::prepareOldVICoefficients(bool forceRecomputation){

	if(forceRecomputation || (! this->oldVICoefficientsComputed)){
		if (this->isReduced){
			this->computeOldReducedVICoefficients();
		}
		else{
			this->computeOldVICoefficients();
		}
	}

}

void Instance::computeNewVICoefficientReducedScenarioSet(vector<size_t> timeSteps, unordered_map<size_t,vector<size_t>>& clusters) {

	for (int t_index = 0; t_index < timeSteps.size(); t_index++) {

		int t = timeSteps[t_index];

		for (int c_index = 0; c_index < clusters[t].size(); c_index++) {

			int c = clusters[t][c_index];

			int sizeCluster = this->sizeReducedScenarios[t][c];
			double clusterProbability = this->reducedScenarioProbabilities[t][c];
			double maxProbabilityCut = this->quantile - 1.0 + clusterProbability;

			if (maxProbabilityCut > 0.0) {

				for (int r = 0; r < this->interventions.size(); r++) {

					Intervention* intervention = &this->interventions[r];

					for (int s = 0; s < intervention->getInProcess().at(t).size(); s++) {

						int tPrime = intervention->getInProcess().at(t).at(s);

						this->newVICoefficients[c][t][tPrime][intervention->getId()] = computeNewVICoefficientReducedScenario(c, t, tPrime, intervention->getId());

					}
				}
			}
		}

	}

}

double Instance::computeNewVICoefficientReducedScenario(int c, int t, int tPrime, int intervention){

	int sizeCluster = this->sizeReducedScenarios[t][c];
	double clusterprobability = this->reducedScenarioProbabilities[t][c];
	double maxProbabilityCut = this->quantile - 1.0 + clusterprobability;

	//Saved values for the Cut
	vector<double> riskValues (sizeCluster,0.0);
	vector<double> probabilityValues (sizeCluster,0.0);

	//Counter for going through the scenarios
	int counter = 0;

	//Extracting the risk and probabilities of the scenarios used in the cut
	for (int j = 0; j < this->scenarioNumber.at(t); j++) {

		if (this->scenarioRepartition[t][j] == c) {
			riskValues[counter] = this->riskValues[intervention][t][tPrime][j];
			probabilityValues[counter] = this->scenarioProbabilities[t][j];
			counter++;
		}

	}

	//Sorting the risks from small to large
	vector<double> riskIndexVector(sizeCluster);
	iota(riskIndexVector.begin(),riskIndexVector.end(),0);
	stable_sort(riskIndexVector.begin(),riskIndexVector.end(), [&](int i,int j) {return riskValues[i] < riskValues[j];});


	//Probabilitysum used for computing the coefficients
	double probabilitySum, coefficient = 0.0;
	counter = 0;

	// Running while loop for greedy coefficient computation
	while (probabilitySum < maxProbabilityCut) {

		//Check if we no not exceed the allowed probability
		if (probabilitySum + probabilityValues[riskIndexVector[counter]] <= maxProbabilityCut){
			coefficient += probabilityValues[riskIndexVector[counter]]*riskValues[riskIndexVector[counter]];
		}
		//Else add only what is possible
		else {
			coefficient += (maxProbabilityCut - probabilitySum)*riskValues[riskIndexVector[counter]];
		}

		//Increment the probability and counter
		probabilitySum += probabilityValues[riskIndexVector[counter]];
		counter++;

	}

	return coefficient;
}


////////////////////////////LP FOR BIGMS/////////////////////////////////////////////////

void Instance::addVariables_x(GRBModel& model, vector<unordered_map<int, GRBVar>>& x) {

	for (int i = 0; i < this->interventions.size(); i = i + 1) {

		Intervention* intervention = &this->interventions[i];
		unordered_map<int, GRBVar> starts;

		for (int j = 0; j < intervention->getFeasibleStartingTimes().size(); j = j + 1) {

			int start = intervention->getFeasibleStartingTimes().at(j);

			string name = "x[" + to_string(intervention->getId()) + "," + to_string(start) + "]";
			GRBVar variable = model.addVar(0, 1, 0, GRB_CONTINUOUS, name);

			starts.insert(make_pair(start, variable));

			model.update();

		}

		x.push_back(starts);

	}

}

void Instance::addObjective(GRBModel& model, vector<unordered_map<int, GRBVar>>& x, int timeInstant, int scenario) {

	GRBLinExpr objective = 0;

#ifdef DEBUG
	cout << "LINE " << __LINE__ << " c1 " << constant1 << "\n";
	cout << "LINE " << __LINE__ << " c2 " << constant2 << "\n";
	cout << "LINE " << __LINE__ << " alpha " << this->instance->getAlpha() << "\n";
	cout << "LINE " << __LINE__ << " size " << this->instance->getTimeHorizon().size() << "\n";


#endif // DEBUG

	for (int k = 0; k < this->interventions.size(); k = k + 1) {

		Intervention* intervention = &this->interventions[k];

		for (int l = 0; l < intervention->getInProcess().at(timeInstant).size(); l = l + 1) {

			int startingTime = intervention->getInProcess().at(timeInstant).at(l);

			double riskValue = this->riskValues[intervention->getId()][timeInstant][startingTime][scenario];

			if (riskValue > epsilonValue) {

				objective += riskValue * x[intervention->getId()][startingTime];

			}

		}

	}

	model.setObjective(objective, GRB_MAXIMIZE);

}

void Instance::addConstraint_allInterventionsScheduled(GRBModel& model, vector<unordered_map<int, GRBVar>>& x, int timeInstant) {

	string str;
	string variableName;

	for (int i = 0; i < this->interventions.size(); i = i + 1) {

		Intervention* intervention = &this->interventions[i];
		GRBLinExpr constraint = 0;
		str = "allInterventionsScheduled_" + to_string(intervention->getId());

		for (int j = 0; j < intervention->getInProcess().at(timeInstant).size(); j = j + 1) {

			int startingTime = intervention->getInProcess().at(timeInstant).at(j);

			constraint += x[intervention->getId()][startingTime];

		}

		model.addConstr(constraint <= 1, str);

	}

}

void Instance::addConstraint_resourceBounds(GRBModel& model, vector<unordered_map<int, GRBVar>>& x, int timeInstant) {

	string str;
	string variableName;

	for (int j = 0; j < this->resources.size(); j = j + 1) {

		Resource* resource = &this->resources.at(j);

		GRBLinExpr constraint = 0;
		str = "resourceConsumption_" + to_string(resource->getId()) + "_" + to_string(timeInstant);

		for (int i = 0; i < this->interventions.size(); i = i + 1) {

			Intervention* intervention = &this->interventions.at(i);

			for (int l = 0; l < intervention->getInProcess().at(timeInstant).size(); l = l + 1) {

				int startingTime = intervention->getInProcess().at(timeInstant).at(l);

				if (this->resourceConsumptions[intervention->getId()][resource->getId()][timeInstant][startingTime] > epsilonValue) {


					constraint += this->resourceConsumptions[intervention->getId()][resource->getId()][timeInstant][startingTime] * x[intervention->getId()][startingTime];

				}


			}

		}

		model.addConstr(resource->getLowerBounds().at(timeInstant) <= constraint, str + "_LB");
		model.addConstr(constraint <= resource->getUpperBounds().at(timeInstant), str + "_UB");

	}

}

void Instance::addConstraint_exclusions(GRBModel& model, vector<unordered_map<int, GRBVar>>& x, int timeInstant) {

	string str;

	for (int i = 0; i < this->exclusions.size(); i = i + 1) {

		Exclusion* exclusion = &this->exclusions.at(i);

		//if the season is the one specified in the constraints, insert it
		if (this->timeHorizonSeasonLink[timeInstant] == exclusion->getSeason()->getId()) {

			GRBLinExpr constraint = 0;
			str = "exclusions_" + to_string(exclusion->getIntervention1()->getId()) + "_" + to_string(exclusion->getIntervention2()->getId()) + "_" + to_string(timeInstant);

			for (int k = 0; k < exclusion->getIntervention1()->getInProcess().at(timeInstant).size(); k = k + 1) {

				int startingTime = exclusion->getIntervention1()->getInProcess().at(timeInstant).at(k);

				constraint += x[exclusion->getIntervention1()->getId()][startingTime];

			}

			for (int k = 0; k < exclusion->getIntervention2()->getInProcess().at(timeInstant).size(); k = k + 1) {

				int startingTime = exclusion->getIntervention2()->getInProcess().at(timeInstant).at(k);

				constraint += x[exclusion->getIntervention2()->getId()][startingTime];

			}

			model.addConstr(constraint <= 1, str);

		}

	}

}


////////////////////////GET ATTRIBUTES///////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////

string Instance::getName() {

	return this->name;

}

vector<Resource>& Instance::getResources() {

	return this->resources;

}

vector<Season>& Instance::getSeasons() {

	return this->seasons;

}

vector<Intervention>& Instance::getInterventions() {

	return this->interventions;

}

vector<unordered_map<int, unordered_map<int, unordered_map<int, double>>>>& Instance::getResourceConsumptions() {

	return this->resourceConsumptions;

}

vector<unordered_map<int, unordered_map<int, unordered_map<int, double>>>>& Instance::getRiskValues(bool returnNonReduced) {

  if (returnNonReduced) {
    return this->riskValues;
  }
  if (this->isReduced) {
    return this->reducedRiskValues;
  }
  else {
    return this->riskValues;
  }

}

vector<Exclusion>& Instance::getExclusions() {

	return this->exclusions;

}

vector<int>& Instance::getScenarioNumber(bool returnNonReduced) {

  if (returnNonReduced) {
    return this->scenarioNumber;
  }
  else if (this->isReduced) {
    return this->reducedScenarioNumber;
  }
  else {
    return this->scenarioNumber;
  }

}

vector<vector<double>>& Instance::getScenarioProbabilities(bool returnNonReduced) {

  if (returnNonReduced) {
    return this->scenarioProbabilities;
  }
  else if (this->isReduced) {
    return this->reducedScenarioProbabilities;
  }
  else {
    return this->scenarioProbabilities;
  }

}

int Instance::getTimeHorizon() {

	return this->timeHorizon;

}

double Instance::getAverageScenarioNumber() {

	return this->averageScenarioNumber;

}

double Instance::getQuantile() {

	return this->quantile;

}

double Instance::getAlpha() {

	return this->alpha;

}

int Instance::getMaxScenarioNumber() {

	return this->maxScenarioNumber;

}

double Instance::getComputationalTime() {

	return this->computationalTime;

}

double Instance::getMinK() {

	return this->minK;

}

double Instance::getReadTime() {

	return this->readTime;

}

double Instance::getReductionTime() {

	return this->reductionTime;

}

chrono::time_point<chrono::system_clock> Instance::getStartTimeInitialization(){

  return this->startTimeInitialization;

}

vector<vector<double>>& Instance::getBigM(bool returnNonReduced) {

  if (returnNonReduced) {
    return this->bigM;
  }
  if (this->isReduced) {
    return this->reducedBigM;
  }
  else {
    return this->bigM;
  }
}


double Instance::getBigMComputationTime() {

	return this->bigMComputationTime;

}

int Instance::getXVariablesNumber() {

	return this->xVariablesNumber;

}

bool Instance::getIsReduced() {

	return this->isReduced;

}

unordered_map<size_t,vector<size_t>>& Instance::getNewClusters(){

	return this->newClusters;

}

vector<unordered_map<int, unordered_map<int, double>>>& Instance::getOldVICoefficients() {

	//Then return the relevant coefficients
	if (this->isReduced){
		return this->oldReducedVICoefficients;
	}
	else {
		return this->oldVICoefficients;
	}

}

unordered_map<int, unordered_map<int, unordered_map<int, unordered_map<int, double>>>>& Instance::getNewVICoefficients() {

	return this->newVICoefficients;

}

////////////////////////SET ATTRIBUTES///////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////

void Instance::setIsReduced(bool isReduced) {

  this->isReduced = isReduced;

}
