#pragma once

#include "Common.h"
#include "Instance.h"
#include "MilpModel.h"

using namespace std;

class ModelSolver {

private:

	//ATTRIBUTES

	//Data + Solve stuff
	Instance* instance;
	double givenTimeLimit = 0.0;
	bool useCuts;
	bool useOldVI;
	chrono::time_point<chrono::system_clock> startTime;
	MilpModel* milpModel;

	//Soluton stuff
	double modelSolveTime = 0.0;
	vector<unordered_map<int, double>> bestXSolution;
	vector<unordered_map<size_t, bool>> bestYSolution;
	double bestObjectiveValue;
	double bestObjectiveBound;

	//RoundNumber function for statistics writing
	double roundNumber(double value);

public:

	//CONSTRUCTORS AND DESTRUCTORS
	ModelSolver(Instance* instance, double givenTimeLimit, bool useCuts, bool useOldVI, bool branchingPriorityX, bool fixThreads, int numThreads);
	ModelSolver(const ModelSolver& modelSolver);
	~ModelSolver();

	//METHODS
	void solve();
	double availableComputationTime();
	double requiredOptimalityGap();
	void writeStatistics(string fileNumber, string fileName);

	//GET-SET ATTRIBUTES
	vector<unordered_map<int, double>>& getBestX();
	double getBestObjectiveValue();
};
