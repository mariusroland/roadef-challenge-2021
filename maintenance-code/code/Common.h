
//ALPHABETIC ORDER
#include <algorithm>
#include <cassert>
#include <chrono>
#include <cmath>
#include <cstdlib>
#include <fstream>
#include <functional>
#include "gurobi_c++.h"
#include <iostream>
#include <iterator>
#include <list>
#include <map>
#include <numeric>
#include "simdjson.h"
#include <regex>
#include <set>
#include <string>
#include <sstream>
#include <time.h>
#include <unordered_map>
#include <utility>
#include <vector>
#include <csignal>
#include <errno.h>   // for errno
#include <limits.h>  // for INT_MAX, INT_MIN
#include <stdlib.h>  // for strtol

#define epsilonValue 0.0000001
#define maxConstant 1000000000000000.0//1.7*pow(10,100)

using namespace std;
using namespace simdjson;
