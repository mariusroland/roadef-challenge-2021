#include "MilpModel.h"

///////////////////////CONSTRUCTORS AND DESTRUCTORS//////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////

MilpModel::MilpModel(Instance* instance, bool isIntegralityRelaxedX, bool isIntegralityRelaxedY, bool isExclusionConstraintsRelaxed, bool isQuantileRelaxed, bool branchingPriorityX, bool fixThreads, int numThreads) {

	this->instance = instance;
	this->isIntegralityRelaxedX = isIntegralityRelaxedX;
	this->isIntegralityRelaxedY = isIntegralityRelaxedY;
	this->isExclusionConstraintsRelaxed = isExclusionConstraintsRelaxed;
	this->isQuantileRelaxed = isQuantileRelaxed;
	this->branchingPriorityX = branchingPriorityX;
	this->fixThreads = fixThreads;
	this->numThreads = numThreads;
	this->internalCounter = 0;
	this->counterCutsperTimestep = vector<int> (this->instance->getTimeHorizon(),0);
}

MilpModel::MilpModel(const MilpModel& milpModel) {

	this->instance = milpModel.instance;
	this->isIntegralityRelaxedX = milpModel.isIntegralityRelaxedX;
	this->isIntegralityRelaxedY = milpModel.isIntegralityRelaxedY;
	this->isExclusionConstraintsRelaxed = milpModel.isExclusionConstraintsRelaxed;
	this->isQuantileRelaxed = milpModel.isQuantileRelaxed;
	this->branchingPriorityX = milpModel.branchingPriorityX;
	this->fixThreads = milpModel.fixThreads;
	this->numThreads = milpModel.numThreads;
}

MilpModel::~MilpModel() {
	delete this->environment;
	delete this->model;
}

//////////////////////////////METHODS///////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////

////////////////////////MILP MODEL INIT FUNCTIONS/////////////////////////////////////////////

void MilpModel::initializeAll() {

	//Gurobi environment, model and solver definition
	this->environment = new GRBEnv();
	this->model = new GRBModel(*environment);

	//Computational parameters
	if (this->fixThreads) {
		this->model->set(GRB_IntParam_Threads, this->numThreads);
	}
	//this->model->set(GRB_DoubleParam_NodefileStart, 0.5);

	//VARIABLES INITIALIZATION
	this->addVariables_x();
}


void MilpModel::initializeGeneralMILP() {

	//Time saving
	chrono::time_point<chrono::system_clock> start = chrono::system_clock::now();

	//Basic common setup
	this->initializeAll();

	//Variables
	this->addVariables_y();
	this->addVariables_Q_eps();

	//Variable initialization
	this->model->update();

	//OBJECTIVE FUNCTION
	this->addObjective();
	this->addConstraint_allInterventionsScheduled();
	this->addConstraint_resourceBounds();
	this->addConstraint_exclusions();
	this->reclusteredTimeSteps = vector<size_t> (this->instance->getTimeHorizon());
	iota (begin(this->reclusteredTimeSteps), end(this->reclusteredTimeSteps), 0);
	this->qConstraints = vector<unordered_map<int, GRBConstr>> (this->instance->getTimeHorizon(),unordered_map<int, GRBConstr>());
	this->addConstraint_QDefinition(this->qConstraints,this->reclusteredTimeSteps);
	this->addConstraint_yDefinition(this->yConstraints,this->reclusteredTimeSteps);
	this->addConstraint_epsDefinition(this->epsConstraints,this->reclusteredTimeSteps);

	//Setuptime computation
	chrono::time_point<chrono::system_clock> end = chrono::system_clock::now();
	this->modelBuildTime = (chrono::duration_cast<chrono::nanoseconds>	(end - start).count()) * 1e-9;

}

void MilpModel::initializeFirstMILP_noQuantile() {

	//Time saving
	chrono::time_point<chrono::system_clock> start = chrono::system_clock::now();

	//Basic common setup
	this->initializeAll();

	//Variable initialization
	this->model->update();

	//OBJECTIVE FUNCTION
	this->addObjective();
	this->addConstraint_allInterventionsScheduled();
	this->addConstraint_resourceBounds();
	this->addConstraint_exclusions();

	chrono::time_point<chrono::system_clock> end = chrono::system_clock::now();
	this->modelBuildTime = (chrono::duration_cast<chrono::nanoseconds>	(end - start).count()) * 1e-9;

}

void MilpModel::initializeFirstMILP_Adaptive(bool usingADM, bool averageClustering) {

	//Time saving
	chrono::time_point<chrono::system_clock> start = chrono::system_clock::now();

	//Basic common setup
	this->initializeAll();

	//Initilize reclustering of all time steps
	this->reclusteredTimeSteps = vector<size_t> (this->instance->getTimeHorizon());
	iota (begin(this->reclusteredTimeSteps), end(this->reclusteredTimeSteps), 0);
	this->instance->initializeAdaptiveData(averageClustering);
	this->instance->computeNewVICoefficientReducedScenarioSet(this->reclusteredTimeSteps,this->instance->getNewClusters());

	//Initilize new variables and remove quantile relaxation
	this->isQuantileRelaxed = false;
	this->addVariables_y();
	this->addVariables_Q_eps();
	this->model->update();

	//Add objective and scheduling constraints
	this->addObjective();
	this->addConstraint_allInterventionsScheduled();
	this->addConstraint_resourceBounds();
	this->addConstraint_exclusions();

	//Adding constraints related to the quantile
	this->qConstraints = vector<unordered_map<int, GRBConstr>> (this->instance->getTimeHorizon(),unordered_map<int, GRBConstr>());
	this->addConstraint_QDefinition(this->qConstraints,this->reclusteredTimeSteps);
	this->addConstraint_yDefinition(this->yConstraints,this->reclusteredTimeSteps);
	this->addConstraint_epsDefinition(this->epsConstraints,this->reclusteredTimeSteps);
	// cout<<"hello"<<endl;
	this->allNewVICoefficients = vector<unordered_map<int, unordered_map<int, unordered_map<int, double>>>> (this->instance->getTimeHorizon(),unordered_map<int, unordered_map<int, unordered_map<int, double>>>());
	// cout<<"hello"<<endl;
	this->addConstraintVI_adaptive(this->reclusteredTimeSteps,this->instance->getNewClusters());

	//If ADM is used
	if (usingADM){
		this->initializeYVariableFixing_ADM();
	}

	chrono::time_point<chrono::system_clock> end = chrono::system_clock::now();
	this->modelBuildTime = (chrono::duration_cast<chrono::nanoseconds>	(end - start).count()) * 1e-9;

}

void MilpModel::initializeFirstMILP_ADM(bool viInFirstMILP) {

	chrono::time_point<chrono::system_clock> startTest,endTest;

	//Time saving
	chrono::time_point<chrono::system_clock> start = chrono::system_clock::now();

	//Basic common setup
	this->initializeAll();

	startTest = chrono::system_clock::now();
	//Variable initialization
	this->isQuantileRelaxed = true;
	this->addVariables_y();
	this->addVariables_Q_eps();
	this->model->update();
	endTest = chrono::system_clock::now();
	cout << "Var time: " << (chrono::duration_cast<chrono::nanoseconds> (endTest - startTest).count()) * 1e-9 << endl;

	//Creating an all timesteps vector for the epsconstraints
	vector<size_t> timeSteps (this->instance->getTimeHorizon());
	iota (begin(timeSteps), end(timeSteps), 0);

	startTest = chrono::system_clock::now();
	//Objective
	this->addObjective();
	endTest = chrono::system_clock::now();
	cout << "Obj time: " << (chrono::duration_cast<chrono::nanoseconds> (endTest - startTest).count()) * 1e-9 << endl;

	startTest = chrono::system_clock::now();
	//Scheduling constraints
	this->addConstraint_allInterventionsScheduled();
	this->addConstraint_resourceBounds();
	this->addConstraint_exclusions();
	endTest = chrono::system_clock::now();
	cout << "Sched cons time: " << (chrono::duration_cast<chrono::nanoseconds> (endTest - startTest).count()) * 1e-9 << endl;


	//Adding constraints related to the quantile
	startTest = chrono::system_clock::now();
	this->qConstraints = vector<unordered_map<int, GRBConstr>> (this->instance->getTimeHorizon(),unordered_map<int, GRBConstr>());
	this->addConstraint_QDefinition(this->qConstraints,timeSteps);
	endTest = chrono::system_clock::now();
	cout << "Q cons time: " << (chrono::duration_cast<chrono::nanoseconds> (endTest - startTest).count()) * 1e-9 << endl;
	startTest = chrono::system_clock::now();
	this->addConstraint_yDefinition(this->yConstraints,timeSteps);
	endTest = chrono::system_clock::now();
	cout << "y cons time: " << (chrono::duration_cast<chrono::nanoseconds> (endTest - startTest).count()) * 1e-9 << endl;
	startTest = chrono::system_clock::now();
	this->addConstraint_epsDefinition(this->epsConstraints,timeSteps);
	endTest = chrono::system_clock::now();
	cout << "eps cons time: " << (chrono::duration_cast<chrono::nanoseconds> (endTest - startTest).count()) * 1e-9 << endl;

	//Time
	chrono::time_point<chrono::system_clock> end = chrono::system_clock::now();
	this->modelBuildTime = (chrono::duration_cast<chrono::nanoseconds>	(end - start).count()) * 1e-9;

}

////////////////////////VARIABLE DEFINITIONS/////////////////////////////////////////////

void MilpModel::addVariables_x() {

	//Itilialize variable saving
	this->x = vector<unordered_map<int, GRBVar>>(this->instance->getInterventions().size(), unordered_map<int, GRBVar>());

	for (int i = 0; i < this->instance->getInterventions().size(); i = i + 1) {

		Intervention* intervention = &this->instance->getInterventions().at(i);
		unordered_map<int, GRBVar> starts;
		//cout << "INTER " << intervention->getName()<<" " << intervention->getId()<<" size "<<intervention->getFeasibleStartingTimes().size()<<"\n";

		for (int j = 0; j < intervention->getFeasibleStartingTimes().size(); j = j + 1) {

			int start = intervention->getFeasibleStartingTimes().at(j);

			string name = "x[" + to_string(intervention->getId()) + "," + to_string(start) + "]";

			if (this->isIntegralityRelaxedX) {

				GRBVar variable = this->model->addVar(0, 1, 0, GRB_CONTINUOUS, name);
				starts.insert(make_pair(start, variable));

			}
			else {

				GRBVar variable = this->model->addVar(0, 1, 0, GRB_BINARY, name);
				if (this->branchingPriorityX){
					variable.set(GRB_IntAttr_BranchPriority, 1);
				}
				starts.insert(make_pair(start, variable));

			}

		}

		this->x[intervention->getId()] = starts;
		//this->x.push_back(starts);
		//cout << "size variables " << this->x[intervention->getId()].size() << "\n";

	}

}

void MilpModel::addVariables_y() {

	//First defining the vector for storing
	this->y = vector<unordered_map<int, GRBVar>>(this->instance->getTimeHorizon(), unordered_map<int, GRBVar>());

	for (int i = 0; i < this->instance->getTimeHorizon(); i = i + 1) {

		unordered_map<int, GRBVar> yPerTimeInstant;

		for (int j = 0; j < this->instance->getScenarioNumber().at(i); j = j + 1) {

			pair<int, int> key = make_pair(i, j);
			string name = "y[" + to_string(i) + "," + to_string(j) + "]";

			if (this->isIntegralityRelaxedY) {

				GRBVar variable = this->model->addVar(0, 1, 0, GRB_CONTINUOUS, name);
				yPerTimeInstant.insert(make_pair(j, variable));

			}
			else {

				GRBVar variable = this->model->addVar(0, 1, 0, GRB_BINARY, name);
				yPerTimeInstant.insert(make_pair(j, variable));

			}

		}

		this->y[i] = yPerTimeInstant;

	}

}

void MilpModel::addVariables_Q_eps() {

	//First defining the storing vectors
	this->Q = vector<GRBVar>(this->instance->getTimeHorizon());
	this->eps = vector<GRBVar>(this->instance->getTimeHorizon());

	for (int i = 0; i < this->instance->getTimeHorizon(); i = i + 1) {

		string name = "Q[" + to_string(i) + "]";
		GRBVar variable1 = this->model->addVar(0, GRB_INFINITY, 0, GRB_CONTINUOUS, name);

		this->Q[i] = variable1;

		name = "eps[" + to_string(i) + "]";
		GRBVar variable2 = this->model->addVar(0, GRB_INFINITY, 0, GRB_CONTINUOUS, name);

		this->eps[i] = variable2;

	}

}

void MilpModel::removeVariables_scenarioDependent(vector<unordered_map<int, GRBVar>>& variables, vector<size_t>& reclusteredTimeSteps) {

	for (int i = 0; i < reclusteredTimeSteps.size(); i = i + 1) {

		int t = reclusteredTimeSteps[i];

		for (int j = 0; j < this->instance->getScenarioNumber().at(t); j = j + 1) {

			if (variables[t].count(j) > 0){

				this->model->remove(variables[t][j]);

			}

		}

	}

}




////////////////////////OBJECTIVE/////////////////////////////////////////////////

void MilpModel::addObjective() {

	GRBLinExpr firstTerm = 0;
	GRBLinExpr secondTerm = 0;
	double constant1 = this->instance->getAlpha() * (1 / (double) this->instance->getTimeHorizon());
	double constant2 = (1 - this->instance->getAlpha()) * (1 / (double) this->instance->getTimeHorizon());

	for (int i = 0; i < this->instance->getInterventions().size(); i = i + 1) {

		Intervention* intervention = &this->instance->getInterventions().at(i);
		//cout << "intervention " << intervention->getId() << " "<<intervention->getName() << "\n";

		for (auto it1 = this->instance->getRiskValues(true).at(i).begin(); it1 != this->instance->getRiskValues(true).at(i).end(); it1++) {

			int timeInstant = it1->first;

			for (auto it2 = it1->second.begin(); it2 != it1->second.end(); it2++) {

				int startTime = it2->first;

				for (auto it3 = it2->second.begin(); it3 != it2->second.end(); it3++) {

					double coeff = this->instance->getScenarioProbabilities(true).at(timeInstant).at(it3->first) * it3->second;
					firstTerm.addTerms(&coeff, &this->x[intervention->getId()][startTime], 1);
					//firstTerm += this->instance->getScenarioProbabilities().at(timeInstant).at(it3->first) * it3->second * this->x[intervention->getId()][startTime];

				}

			}

		}

	}


	if (this->isQuantileRelaxed) {

		this->model->setObjective(constant1 * firstTerm, GRB_MINIMIZE);

	}
	else {

		for (int i = 0; i < this->instance->getTimeHorizon(); i = i + 1) {

			secondTerm += this->eps[i];

		}

		this->model->setObjective(constant1*firstTerm + constant2*secondTerm, GRB_MINIMIZE);
	}


}

///////////////////////////CONSTRAINTS///////////////////////////////////////////////////

void MilpModel::addConstraint_allInterventionsScheduled() {

	string str;
	string variableName;

	for (int i = 0; i < this->instance->getInterventions().size(); i = i + 1) {

		Intervention* intervention = &this->instance->getInterventions().at(i);
		GRBLinExpr constraint = 0;
		str = "allInterventionsScheduled_" + to_string(intervention->getId());

		//cout << "inter " << intervention->getName() << "\n";

		for (int j = 0; j < intervention->getFeasibleStartingTimes().size(); j = j + 1) {

			int start = intervention->getFeasibleStartingTimes().at(j);

			constraint += this->x[intervention->getId()][start];

		}

		this->model->addConstr(constraint == 1, str);

	}

}

void MilpModel::addConstraint_resourceBounds() {

	string str;
	string variableName;

	for (int j = 0; j < this->instance->getResources().size(); j = j + 1) {

		Resource* resource = &this->instance->getResources().at(j);

		for (int k = 0; k < this->instance->getTimeHorizon(); k = k + 1) {

			GRBLinExpr constraint = 0;
			str = "resourceConsumption_" + to_string(resource->getId()) + "_" + to_string(k);

			/*int size = this->instance->getInterventions().size() * this->instance->getTimeHorizon();
			GRBLinExpr constraint;
			double* coeffs = (double*)malloc(size * sizeof(double));
			GRBVar* vars = (GRBVar*)malloc(size * sizeof(GRBVar));
			int counter = 0;
			bool isNotEmpty = false;*/

			for (int i = 0; i < this->instance->getInterventions().size(); i = i + 1) {

				Intervention* intervention = &this->instance->getInterventions().at(i);

				for (int l = 0; l < intervention->getInProcess().at(k).size(); l = l + 1) {

					int startingTime = intervention->getInProcess().at(k).at(l);

					//mystream << "int " << intervention->getName() << " res " << resource->getName() << " " << k << " " << startingTime << " val " << instance->getResourceConsumptions().at(intervention->getId()).at(resource->getId()).at(k).at(startingTime) << "\n";

					try {

						if (this->instance->getResourceConsumptions()[intervention->getId()][resource->getId()][k][startingTime] > epsilonValue) {

							/*coeffs[counter] = this->instance->getResourceConsumptions()[intervention->getId()][resource->getId()][k][startingTime];
							vars[counter] = this->x[intervention->getId()][startingTime];
							isNotEmpty = true;
							counter++;*/
							constraint.addTerms(&this->instance->getResourceConsumptions()[intervention->getId()][resource->getId()][k][startingTime], &this->x[intervention->getId()][startingTime], 1);
							//constraint += this->instance->getResourceConsumptions()[intervention->getId()][resource->getId()][k][startingTime] * this->x[intervention->getId()][startingTime];

						}

					}
					catch (out_of_range ex) {



					}

				}

			}

			if (constraint.size() > 0/*isNotEmpty*/) {

				//constraint.addTerms(coeffs, vars, counter);

				this->model->addConstr(resource->getLowerBounds().at(k) <= constraint, str + "_LB");
				this->model->addConstr(constraint <= resource->getUpperBounds().at(k), str + "_UB");

			}

		}

	}

}

void MilpModel::addConstraint_exclusions() {

	string str;

	for (int i = 0; i < this->instance->getExclusions().size(); i = i + 1) {

		Exclusion* exclusion = &this->instance->getExclusions().at(i);

		for (int j = 0; j < exclusion->getSeason()->getTimeInstants().size(); j = j + 1) {

			int timeInstant = exclusion->getSeason()->getTimeInstants().at(j);

			GRBLinExpr constraint = 0;
			str = "exclusions_" + to_string(exclusion->getIntervention1()->getId()) + "_" + to_string(exclusion->getIntervention2()->getId()) + "_" + to_string(timeInstant);

			for (int k = 0; k < exclusion->getIntervention1()->getInProcess().at(timeInstant).size(); k = k + 1) {

				int startingTime = exclusion->getIntervention1()->getInProcess().at(timeInstant).at(k);
				pair<Intervention*, int> key = make_pair(exclusion->getIntervention1(), startingTime);

				constraint += this->x[exclusion->getIntervention1()->getId()][startingTime];

			}

			for (int k = 0; k < exclusion->getIntervention2()->getInProcess().at(timeInstant).size(); k = k + 1) {

				int startingTime = exclusion->getIntervention2()->getInProcess().at(timeInstant).at(k);
				pair<Intervention*, int> key = make_pair(exclusion->getIntervention2(), startingTime);

				constraint += this->x[exclusion->getIntervention2()->getId()][startingTime];

			}

			if (constraint.size() > 0) {

				this->model->addConstr(constraint <= 1, str);

			}
			/*model.update();
			GRBConstr c = model.getConstrByName(str);

			c.set(GRB_IntAttr_Lazy, 1);*/

		}

	}

}

void MilpModel::addConstraint_QDefinition(vector<unordered_map<int,GRBConstr>>& qConstraints, vector<size_t>& timeSteps) {

	string str;

	for (int i = 0; i < timeSteps.size(); i = i + 1) {

		int t = timeSteps[i];

		unordered_map<int, GRBConstr> qConstraintsPerTime;

		for (int j = 0; j < this->instance->getScenarioNumber().at(t); j = j + 1) {

			GRBLinExpr constraint = -this->Q[t];

			double clusterProbability = this->instance->getScenarioProbabilities().at(t).at(j);

			double maxProbabilityCut = this->instance->getQuantile() - 1.0 + clusterProbability;

			if (maxProbabilityCut <= 0.0) {
				constraint += this->instance->getBigM().at(t).at(j) * (this->y[t][j] - 1);
			}

			str = "QDefinition_" + to_string(t) + "_" + to_string(j);

			for (int k = 0; k < this->instance->getInterventions().size(); k = k + 1) {

				Intervention* intervention = &this->instance->getInterventions().at(k);

				for (int l = 0; l < intervention->getInProcess().at(t).size(); l = l + 1) {

					int startingTime = intervention->getInProcess().at(t).at(l);
					constraint.addTerms(&this->instance->getRiskValues().at(intervention->getId()).at(t).at(startingTime).at(j), &this->x[intervention->getId()][startingTime], 1);


				}

			}

			if (constraint.size() > 0) {

				//constraint.addTerms(coeffs, vars, counter);
				//cout << constraint << "\n";
				qConstraintsPerTime.insert(make_pair(j, this->model->addConstr(constraint <= 0, str)));

			}

		}
		qConstraints[t] = qConstraintsPerTime;
	}

}

void MilpModel::addConstraint_yDefinition(unordered_map<int, GRBConstr>& yConstraints, vector<size_t>& timeSteps) {

	string str;

	for (int i = 0; i < timeSteps.size(); i = i + 1) {

		int t = timeSteps[i];
		GRBLinExpr constraint = 0;
		str = "yDefinition_" + to_string(t);

		for (int j = 0; j < this->instance->getScenarioNumber().at(t); j = j + 1) {

			double clusterProbability = this->instance->getScenarioProbabilities().at(t).at(j);
			double maxProbabilityCut = this->instance->getQuantile() - 1.0 + clusterProbability;

			if (maxProbabilityCut <= 0.0) {
				constraint += this->y[t][j] * this->instance->getScenarioProbabilities().at(t).at(j);
			}
			else {
				constraint += this->instance->getScenarioProbabilities().at(t).at(j);
			}

		}

		double value = this->instance->getQuantile();


		yConstraints[t] = this->model->addConstr( constraint >= value, str);

	}

}

void MilpModel::addConstraint_epsDefinition(unordered_map<int, GRBConstr>& epsConstraints, vector<size_t>& timeSteps) {

	string str;

	for (int i = 0; i < timeSteps.size(); i = i + 1) {

		int t = timeSteps[i];

		GRBLinExpr constraint;
		str = "epsDefinition_" + to_string(t);

		for (int j = 0; j < this->instance->getScenarioNumber(true).at(t); j = j + 1) {

			for (int k = 0; k < this->instance->getInterventions().size(); k = k + 1) {

				Intervention* intervention = &this->instance->getInterventions().at(k);

				for (int l = 0; l < intervention->getInProcess().at(t).size(); l = l + 1) {

					int startingTime = intervention->getInProcess().at(t).at(l);
					double coeff = this->instance->getRiskValues(true).at(intervention->getId()).at(t).at(startingTime).at(j) * this->instance->getScenarioProbabilities(true).at(t).at(j);
					constraint.addTerms(&coeff, &this->x[intervention->getId()][startingTime], 1);

				}

			}

		}

		if (constraint.size()>0) {

			constraint += this->eps[t] - this->Q[t];
			if (epsConstraints.find(t) == epsConstraints.end()) {
				epsConstraints.insert( make_pair(t,this->model->addConstr(constraint >= 0, str)));
			}
			else {
				epsConstraints[t] = this->model->addConstr(constraint >= 0, str);
			}


		}

	}

}

void MilpModel::removeConstraints_timeDependent( unordered_map<int, GRBConstr>& constraints, vector<size_t>& reclusteredTimeSteps) {

	for (int i = 0; i < reclusteredTimeSteps.size(); i = i + 1) {
		int t = reclusteredTimeSteps[i];
		if (constraints.count(t) > 0){
			this->model->remove(constraints[t]);
		}
	}

}

void MilpModel::removeConstraints_scenarioDependent(vector<unordered_map<int, GRBConstr>>& constraints, vector<size_t>& reclusteredTimeSteps) {

	for (int i = 0; i < reclusteredTimeSteps.size(); i = i + 1) {

		int t = reclusteredTimeSteps[i];

		for (int j = 0; j < this->instance->getScenarioNumber().at(t); j = j + 1) {

			if (constraints[t].count(j) > 0){

				this->model->remove(constraints[t][j]);

			}


		}

	}

}


////////////////////////VARIABLE FIXING/////////////////////////////////////////////

void MilpModel::addConstraints_yVariableFixing(vector<unordered_map<size_t, bool>>& yValues) {

	string str;

	for (int i = 0; i < this->instance->getTimeHorizon(); i = i + 1) {

		unordered_map<int, GRBConstr> yVariableFixingPerTime;

		for (int j = 0; j < this->instance->getScenarioNumber().at(i); j = j + 1) {

			bool value = yValues[i][j];

			GRBLinExpr constraint = this->y[i][j];
			str = "yValues" + to_string(i) + "_" + to_string(j);

			if (value) {

				yVariableFixingPerTime.insert(make_pair(j, this->model->addConstr(constraint == this->y[i][j].get(GRB_DoubleAttr_UB), str)));

			}
			else {

				yVariableFixingPerTime.insert(make_pair(j, this->model->addConstr(constraint == this->y[i][j].get(GRB_DoubleAttr_LB), str)));

			}

		}

		this->yVariableFixing[i] = yVariableFixingPerTime;

	}

}

void MilpModel::removeConstraints_yVariableFixing() {

	for (int i = 0; i < this->instance->getTimeHorizon(); i = i + 1) {

		for (int j = 0; j < this->instance->getScenarioNumber().at(i); j = j + 1) {

			  this->model->remove(this->yVariableFixing[i][j]);

		}

	}

}


////////////////////////WARMSTARTING/////////////////////////////////////////////


void MilpModel::warmStartXVariable(vector<unordered_map<int, double>>& xValues) {

	for (int i = 0; i < this->instance->getInterventions().size(); i = i + 1) {

		Intervention* intervention = &this->instance->getInterventions().at(i);

		for (int j = 0; j < intervention->getFeasibleStartingTimes().size(); j = j + 1) {

			int start = intervention->getFeasibleStartingTimes().at(j);
			double xValue = xValues[intervention->getId()][start];
			this->x[intervention->getId()][start].set(GRB_DoubleAttr_Start, xValue);

		}

	}

}

void MilpModel::warmStartYVariable(vector<unordered_map<size_t, bool>>& yValues) {

	for (int i = 0; i < this->instance->getTimeHorizon(); i = i + 1) {

		for (int j = 0; j < this->instance->getScenarioNumber().at(i); j = j + 1) {

			if (yValues[i][j]){
				this->y[i][j].set(GRB_DoubleAttr_Start, 1.0);
			}
			else{
				this->y[i][j].set(GRB_DoubleAttr_Start, 0.0);
			}

		}

	}


}

void MilpModel::computeWarmStartYWithX(vector<unordered_map<int, double>>& theXValues, vector<unordered_map<size_t, bool>>& yValues, bool newX){

	if (newX){
		#pragma omp parallel for
		for (int t = 0; t < this->instance->getTimeHorizon(); t = t + 1) {

			yValues[t].clear();

			vector<double> estimatedRiskPerScenario(this->instance->getScenarioNumber().at(t), 0.0);

			for (int j = 0; j < this->instance->getScenarioNumber().at(t); j = j + 1) {

				yValues[t][j] = false;

				double estimatedRiskCurrentScenario = 0.0;

				for (int k = 0; k < this->instance->getInterventions().size(); k++) {

					Intervention* intervention = &this->instance->getInterventions().at(k);

					for (int s = 0; s < intervention->getInProcess().at(t).size(); s++) {

						int startingTime = intervention->getInProcess().at(t).at(s);

						estimatedRiskCurrentScenario += this->instance->getRiskValues().at(intervention->getId()).at(t).at(startingTime).at(j)*theXValues[intervention->getId()][startingTime] ;

					}

				}

				estimatedRiskPerScenario[j] = estimatedRiskCurrentScenario;

			}

			vector<size_t> estimatedIndexRiskPerScenario (this->instance->getScenarioNumber().at(t));
			iota(estimatedIndexRiskPerScenario.begin(),estimatedIndexRiskPerScenario.end(),0);
			sort(estimatedIndexRiskPerScenario.begin(),estimatedIndexRiskPerScenario.end(), [&](int i,int j){return estimatedRiskPerScenario.at(i) < estimatedRiskPerScenario.at(j);} );
			int index = 0;
			double probabilitySum = this->instance->getScenarioProbabilities().at(t).at(estimatedIndexRiskPerScenario.at(index));
			yValues[t][estimatedIndexRiskPerScenario.at(index)] = true;
			while (probabilitySum < (this->instance->getQuantile()-(1e-8))) {
				index++;
				probabilitySum += this->instance->getScenarioProbabilities().at(t).at(estimatedIndexRiskPerScenario.at(index));
				yValues[t][estimatedIndexRiskPerScenario.at(index)] = true;
			}
		}
	}
	else {
		#pragma omp parallel for
		for (int i = 0; i < this->reclusteredTimeSteps.size(); i++) {

			int t = this->reclusteredTimeSteps[i];

			yValues[t].clear();

			vector<double> estimatedRiskPerScenario(this->instance->getScenarioNumber().at(t), 0.0);

			for (int j = 0; j < this->instance->getScenarioNumber().at(t); j = j + 1) {

				yValues[t][j] = false;

				double estimatedRiskCurrentScenario = 0.0;

				for (int k = 0; k < this->instance->getInterventions().size(); k++) {

					Intervention* intervention = &this->instance->getInterventions().at(k);

					for (int s = 0; s < intervention->getInProcess().at(t).size(); s++) {

						int startingTime = intervention->getInProcess().at(t).at(s);

						estimatedRiskCurrentScenario +=  this->instance->getRiskValues().at(intervention->getId()).at(t).at(startingTime).at(j) * theXValues[intervention->getId()][startingTime];

					}

				}

				estimatedRiskPerScenario[j] = estimatedRiskCurrentScenario;

			}

			vector<size_t> estimatedIndexRiskPerScenario (this->instance->getScenarioNumber().at(t));
			iota(estimatedIndexRiskPerScenario.begin(),estimatedIndexRiskPerScenario.end(),0);
			sort(estimatedIndexRiskPerScenario.begin(),estimatedIndexRiskPerScenario.end(), [&](int i,int j){return estimatedRiskPerScenario.at(i)<estimatedRiskPerScenario.at(j);} );
			int index = 0;
			double probabilitySum = this->instance->getScenarioProbabilities().at(t).at(estimatedIndexRiskPerScenario.at(index));
			yValues[t][estimatedIndexRiskPerScenario.at(index)] = true;
			while (probabilitySum < (this->instance->getQuantile()-(1e-8))) {
				index++;
				probabilitySum += this->instance->getScenarioProbabilities().at(t).at(estimatedIndexRiskPerScenario.at(index));
				yValues[t][estimatedIndexRiskPerScenario.at(index)] = true;
			}
		}


	}

}

void MilpModel::computeFullYWithX(vector<unordered_map<int, double>>& theXValues, vector<unordered_map<size_t, bool>>& yValues){

		#pragma omp parallel for
		for (int t = 0; t < this->instance->getTimeHorizon(); t = t + 1) {

			yValues[t].clear();

			vector<double> estimatedRiskPerScenario(this->instance->getScenarioNumber(true).at(t), 0.0);

			for (int j = 0; j < this->instance->getScenarioNumber(true).at(t); j = j + 1) {

				yValues[t][j] = false;

				double estimatedRiskCurrentScenario = 0.0;

				for (int k = 0; k < this->instance->getInterventions().size(); k++) {

					Intervention* intervention = &this->instance->getInterventions().at(k);

					for (int s = 0; s < intervention->getInProcess().at(t).size(); s++) {

						int startingTime = intervention->getInProcess().at(t).at(s);

						estimatedRiskCurrentScenario += this->instance->getRiskValues(true).at(intervention->getId()).at(t).at(startingTime).at(j)*theXValues[intervention->getId()][startingTime] ;

					}

				}

				estimatedRiskPerScenario[j] = estimatedRiskCurrentScenario;

			}

			vector<size_t> estimatedIndexRiskPerScenario (this->instance->getScenarioNumber(true).at(t));
			iota(estimatedIndexRiskPerScenario.begin(),estimatedIndexRiskPerScenario.end(),0);
			sort(estimatedIndexRiskPerScenario.begin(),estimatedIndexRiskPerScenario.end(), [&](int i,int j){return estimatedRiskPerScenario.at(i) < estimatedRiskPerScenario.at(j);} );
			int index = 0;
			double probabilitySum = this->instance->getScenarioProbabilities(true).at(t).at(estimatedIndexRiskPerScenario.at(index));
			yValues[t][estimatedIndexRiskPerScenario.at(index)] = true;
			while (probabilitySum < (this->instance->getQuantile()-(1e-8))) {
				index++;
				probabilitySum += this->instance->getScenarioProbabilities(true).at(t).at(estimatedIndexRiskPerScenario.at(index));
				yValues[t][estimatedIndexRiskPerScenario.at(index)] = true;
			}
		}

}

///////////////////////////CALLBACK//////////////////////////////////////////////////


void MilpModel::initializeCutCallback(bool useOldVI,bool forceRecomputation) {

	//Setting lazy constraints possible
	this->model->set(GRB_IntParam_LazyConstraints,1);

	//If Old VI is requested
	if (useOldVI){
		this->instance->prepareOldVICoefficients(forceRecomputation);
		this->cutCallback = new CutCallback (this->instance, *this->model, this->x, this->y, this->Q, this->eps);
		this->model->setCallback(this->cutCallback);
	}
	else {
		this->cutCallback2 = new CutCallback2 (this->instance, *this->model, this->x, this->y, this->Q, this->eps);
		this->model->setCallback(this->cutCallback2);
	}
}

void MilpModel::removeCallback() {

	this->model->setCallback(NULL);

}


///////////////////////////SOLVE WRITE AND EXTRACT//////////////////////////////////////////////////

void MilpModel::solveModel(double specialTimeLimit, double optimalityGap) {
	//Parameters
	this->model->set(GRB_DoubleParam_MIPGap, optimalityGap);
	this->model->set(GRB_DoubleParam_TimeLimit, specialTimeLimit);


	//Solve instruction
	try{
		this->model->optimize();
	}
	catch(GRBException e) {
		cout << "Error code = " << e.getErrorCode() << endl;
		cout << e.getMessage() << endl;
	}
	catch(...) {
		cout << "Exception during optimization" << endl;
	}

}

void MilpModel::writeModel(string name) {

	this->model->write(name);

}

void MilpModel::extractSolutionAttributes() {
	try {
		this->modelObjVal = this->model->get(GRB_DoubleAttr_ObjVal);
	}
	catch (GRBException& e) {
		this->modelObjVal = maxConstant;
	}
	try {
		this->modelObjBound = this->model->get(GRB_DoubleAttr_ObjBound);
	}
	catch (GRBException& e) {
		this->modelObjBound = 0.0;
	}
	try {
		this->modelMipGap = this->model->get(GRB_DoubleAttr_MIPGap) * 100;
	}
	catch (GRBException& e) {
		this->modelMipGap = 100.0;
	}
	try {
		this->modelComputationTime = this->model->get(GRB_DoubleAttr_Runtime);
	}
	catch (GRBException& e) {
		this->modelComputationTime = 0.0;
	}


}


///////////////////////////GET VARIABLE VALUES//////////////////////////////////////////////////

void MilpModel::getXValues(vector<unordered_map<int, double>>& xValues) {

	//cout << "X VARIABLES\n";

	for (int i = 0; i < this->instance->getInterventions().size(); i = i + 1) {

		Intervention* intervention = &this->instance->getInterventions().at(i);
		unordered_map<int, double> xValuesPerIntervention;

		for (int j = 0; j < intervention->getFeasibleStartingTimes().size(); j = j + 1) {

			int start = intervention->getFeasibleStartingTimes().at(j);

			if (this->x[intervention->getId()][start].get(GRB_DoubleAttr_X) > epsilonValue) {

				xValuesPerIntervention.insert(make_pair(start, this->x[intervention->getId()][start].get(GRB_DoubleAttr_X)));
				// cout << "INTERVENTION\t" << intervention->getName() << " " << intervention->getId() << "\t" << start << "\n";


			}

		}

		xValues[intervention->getId()] = xValuesPerIntervention;

	}

}

void MilpModel::getYValues(vector<unordered_map<int, double>>& yValues) {

	//cout << "Y VALUES\n";

	for (int i = 0; i < this->instance->getTimeHorizon(); i = i + 1) {

		unordered_map<int, double> yValuesPerTimeInstant;

		for (int j = 0; j < this->instance->getScenarioNumber().at(i); j = j + 1) {

			yValuesPerTimeInstant.insert(make_pair(j, this->y[i][j].get(GRB_DoubleAttr_X)));

		}

		yValues[i] = yValuesPerTimeInstant;

	}

}

void MilpModel::getYValues(vector<unordered_map<size_t, bool>>& yValues) {

	//cout << "Y VALUES\n";

	for (int i = 0; i < this->instance->getTimeHorizon(); i = i + 1) {

		unordered_map<size_t, bool> yValuesPerTimeInstant;

		for (int j = 0; j < this->instance->getScenarioNumber().at(i); j = j + 1) {

			yValuesPerTimeInstant.insert(make_pair(j, (this->y[i][j].get(GRB_DoubleAttr_X) >= 1.0 - epsilonValue)));

		}

		yValues[i] = yValuesPerTimeInstant;

	}

}

void MilpModel::getQValues(vector<double>& QValues) {

	//cout << "QUANTILE VALUES\n";

	for (int i = 0; i < this->instance->getTimeHorizon(); i = i + 1) {

		//cout << "TIME \t" << i << "\t" << this->Q[i].get(GRB_DoubleAttr_X) << "\t"<< this->eps[i].get(GRB_DoubleAttr_X) << "\n";
		QValues[i] = this->Q[i].get(GRB_DoubleAttr_X);

	}

}

void MilpModel::getEpsValues(vector<double>& epsValues) {

	//cout << "QUANTILE VALUES\n";

	for (int i = 0; i < this->instance->getTimeHorizon(); i = i + 1) {

		epsValues[i] = this->eps[i].get(GRB_DoubleAttr_X);

	}

}

void MilpModel::getQEpsValues(vector<double>& QValues, vector<double>& epsValues) {

	//cout << "QUANTILE VALUES\n";

	for (int i = 0; i < this->instance->getTimeHorizon(); i = i + 1) {

		//cout << "TIME \t" << i << "\t" << this->Q[i].get(GRB_DoubleAttr_X) << "\t"<< this->eps[i].get(GRB_DoubleAttr_X) << "\n";
		QValues[i] = this->Q[i].get(GRB_DoubleAttr_X);
		epsValues[i] = this->eps[i].get(GRB_DoubleAttr_X);

	}

}

void MilpModel::solutionFeasibilityCheck() {

	if (!this->isIntegralityRelaxedX) {

		//All intervention scheduled
		for (int i = 0; i < this->instance->getInterventions().size(); i = i + 1) {

			Intervention* intervention = &this->instance->getInterventions().at(i);
			int numberOfSchedules = 0;
			vector<int> values;

			for (int j = 0; j < intervention->getFeasibleStartingTimes().size(); j = j + 1) {

				int start = intervention->getFeasibleStartingTimes().at(j);

				if (this->x[intervention->getId()][start].get(GRB_DoubleAttr_X) > epsilonValue) {

					int startingTime = intervention->getFeasibleStartingTimes().at(j);
					values.push_back(1);
					numberOfSchedules = numberOfSchedules + 1;

					cout << "LINE " << __LINE__ << " Intervention " << intervention->getName() << " id " << intervention->getId() << " is scheduled at " << startingTime << " processing " << intervention->getProcessingTimes().at(startingTime) << "\n";

				}
				else {

					values.push_back(0);

				}

			}

			this->xVariableValues.push_back(values);

			if (numberOfSchedules != 1) {

				this->isSolutionFeasible = false;
				cout << "LINE " << __LINE__ << " WARNING: intervention " << intervention->getId() << " is scheduled " << numberOfSchedules << " times\n";

			}

		}

		//Resource consumptions check
		for (int i = 0; i < this->instance->getResources().size(); i = i + 1) {

			Resource* resource = &this->instance->getResources().at(i);

			/*cout << "LINE " << __LINE__ << " WARNING: resource " << resource->getId() << "\n";*/

			for (int j = 0; j < this->instance->getTimeHorizon(); j = j + 1) {
				double consumption = 0;

				for (int k = 0; k < this->instance->getInterventions().size(); k = k + 1) {

					Intervention* intervention = &this->instance->getInterventions().at(k);

					for (int l = 0; l < intervention->getInProcess().at(j).size(); l = l + 1) {

						int startingTime = intervention->getInProcess().at(j).at(l);

						if (this->x[intervention->getId()][startingTime].get(GRB_DoubleAttr_X) > epsilonValue) {

							double resourceConsumption = this->instance->getResourceConsumptions().at(intervention->getId()).at(resource->getId()).at(j).at(startingTime);

							if (resourceConsumption > -1 + epsilonValue) {

								consumption = consumption + resourceConsumption;

							}

						}

					}

				}

				if (consumption > resource->getUpperBounds().at(j) + epsilonValue || consumption < resource->getLowerBounds().at(j) - epsilonValue) {

					this->isSolutionFeasible = false;
					cout << "\nLINE " << __LINE__ << " WARNING: resource " << resource->getName() << " consumption " << consumption << " outside bounds " << resource->getLowerBounds().at(j) << " " << resource->getUpperBounds().at(j) << " at time " << j << endl;

				}

			}

		}

		if (!this->isExclusionConstraintsRelaxed) {

			//Exclusions check
			for (int i = 0; i < this->instance->getExclusions().size(); i = i + 1) {

				Exclusion* exclusion = &this->instance->getExclusions().at(i);

				for (int j = 0; j < exclusion->getSeason()->getTimeInstants().size(); j = j + 1) {

					int timeInstant = exclusion->getSeason()->getTimeInstants().at(j);

					bool check1 = false;
					for (int k = 0; k < exclusion->getIntervention1()->getInProcess().at(timeInstant).size(); k = k + 1) {

						int startingTime = exclusion->getIntervention1()->getInProcess().at(timeInstant).at(k);
						pair<Intervention*, int> key = make_pair(exclusion->getIntervention1(), startingTime);

						if (this->x[exclusion->getIntervention1()->getId()][startingTime].get(GRB_DoubleAttr_X) > epsilonValue) {

							check1 = true;

						}

					}

					bool check2 = false;
					for (int k = 0; k < exclusion->getIntervention2()->getInProcess().at(timeInstant).size(); k = k + 1) {

						int startingTime = exclusion->getIntervention2()->getInProcess().at(timeInstant).at(k);
						pair<Intervention*, int> key = make_pair(exclusion->getIntervention2(), startingTime);

						if (this->x[exclusion->getIntervention2()->getId()][startingTime].get(GRB_DoubleAttr_X) > epsilonValue) {

							check2 = true;

						}

					}

					if (check1 && check2) {

						this->isSolutionFeasible = false;
						cout << "LINE " << __LINE__ << " WARNING: exclusion " << exclusion->getName() << " violated at time instant " << timeInstant << "\n";

					}

				}

			}

		}

	}

	if (!this->isQuantileRelaxed && !this->isIntegralityRelaxedY) {

		//y variable
		for (int i = 0; i < this->instance->getTimeHorizon(); i = i + 1) {

			int yValue = 0;

			for (int j = 0; j < this->instance->getScenarioNumber().at(i); j = j + 1) {

				yValue = yValue + this->y[i][j].get(GRB_DoubleAttr_X);

			}

			double value = ceil(this->instance->getQuantile() * this->instance->getScenarioNumber().at(i));

			if (yValue != value) {

				this->isSolutionFeasible = false;
				cout << "LINE " << __LINE__ << " WARNING: y value at " << i << " is " << yValue << " instead of " << value << "\n";

			}

		}

		//Q variables
		for (int i = 0; i < this->instance->getTimeHorizon(); i = i + 1) {

			for (int j = 0; j < this->instance->getScenarioNumber().at(i); j = j + 1) {

				double risk = 0;

				//if y[t][s]=1
				if (this->y[i][j].get(GRB_DoubleAttr_X) > epsilonValue) {

					for (int k = 0; k < this->instance->getInterventions().size(); k = k + 1) {

						Intervention* intervention = &this->instance->getInterventions().at(k);

						for (int l = 0; l < intervention->getInProcess().at(i).size(); l = l + 1) {

							int startingTime = intervention->getInProcess().at(i).at(l);

							double riskValue = this->instance->getRiskValues().at(intervention->getId()).at(i).at(startingTime).at(j);

							if (riskValue > epsilonValue && this->x[intervention->getId()][startingTime].get(GRB_DoubleAttr_X) > epsilonValue) {

								risk += riskValue;

							}

						}

					}

					if (this->Q[i].get(GRB_DoubleAttr_X) < risk - epsilonValue) {

						cout << "LINE " << __LINE__ << " WARNING: Q value at " << i << " is " << this->Q[i].get(GRB_DoubleAttr_X) << " less than " << risk << "\n";

					}

				}//if y[t][s]=1
				else {

					if (this->Q[i].get(GRB_DoubleAttr_X) < - this->instance->getBigM().at(i).at(j) - epsilonValue) {

						cout << "LINE " << __LINE__ << " WARNING: Q value at " << i << " is " << this->Q[i].get(GRB_DoubleAttr_X) << " less than bigM " << - this->instance->getBigM().at(i).at(j) << "\n";

					}

				}

			}

		}

		//eps variables
		for (int i = 0; i < this->instance->getTimeHorizon(); i = i + 1) {

			if (this->Q[i].get(GRB_DoubleAttr_X) < epsilonValue) {

				if (this->eps[i].get(GRB_DoubleAttr_X) > epsilonValue) {

					cout << "LINE " << __LINE__ << " WARNING: eps value at " << i << " is " << this->eps[i].get(GRB_DoubleAttr_X) << " but Q value is " << this->Q[i].get(GRB_DoubleAttr_X) << "\n";

				}

			}
			else {

				double risk = 0;

				for (int j = 0; j < this->instance->getScenarioNumber().at(i); j = j + 1) {

					for (int k = 0; k < this->instance->getInterventions().size(); k = k + 1) {

						Intervention* intervention = &this->instance->getInterventions().at(k);

						for (int l = 0; l < intervention->getInProcess().at(i).size(); l = l + 1) {

							int startingTime = intervention->getInProcess().at(i).at(l);

							double riskValue = this->instance->getRiskValues().at(intervention->getId()).at(i).at(startingTime).at(j);

							if (riskValue > epsilonValue && this->x[intervention->getId()][startingTime].get(GRB_DoubleAttr_X) > epsilonValue) {

								risk += riskValue;

							}

						}

					}

				}

				risk *= (1 / (double)this->instance->getScenarioNumber().at(i));

				if (this->eps[i].get(GRB_DoubleAttr_X) < this->Q[i].get(GRB_DoubleAttr_X) - risk - epsilonValue) {

					cout << "LINE " << __LINE__ << " WARNING: eps value at " << i << " is " << this->eps[i].get(GRB_DoubleAttr_X) << " but it should be at least " << this->Q[i].get(GRB_DoubleAttr_X) - risk << "\n";

				}

			}

		}

	}

	//Objective value check
	if (!this->isIntegralityRelaxedX) {

		double firstTerm = 0;
		for (int i = 0; i < this->instance->getTimeHorizon(); i = i + 1) {

			double subFirstTerm = 0;

			for (int j = 0; j < this->instance->getScenarioNumber().at(i); j = j + 1) {

				for (int k = 0; k < this->instance->getInterventions().size(); k = k + 1) {

					Intervention* intervention = &this->instance->getInterventions().at(k);

					for (int l = 0; l < intervention->getInProcess().at(i).size(); l = l + 1) {

						int startingTime = intervention->getInProcess().at(i).at(l);

						double riskValue = this->instance->getRiskValues().at(intervention->getId()).at(i).at(startingTime).at(j);

						if (riskValue > epsilonValue && this->x[intervention->getId()][startingTime].get(GRB_DoubleAttr_X) > epsilonValue) {

							subFirstTerm += riskValue;

						}

					}

				}

			}

			firstTerm += (1 / (double)this->instance->getScenarioNumber().at(i)) * subFirstTerm;

		}

		this->firstTermObjective = firstTerm / this->instance->getTimeHorizon();

		if (!this->isIntegralityRelaxedY && !this->isQuantileRelaxed) {

			double secondTerm = 0;
			vector<double> realVal(this->instance->getTimeHorizon(), 0);
			for (int i = 0; i < this->instance->getTimeHorizon(); i = i + 1) {

				vector<double> subSecondTerm1(this->instance->getScenarioNumber().at(i), 0);
				double subSecondTerm2 = 0;

				for (int j = 0; j < this->instance->getScenarioNumber().at(i); j = j + 1) {

					double subsubTerm1 = 0;

					if (this->y[i][j].get(GRB_DoubleAttr_X) > epsilonValue) {

						for (int k = 0; k < this->instance->getInterventions().size(); k = k + 1) {

							Intervention* intervention = &this->instance->getInterventions().at(k);

							for (int l = 0; l < intervention->getInProcess().at(i).size(); l = l + 1) {

								int startingTime = intervention->getInProcess().at(i).at(l);

								double riskValue = this->instance->getRiskValues().at(intervention->getId()).at(i).at(startingTime).at(j);

								if (riskValue > epsilonValue && this->x[intervention->getId()][startingTime].get(GRB_DoubleAttr_X) > epsilonValue) {

									subsubTerm1 += riskValue;

								}

							}

						}

					}

					subSecondTerm1.push_back(subsubTerm1);

				}

				for (int j = 0; j < this->instance->getScenarioNumber().at(i); j = j + 1) {

					for (int k = 0; k < this->instance->getInterventions().size(); k = k + 1) {

						Intervention* intervention = &this->instance->getInterventions().at(k);

						for (int l = 0; l < intervention->getInProcess().at(i).size(); l = l + 1) {

							int startingTime = intervention->getInProcess().at(i).at(l);

							double riskValue = this->instance->getRiskValues().at(intervention->getId()).at(i).at(startingTime).at(j);

							if (riskValue > epsilonValue && this->x[intervention->getId()][startingTime].get(GRB_DoubleAttr_X) > epsilonValue) {

								subSecondTerm2 += riskValue;

							}

						}

					}

				}

				subSecondTerm2 *= (1 / (double)this->instance->getScenarioNumber().at(i));

				realVal[i] = (*max_element(subSecondTerm1.begin(), subSecondTerm1.end()) - subSecondTerm2);

			}

			for (int i = 0; i < this->instance->getTimeHorizon(); i = i + 1) {

				cout << "LINE " << __LINE__ << " eps " << this->eps[i].get(GRB_DoubleAttr_X) << " real val " << realVal[i] << "\n";

				secondTerm += realVal[i];

				/*if (this->eps[timeInstant].get(GRB_DoubleAttr_X) > epsilonValue) {

					secondTerm += this->eps[timeInstant].get(GRB_DoubleAttr_X);

				}*/

			}

			this->secondTermObjective = secondTerm / this->instance->getTimeHorizon();

		}

	}

}

void MilpModel::solutionPrinting() {

	string streamName = "Output_" + this->instance->getName() + ".txt";
	ofstream mystream;

	mystream.open(streamName, ofstream::out | ofstream::trunc);

	if (this->isIntegralityRelaxedX && this->isIntegralityRelaxedY) {

		mystream << "\nx variables\n";

	}

	//All intervention scheduled
	for (int i = 0; i < this->instance->getInterventions().size(); i = i + 1) {

		Intervention* intervention = &this->instance->getInterventions().at(i);

		for (int j = 0; j < intervention->getFeasibleStartingTimes().size(); j = j + 1) {

			int startingTime = intervention->getFeasibleStartingTimes().at(j);

			if (this->x[intervention->getId()][startingTime].get(GRB_DoubleAttr_X) > epsilonValue) {

				if (this->isIntegralityRelaxedX) {

					mystream << intervention->getName() << " " << startingTime << " variable value " << this->x[intervention->getId()][startingTime].get(GRB_DoubleAttr_X) << "\n";

				}
				else {

					mystream << intervention->getName() << " " << startingTime + 1 << "\n";

				}


			}

		}

	}

	mystream << "\nQ variables\n";

	for (int i = 0; i < this->instance->getTimeHorizon(); i = i + 1) {

		mystream << "Time instant " << i << " variable value " << this->Q[i].get(GRB_DoubleAttr_X) << "\n";

	}


	mystream << "\nepsilonValue variables\n";

	for (int i = 0; i < this->instance->getTimeHorizon(); i = i + 1) {

		mystream << "Time instant " << i << " variable value " << this->eps[i].get(GRB_DoubleAttr_X) << "\n";

	}

	if (this->isIntegralityRelaxedY && !this->isQuantileRelaxed) {

		mystream << "\ny variables\n";

		for (int i = 0; i < this->instance->getTimeHorizon(); i = i + 1) {

			for (int j = 0; j < this->instance->getScenarioNumber().at(i); j = j + 1) {

				if (this->y[i][j].get(GRB_DoubleAttr_X) > epsilonValue) {

					mystream << "Time instant " << i << " scenario " << j << " variable value " << this->y[i][j].get(GRB_DoubleAttr_X) << "\n";

				}

			}

		}

		mystream << "\nQ variables\n";

		for (int i = 0; i < this->instance->getTimeHorizon(); i = i + 1) {

			if (this->Q[i].get(GRB_DoubleAttr_X) > epsilonValue) {

				mystream << "Time instant " << i << " variable value " << this->Q[i].get(GRB_DoubleAttr_X) << "\n";

			}

		}

		mystream << "\nepsilonValue variables\n";

		for (int i = 0; i < this->instance->getTimeHorizon(); i = i + 1) {

			if (this->eps[i].get(GRB_DoubleAttr_X) > epsilonValue) {

				mystream << "Time instant " << i << " variable value " << this->eps[i].get(GRB_DoubleAttr_X) << "\n";

			}

		}

	}

	mystream.close();

}

void MilpModel::solutionPrinting(vector<unordered_map<int, double>>& xValues, vector<unordered_map<size_t, bool>>& yValues, string outputFile) {

	string streamName = outputFile;
	ofstream mystream;

	mystream.open(streamName, ofstream::out | ofstream::trunc);

	mystream << "X VARIABLES\n\n";

	//Print interventions
	mystream << "intervention , start-time\n";
	mystream << "=========================\n";
	for (int i = 0; i < this->instance->getInterventions().size(); i = i + 1) {

		Intervention* intervention = &this->instance->getInterventions().at(i);

		for (int j = 0; j < intervention->getFeasibleStartingTimes().size(); j = j + 1) {

			int startingTime = intervention->getFeasibleStartingTimes().at(j);

			if (xValues[intervention->getId()][startingTime] > epsilonValue) {

				mystream << intervention->getName() << "," << startingTime + 1 << "\n";

			}

		}

	}

	mystream << "\nY VARIABLES\n\n";

	//Print selected scenarios
	mystream << "timestep , scenario , value\n";
	mystream << "===========================\n";
	for (int i = 0; i < this->instance->getTimeHorizon(); i = i + 1) {

		for (int j = 0; j < this->instance->getScenarioNumber(true).at(i); j = j + 1) {

			mystream << i + 1 << "," << j + 1 << "," << yValues[i][j] << "\n";

		}

	}

	mystream.close();

}


///////////////////////////OBJECTIVE VALUE RECOVERY//////////////////////////////////////////////

double MilpModel::objectiveValueRecovery(vector<unordered_map<int, double>>& xValues) {

	double firstTerm = 0;
	chrono::time_point<chrono::system_clock> start,end;
	start = chrono::system_clock::now();

	double constant1 = (1 / (double)this->instance->getTimeHorizon())*this->instance->getAlpha();

	for (int i = 0; i < this->instance->getInterventions().size(); i = i + 1) {

		Intervention* intervention = &this->instance->getInterventions().at(i);

		for (auto it1 = this->instance->getRiskValues(true).at(i).begin(); it1 != this->instance->getRiskValues(true).at(i).end(); it1++) {

			int timeInstant = it1->first;

			for (auto it2 = it1->second.begin(); it2 != it1->second.end(); it2++) {

				int startTime = it2->first;

				for (auto it3 = it2->second.begin(); it3 != it2->second.end(); it3++) {

					try {

						firstTerm += this->instance->getScenarioProbabilities(true).at(timeInstant).at(it3->first) * it3->second * xValues[intervention->getId()][startTime];
					}
					catch (out_of_range ex) {

					}

				}

			}

		}

	}
	firstTerm *= constant1;

	std::cout << endl << "FIRST TERM: " << "\n";
	std::cout << firstTerm << "\n";


	//total excess
	double totalExcess = 0;
	double constant = (1.0 - this->instance->getAlpha()) * (1 / (double)this->instance->getTimeHorizon());
	vector<double> realQuantiles (this->instance->getTimeHorizon(), 0.0);
	this->realRisksPerTimestepPerScenario = vector<vector<double>> (this->instance->getTimeHorizon(), vector<double> ());
	this->scenarioRankingPerTimestep = vector<vector<size_t>> (this->instance->getTimeHorizon(), vector<size_t> ());


	//Compute the quantile per each time instant
	#pragma omp parallel for
	for (int t = 0; t < this->instance->getTimeHorizon(); t++) {

		//Risks for each real scenario
		this->realRisksPerTimestepPerScenario.at(t).resize(this->instance->getScenarioNumber(true).at(t), 0.0);
		this->scenarioRankingPerTimestep.at(t).resize(this->instance->getScenarioNumber(true).at(t), 0);
		double realQuantileCurrentTimeInstant = 0.0;
		double averageRiskCurrentTimeInstant = 0.0;
		double coeff = ((double)1.0 / (double)this->instance->getScenarioNumber(true).at(t));

		for (int j = 0; j < this->instance->getScenarioNumber(true).at(t); j++) {

			double realRiskCurrentScenario = 0.0;

			for (int i = 0; i < this->instance->getInterventions().size(); i++) {

				Intervention* intervention = &this->instance->getInterventions().at(i);

				for (int s = 0; s < intervention->getInProcess().at(t).size(); s++) {

					int startingTime = intervention->getInProcess().at(t).at(s);

					realRiskCurrentScenario +=  this->instance->getRiskValues(true).at(intervention->getId()).at(t).at(startingTime).at(j) * xValues[intervention->getId()][startingTime];
					averageRiskCurrentTimeInstant += coeff * this->instance->getRiskValues(true).at(intervention->getId()).at(t).at(startingTime).at(j) * xValues[intervention->getId()][startingTime];

				}

			}

			this->realRisksPerTimestepPerScenario.at(t).at(j) = realRiskCurrentScenario;
		}

		//Sorting based on the quantile and taking the right risk sum for the quantile
		vector<size_t> realIndexRisksPerScenario (this->instance->getScenarioNumber(true).at(t));
		iota(realIndexRisksPerScenario.begin(),realIndexRisksPerScenario.end(),0);
		stable_sort(realIndexRisksPerScenario.begin(),realIndexRisksPerScenario.end(), [&](int i,int j){return this->realRisksPerTimestepPerScenario.at(t).at(i) < this->realRisksPerTimestepPerScenario.at(t).at(j);} );
		int index1 = ceil(this->instance->getQuantile()*this->instance->getScenarioNumber(true).at(t)) - 1;
		realQuantileCurrentTimeInstant = this->realRisksPerTimestepPerScenario.at(t).at(realIndexRisksPerScenario.at(index1));
		realQuantiles.at(t) = realQuantileCurrentTimeInstant;
		totalExcess += max(0.0, realQuantileCurrentTimeInstant - averageRiskCurrentTimeInstant);

		this->scenarioRankingPerTimestep.at(t) = realIndexRisksPerScenario;

	}

	this->realQuantilesPerTimeInstant = realQuantiles;


	totalExcess *= constant;
	std::cout << "SECOND TERM" << "\n";
	std::cout << totalExcess << "\n";
	cout << "\nOBJECTIVE VALUE\t" << firstTerm+totalExcess << "\n";
	cout << "\n";
	this->recoveredObjectiveValue = firstTerm+totalExcess;
	end = chrono::system_clock::now();
	this->maxObjectiveValueRecoveryTime = max(this->maxObjectiveValueRecoveryTime, (chrono::duration_cast<chrono::nanoseconds> (end - start).count()) * 1e-9);
	std::cout << "Computation Time: "<< (chrono::duration_cast<chrono::nanoseconds> (end - start).count()) * 1e-9 << "\n";
	return totalExcess+firstTerm;
}



///////////////////////////ALTERNATING DIRECTION METHOD//////////////////////////////////////////////


void MilpModel::initializeSecondMILP_ADM(bool viInFirstMILP) {

	//Creating an all timesteps vector for adapting constraints
	vector<size_t> timeSteps (this->instance->getTimeHorizon());
	iota (begin(timeSteps), end(timeSteps), 0);

	this->isQuantileRelaxed = false;
	this->addObjective();

	//Initialize xVariableFixing and yVariableFixing
	this->initializeYVariableFixing_ADM();

	this->model->set(GRB_IntParam_MIPFocus, 0);
	this->model->set(GRB_IntParam_Presolve, 0);

}

void MilpModel::ySolving_ADM_Adaptive(vector<unordered_map<int, double>>& xValues, vector<unordered_map<size_t, bool>>& yValues, double& objectiveValue){

	chrono::time_point<chrono::system_clock> start,end;

	start = chrono::system_clock::now();

	//tests
	vector<vector<double>> estimatedRiskPerTimestepPerScenario (this->instance->getTimeHorizon(),vector<double>());
	vector<double> averageRiskPerTimestep (this->instance->getTimeHorizon(),0.0);
	for (int t = 0; t < this->instance->getTimeHorizon(); t = t + 1) {
		estimatedRiskPerTimestepPerScenario[t].resize(this->instance->getScenarioNumber().at(t),0.0);
		for (int j = 0; j < this->instance->getScenarioNumber().at(t); j = j + 1) {
			yValues[t][j] = false;
		}
	}

	double firstTerm = 0.0;
	double constant1 = (1 / (double)this->instance->getTimeHorizon())*this->instance->getAlpha();

	for (int i = 0; i < this->instance->getInterventions().size(); i = i + 1) {

		Intervention* intervention = &this->instance->getInterventions().at(i);

		for (auto it1 = this->instance->getRiskValues(true).at(i).begin(); it1 != this->instance->getRiskValues(true).at(i).end(); it1++) {

			int timeInstant = it1->first;

			for (auto it2 = it1->second.begin(); it2 != it1->second.end(); it2++) {

				int startTime = it2->first;

				for (auto it3 = it2->second.begin(); it3 != it2->second.end(); it3++) {

					int scenario = it3->first;
					double localRisk = it3->second * xValues[intervention->getId()][startTime];
					double probability = this->instance->getScenarioProbabilities(true).at(timeInstant).at(it3->first);

					firstTerm += localRisk*probability;
					averageRiskPerTimestep[timeInstant] += localRisk*probability;

				}

			}

		}

	}

	for (int i = 0; i < this->instance->getInterventions().size(); i = i + 1) {

		Intervention* intervention = &this->instance->getInterventions().at(i);

		for (auto it1 = this->instance->getRiskValues().at(i).begin(); it1 != this->instance->getRiskValues().at(i).end(); it1++) {

			int timeInstant = it1->first;

			for (auto it2 = it1->second.begin(); it2 != it1->second.end(); it2++) {

				int startTime = it2->first;

				for (auto it3 = it2->second.begin(); it3 != it2->second.end(); it3++) {

					int scenario = it3->first;
					double localRisk = it3->second * xValues[intervention->getId()][startTime];
					double probability = this->instance->getScenarioProbabilities().at(timeInstant).at(it3->first);

					estimatedRiskPerTimestepPerScenario[timeInstant][scenario] += localRisk;

				}

			}

		}

	}

	firstTerm *= constant1;

	//Computing second term and saving yValues
	double totalExcess = 0.0;
	double constant2 = (1.0 - this->instance->getAlpha()) * (1.0 / (double)this->instance->getTimeHorizon());


	//#pragma omp parallel for
	for (int t = 0; t < this->instance->getTimeHorizon(); t = t + 1) {

		// cout<<"Time "<<t<<endl;

		double maxCutValue = 0.0;

		for (int cut_index = 0; cut_index < this->counterCutsperTimestep[t]; cut_index++) {

			double cutValue = 0.0;

			for (int i = 0; i < this->instance->getInterventions().size(); i++) {

				Intervention* intervention = &this->instance->getInterventions().at(i);

				for (int s = 0; s < intervention->getInProcess().at(t).size(); s++) {

					int tPrime = intervention->getInProcess().at(t).at(s);
					// cout<<this->allNewVICoefficients[t][cut_index][intervention->getId()][tPrime]*xValues[intervention->getId()][tPrime]<<endl;
					if (xValues[intervention->getId()][tPrime]){
						cutValue += this->allNewVICoefficients[t][cut_index][intervention->getId()][tPrime];
					}
				}
			}

			// cout<<"Time "<<t<<" Cut "<<cut_index<<" Value "<<cutValue<<endl;
			maxCutValue = max(maxCutValue,cutValue);
		}
		// cout<<"Max cut value: "<<maxCutValue<<endl;

		vector<size_t> estimatedIndexRiskPerScenario (this->instance->getScenarioNumber().at(t));
		iota(estimatedIndexRiskPerScenario.begin(),estimatedIndexRiskPerScenario.end(),0);
		sort(estimatedIndexRiskPerScenario.begin(),estimatedIndexRiskPerScenario.end(), [&](int i,int j){return estimatedRiskPerTimestepPerScenario.at(t).at(i) < estimatedRiskPerTimestepPerScenario.at(t).at(j);} );
		int index = 0;
		double probabilitySum = this->instance->getScenarioProbabilities().at(t).at(estimatedIndexRiskPerScenario.at(index));
		yValues[t][estimatedIndexRiskPerScenario.at(index)] = true;
		while (probabilitySum < (this->instance->getQuantile()-(1e-8))) {
			index++;
			probabilitySum += this->instance->getScenarioProbabilities().at(t).at(estimatedIndexRiskPerScenario.at(index));
			yValues[t][estimatedIndexRiskPerScenario.at(index)] = true;
		}
		// cout<<"quantile: "<<estimatedRiskPerTimestepPerScenario[t][estimatedIndexRiskPerScenario[index]]<<endl;
		double estimatedQuantileCurrentTimeInstant = max(estimatedRiskPerTimestepPerScenario[t][estimatedIndexRiskPerScenario[index]],maxCutValue);
		// cout<<"estimated quantile: "<<estimatedQuantileCurrentTimeInstant<<endl;
		// cout<<"epsilon: "<< max(0.0, estimatedQuantileCurrentTimeInstant - averageRiskPerTimestep.at(t))<<endl;
		totalExcess += max(0.0, estimatedQuantileCurrentTimeInstant - averageRiskPerTimestep.at(t));
	}

	end = chrono::system_clock::now();
	cout << "Computation time: " << (chrono::duration_cast<chrono::nanoseconds> (end - start).count()) * 1e-9 << endl;

	totalExcess *= constant2;

	objectiveValue = firstTerm+totalExcess;
	cout<<"Objective "<<objectiveValue<<endl;

}

void MilpModel::ySolving_ADM(vector<unordered_map<int, double>>& xValues, vector<unordered_map<size_t, bool>>& yValues, double& objectiveValue){

	chrono::time_point<chrono::system_clock> start,end;

	start = chrono::system_clock::now();

	//tests
	vector<vector<double>> estimatedRiskPerTimestepPerScenario (this->instance->getTimeHorizon(),vector<double>());
	vector<double> averageRiskPerTimestep (this->instance->getTimeHorizon(),0.0);
	for (int t = 0; t < this->instance->getTimeHorizon(); t = t + 1) {
		estimatedRiskPerTimestepPerScenario[t].resize(this->instance->getScenarioNumber().at(t),0.0);
		for (int j = 0; j < this->instance->getScenarioNumber().at(t); j = j + 1) {
			yValues[t][j] = false;
		}
	}

	double firstTerm = 0.0;
	double constant1 = (1 / (double)this->instance->getTimeHorizon())*this->instance->getAlpha();

	for (int i = 0; i < this->instance->getInterventions().size(); i = i + 1) {

		Intervention* intervention = &this->instance->getInterventions().at(i);

		for (auto it1 = this->instance->getRiskValues().at(i).begin(); it1 != this->instance->getRiskValues().at(i).end(); it1++) {

			int timeInstant = it1->first;

			for (auto it2 = it1->second.begin(); it2 != it1->second.end(); it2++) {

				int startTime = it2->first;

				for (auto it3 = it2->second.begin(); it3 != it2->second.end(); it3++) {

					int scenario = it3->first;
					double localRisk = it3->second * xValues[intervention->getId()][startTime];
					double probability = this->instance->getScenarioProbabilities().at(timeInstant).at(it3->first);

					firstTerm += localRisk*probability;
					estimatedRiskPerTimestepPerScenario[timeInstant][scenario] += localRisk;
					averageRiskPerTimestep[timeInstant] += localRisk*probability;

				}

			}

		}

	}

	firstTerm *= constant1;

	//Computing second term and saving yValues
	double totalExcess = 0.0;
	double constant2 = (1.0 - this->instance->getAlpha()) * (1 / (double)this->instance->getTimeHorizon());


	//#pragma omp parallel for
	for (int t = 0; t < this->instance->getTimeHorizon(); t = t + 1) {

		vector<size_t> estimatedIndexRiskPerScenario (this->instance->getScenarioNumber().at(t));
		iota(estimatedIndexRiskPerScenario.begin(),estimatedIndexRiskPerScenario.end(),0);
		sort(estimatedIndexRiskPerScenario.begin(),estimatedIndexRiskPerScenario.end(), [&](int i,int j){return estimatedRiskPerTimestepPerScenario.at(t).at(i) < estimatedRiskPerTimestepPerScenario.at(t).at(j);} );
		int index = 0;
		double probabilitySum = this->instance->getScenarioProbabilities().at(t).at(estimatedIndexRiskPerScenario.at(index));
		yValues[t][estimatedIndexRiskPerScenario.at(index)] = true;
		while (probabilitySum < (this->instance->getQuantile()-(1e-8))) {
			index++;
			probabilitySum += this->instance->getScenarioProbabilities().at(t).at(estimatedIndexRiskPerScenario.at(index));
			yValues[t][estimatedIndexRiskPerScenario.at(index)] = true;
		}
		double estimatedQuantileCurrentTimeInstant = estimatedRiskPerTimestepPerScenario[t][estimatedIndexRiskPerScenario[index]];
		totalExcess += max(0.0, estimatedQuantileCurrentTimeInstant - averageRiskPerTimestep.at(t));
	}

	end = chrono::system_clock::now();
	cout << "Computation time: " << (chrono::duration_cast<chrono::nanoseconds> (end - start).count()) * 1e-9 << endl;

	totalExcess *= constant2;

	objectiveValue = firstTerm+totalExcess;
	cout<<"Objective "<<objectiveValue<<endl;

}

void MilpModel::initializeXVariableFixing_ADM() {

	//Initilize storage for the variable fixing constraint
	this->xVariableFixing = vector<unordered_map<int, GRBConstr>> (this->instance->getInterventions().size(), unordered_map<int, GRBConstr>());


}

void MilpModel::initializeYVariableFixing_ADM() {

	//Initilize storage for the variable fixing constraint
	this->yVariableFixing = vector<unordered_map<int, GRBConstr>> (this->instance->getTimeHorizon(), unordered_map<int, GRBConstr>());


}

///////////////////////////ADAPTIVE OPTIMIZATION//////////////////////////////////////////////

void MilpModel::computeClusterRisk_adaptive(vector<unordered_map<int, double>>& xValues) {

	this->risksPerTimestepPerCluster = vector<vector<double>> (this->instance->getTimeHorizon(), vector<double> ());

	// cout<<endl;
	//Compute the quantile per each time instant
	//#pragma omp parallel for
	for (int t = 0; t < this->instance->getTimeHorizon(); t++) {

		//If the model is reduced we also compute it for the reduced model
		vector<double> estimatedRisksPerScenario(this->instance->getScenarioNumber().at(t), 0.0);
		// cout<< "Time "<< t<<endl;

		for (int c = 0; c < this->instance->getScenarioNumber().at(t); c++) {

			double estimatedRiskCurrentScenario = 0.0;
			double cutRHS = 0.0;

			double clusterprobability = this->instance->getScenarioProbabilities()[t][c];
			double maxProbabilityCut = this->instance->getQuantile() - 1.0 + clusterprobability;

			for (int i = 0; i < this->instance->getInterventions().size(); i++) {

				Intervention* intervention = &this->instance->getInterventions().at(i);

				for (int s = 0; s < intervention->getInProcess().at(t).size(); s++) {

					int startingTime = intervention->getInProcess().at(t).at(s);

					estimatedRiskCurrentScenario +=  this->instance->getRiskValues().at(intervention->getId()).at(t).at(startingTime).at(c) * xValues[intervention->getId()][startingTime];
					// cout<< this->instance->getNewVICoefficients()[c][t][startingTime][intervention->getId()] << ", ";

					cutRHS += this->instance->getNewVICoefficients()[c][t][startingTime][intervention->getId()]*xValues[intervention->getId()][startingTime]/maxProbabilityCut;

				}

			}

			// cout <<endl<< "Cluster " << c << ": risk " << estimatedRiskCurrentScenario << ": cut " << cutRHS <<endl;

			estimatedRisksPerScenario.at(c) = max(estimatedRiskCurrentScenario,cutRHS);

		}

		//Saving the risks per cluster
		this->risksPerTimestepPerCluster[t] = estimatedRisksPerScenario;

	}

}


void MilpModel::computeErrorEstimators_adaptive() {

	//Preallowating the data and extracting quantiles
	this->estimatedQuantilesPerTimeInstant = vector<double> (this->instance->getTimeHorizon(), 0.0);
	this->getQValues(this->estimatedQuantilesPerTimeInstant);
	this->errorEstimators = vector<double> (this->instance->getTimeHorizon(), 0.0);
	this->totalError = 0.0;

	//Iterate over all timeperiods and extract the error
	// cout<<endl<<"ERROR ESTIMATORS"<<endl;
	#pragma omp parallel for
	for (int t = 0; t < this->instance->getTimeHorizon(); t++) {

		this->errorEstimators.at(t) = this->estimatedQuantilesPerTimeInstant.at(t) - this->realQuantilesPerTimeInstant.at(t);

		// cout<<"Time "<< t <<": "<< this->estimatedQuantilesPerTimeInstant.at(t) <<": "<<  this->realQuantilesPerTimeInstant.at(t) << ": "<<  this->errorEstimators.at(t)<<endl;

		this->errorEstimators.at(t) = abs(this->errorEstimators.at(t));

		this->totalError += this->errorEstimators.at(t);
	}
}


void MilpModel::computeImportantTimesteps_adaptive(double criteria) {

	cout<<"The reclustered timesteps are: ";
	vector<size_t> indexErrorEstimators (this->instance->getTimeHorizon());
	vector<size_t> reclusteredTimeSteps;
	iota(indexErrorEstimators.begin(),indexErrorEstimators.end(),0);
	sort(indexErrorEstimators.begin(),indexErrorEstimators.end(), [&](int i,int j){return this->errorEstimators.at(i) > this->errorEstimators.at(j);} );

	int index = 0;
	double errorSum = this->errorEstimators.at(indexErrorEstimators.at(index));
	reclusteredTimeSteps.push_back(indexErrorEstimators.at(index));
	cout<<indexErrorEstimators.at(index);

	while (errorSum < this->totalError*criteria) {

		index++;
		errorSum += this->errorEstimators.at(indexErrorEstimators.at(index));
		reclusteredTimeSteps.push_back(indexErrorEstimators.at(index));

		cout<<", "<<indexErrorEstimators.at(index);
	}
	cout<<endl;

	this->reclusteredTimeSteps = reclusteredTimeSteps;
}

void MilpModel::updateVariables_y_adaptive() {

	for (int i = 0; i < this->instance->getTimeHorizon(); i++) {

		for (int j = 0; j < this->instance->getScenarioNumber().at(i); j++) {

			if (j < this->y[i].size()){
				this->y[i][j].set(GRB_DoubleAttr_Start, GRB_UNDEFINED);
			}
			else{
				pair<int, int> key = make_pair(i, j);
				string name = "y[" + to_string(i) + "," + to_string(j) + "]";
				GRBVar variable = this->model->addVar(0, 1, 0, GRB_BINARY, name);
				this->y[i].insert(make_pair(j, variable));
			}

		}

	}
	this->model->update();

}

void MilpModel::addConstraintVI_adaptive(vector<size_t>& reclusteredTimeSteps, unordered_map<size_t,vector<size_t>>& newClusters) {

	for (int t_index = 0; t_index < reclusteredTimeSteps.size(); t_index++) {

		int t = reclusteredTimeSteps[t_index];

		for (int c_index = 0; c_index < newClusters[t].size(); c_index++) {

			int c = newClusters[t][c_index];

			double clusterProbability = this->instance->getScenarioProbabilities().at(t).at(c);

			double maxProbabilityCut = this->instance->getQuantile() - 1.0 + clusterProbability;

			if (maxProbabilityCut > 0.0) {

				// cout<<"Added "<< this->internalCounter << " cuts"<<endl;

				// cout << "Time "<< t << ": Cluster " << c << ": probability cluster " << clusterProbability << ": size cut " << maxProbabilityCut <<endl;

				GRBLinExpr constraint = - maxProbabilityCut*this->Q[t];

				for (int i = 0; i < this->instance->getInterventions().size(); i++) {

					Intervention* intervention = &this->instance->getInterventions().at(i);

					for (int s = 0; s < intervention->getInProcess().at(t).size(); s++) {

						int tPrime = intervention->getInProcess().at(t).at(s);

						constraint += this->instance->getNewVICoefficients()[c][t][tPrime][intervention->getId()]*this->x[intervention->getId()][tPrime];

						this->allNewVICoefficients[t][this->counterCutsperTimestep[t]][intervention->getId()][tPrime] = this->instance->getNewVICoefficients()[c][t][tPrime][intervention->getId()]/maxProbabilityCut;

						// cout<<"Time "<<t<<" Cut "<< this->counterCutsperTimestep[t] << " t prime "<< tPrime <<" Intervention " << intervention->getId() <<" Value "<< this->allNewVICoefficients[t][this->counterCutsperTimestep[t]][intervention->getId()][tPrime] <<endl;

					}

				}

				string str = "betterVI_" +to_string(t) +"_"+to_string(this->internalCounter);
				this->model->addConstr(constraint <= 0, str);
				this->internalCounter++;
				this->counterCutsperTimestep[t]++;

			}

		}

	}

}



void MilpModel::initializeNextMILP_adaptive(bool averageClustering, bool problemChange) {


	vector<size_t> tempTimeSteps;

	if (problemChange){
		tempTimeSteps = vector<size_t> (this->instance->getTimeHorizon());
		iota (begin(tempTimeSteps), end(tempTimeSteps), 0);
	}
	else {
		tempTimeSteps = this->reclusteredTimeSteps;
	}


	this->removeConstraints_scenarioDependent(this->qConstraints,tempTimeSteps);
	this->removeConstraints_timeDependent(this->yConstraints,this->reclusteredTimeSteps);

	//Adapt reduction
	this->instance->adaptiveReduction(this->realRisksPerTimestepPerScenario,this->scenarioRankingPerTimestep,this->risksPerTimestepPerCluster,this->reclusteredTimeSteps);
	this->instance->reclusterUsingNewSplitting(tempTimeSteps,averageClustering);
	this->instance->computeNewVICoefficientReducedScenarioSet(this->reclusteredTimeSteps,this->instance->getNewClusters());

	//Update old variables
	this->updateVariables_y_adaptive();

	//Add ne constraints due to reclutering and adapt objective
	this->addConstraint_QDefinition(this->qConstraints,tempTimeSteps);
	this->addConstraint_yDefinition(this->yConstraints,this->reclusteredTimeSteps);
	this->addConstraintVI_adaptive(this->reclusteredTimeSteps,this->instance->getNewClusters());
	this->addObjective();

}

double MilpModel::getClusteringTotalError() {

	return this->totalError;

}

//////////////////////////GET-SET ATTRIBUTES/////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////


////////////////////////////////GET/////////////////////////////////////////////////////

bool MilpModel::getIsSolutionFeasible() {

	return this->isSolutionFeasible;

}

string MilpModel::getGurobiStatus() {

	return this->gurobiStatus;

}


double MilpModel::getFirstTermObjective() {

	return this->firstTermObjective;

}

double MilpModel::getSecondTermObjective() {

	return this->secondTermObjective;

}

double MilpModel::getModelObjectiveValue() {

	return this->modelObjVal;

}

double MilpModel::getModelObjectiveBound() {

	return this->modelObjBound;

}

double MilpModel::getModelMipGap() {

	return this->modelMipGap;

}

double MilpModel::getModelBuildTime() {

	return this->modelBuildTime;

}

double MilpModel::getModelComputationTime() {

	return this->modelComputationTime;

}

double MilpModel::getMaxObjectiveValueRecoveryTime() {

	return this->maxObjectiveValueRecoveryTime;

}

double MilpModel::getRecoveredObjectiveValue() {

	return this->recoveredObjectiveValue;

}

int MilpModel::getExclusionCallbackLazyCuts() {

	return this->exclusionCallbackLazyCuts;

}

double MilpModel::getExclusionsCallbackLazyCutsTime() {

	return this->exclusionsCallbackLazyCutsTime;

}

////////////////////////////////SET/////////////////////////////////////////////////////
