#pragma once
#include "Common.h"
#include "Instance.h"
#include "CutCallback.h"
#include "CutCallback2.h"

//Gurobi status
//https://www.gurobi.com/documentation/9.0/refman/optimization_status_codes.html#sec:StatusCodes

class MilpModel {

	//ATTRIBUTES
	int internalCounter;
	vector<int> counterCutsperTimestep;

	//Model variables
	Instance* instance;
	CutCallback* cutCallback = NULL;
	CutCallback2* cutCallback2 = NULL;
	GRBEnv* environment;
	GRBModel* model;

	//Model parameters
	bool isIntegralityRelaxedX;
	bool isIntegralityRelaxedY;
	bool isExclusionConstraintsRelaxed;
	bool isQuantileRelaxed;
	bool branchingPriorityX;
	bool fixThreads;
	int numThreads;

	//Model variables
	vector<unordered_map<int, GRBVar>> x;
	vector<unordered_map<int, GRBVar>> y;
	vector<GRBVar> Q;
	vector<GRBVar> eps;

	//Constraints variables
	vector<unordered_map<int, GRBConstr>> qConstraints;
	unordered_map<int, GRBConstr> yConstraints;
	unordered_map<int, GRBConstr> epsConstraints;

	//Cuts
	int exclusionCallbackLazyCuts = 0;
	double exclusionsCallbackLazyCutsTime = 0;

	//Objective computation variables
	double firstTermObjective = -1;
	double secondTermObjective = -1;
	vector<vector<int>> xVariableValues;
	double maxObjectiveValueRecoveryTime = 0.0;
	double recoveredObjectiveValue = - 1;

	//Solver variables
	bool isSolutionFeasible = true;
	string gurobiStatus;
	double modelObjVal = -1;
	double modelObjBound = -1;
	double modelMipGap = -1;
	double modelBuildTime = 0.0;
	double modelComputationTime = 0.0;

   	//Adaptive optimization
	vector<double> realQuantilesPerTimeInstant;
	vector<double> estimatedQuantilesPerTimeInstant;
	vector<double> errorEstimators;
	double totalError;
	vector<size_t> reclusteredTimeSteps;
	vector<vector<double>> realRisksPerTimestepPerScenario;
	vector<vector<double>> risksPerTimestepPerCluster;
	vector<vector<size_t>> scenarioRankingPerTimestep;
	vector<unordered_map<int, unordered_map<int, unordered_map<int, double>>>> allNewVICoefficients;

	//Alternating direction method
	vector<unordered_map<int, GRBConstr>> xVariableFixing;
	vector<unordered_map<int, GRBConstr>> yVariableFixing;
	vector<unordered_map<int, GRBVar>> kappa;
	unordered_map<int, GRBConstr> validInequality1FirstMILP_ADM;
	vector<unordered_map<int, GRBConstr>> validInequality2FirstMILP_ADM;


public:

	////CONSTRUCTORS AND DESTRUCTORS////
	MilpModel(Instance* instance,  bool isIntegralityRelaxedX, bool isIntegralityRelaxedY, bool isExclusionConstraintsRelaxed, bool isQuantileRelaxed, bool branchingPriorityX, bool fixThreads, int numThreads);
	MilpModel(const MilpModel& milpModel);
	~MilpModel();

	////METHODS////

	//General initialization function
	void initializeAll();

	//Initilize MILP model
	void initializeGeneralMILP();
	void initializeFirstMILP_noQuantile();
	void initializeFirstMILP_Adaptive(bool usingADM, bool averageClustering);
	void initializeFirstMILP_ADM(bool viInFirstMILP);

	//Adding and removing variables
	void addVariables_x();
	void addVariables_y();
	void addVariables_Q_eps();
	void removeVariables_scenarioDependent(vector<unordered_map<int, GRBVar>>& variables, vector<size_t>& reclusteredTimeSteps);

	//Objective
	void addObjective();

	//Adding and removing the model constraints
	void addConstraint_allInterventionsScheduled();
	void addConstraint_resourceBounds();
	void addConstraint_exclusions();
	void addConstraint_QDefinition(vector<unordered_map<int, GRBConstr>>& qConstraints, vector<size_t>& reclusteredTimeSteps);
	void addConstraint_yDefinition(unordered_map<int, GRBConstr>& yConstraints, vector<size_t>& reclusteredTimeSteps);
	void addConstraint_epsDefinition(unordered_map<int, GRBConstr>& epsConstraints, vector<size_t>& reclusteredTimeSteps);
	void removeConstraints_timeDependent(unordered_map<int, GRBConstr>& constraints, vector<size_t>& reclusteredTimeSteps);
	void removeConstraints_scenarioDependent(vector<unordered_map<int, GRBConstr>>& constraints, vector<size_t>& reclusteredTimeSteps);

	//Fixing variables
	void addConstraints_yVariableFixing(vector<unordered_map<size_t, bool>>& yValues);
	void removeConstraints_yVariableFixing();

	//Warmstarting
	void warmStartXVariable(vector<unordered_map<int, double>>& xValues);
	void warmStartYVariable(vector<unordered_map<size_t, bool>>& yValues);
	void computeWarmStartYWithX(vector<unordered_map<int, double>>& xValues, vector<unordered_map<size_t, bool>>& yValues, bool newX);
	void computeFullYWithX(vector<unordered_map<int, double>>& theXValues, vector<unordered_map<size_t, bool>>& yValues);

	//Callback
	void initializeCutCallback(bool useOldVI,bool forceRecomputation = false);
	void removeCallback();

	//Solve, write and extract attributes of the model
	void solveModel(double specialTimeLimit = 800, double optimalityGap = 0.01);
	void writeModel(string name);
	void extractSolutionAttributes();

	//Get variable values, check feasibility and print solution to file
	void getXValues(vector<unordered_map<int, double>>& xValues);
	void getYValues(vector<unordered_map<int, double>>& yValues);
	void getYValues(vector<unordered_map<size_t, bool>>& yValues);
	void getQValues(vector<double>& QValues);
	void getEpsValues(vector<double>& epsValues);
	void getQEpsValues(vector<double>& QValues, vector<double>& epsValues);
	void solutionFeasibilityCheck();
	void solutionPrinting();
	void solutionPrinting(vector<unordered_map<int, double>>& xValues, vector<unordered_map<size_t, bool>>& yValues, string outputFile);

	//Recover objective given a solution and compute error estimators if the model is reduced
	double objectiveValueRecovery(vector<unordered_map<int, double>>& xValues);

	//ADM functions
	void initializeSecondMILP_ADM(bool viInFirstMILP);
	void ySolving_ADM(vector<unordered_map<int, double>>& xValues, vector<unordered_map<size_t, bool>>& yValues, double& objectiveValue);
	void ySolving_ADM_Adaptive(vector<unordered_map<int, double>>& xValues, vector<unordered_map<size_t, bool>>& yValues, double& objectiveValue);
	void initializeYVariableFixing_ADM();
	void initializeXVariableFixing_ADM();

 	//Adaptive optimization
	void computeClusterRisk_adaptive(vector<unordered_map<int, double>>& xValues);
	void computeErrorEstimators_adaptive();
	void computeImportantTimesteps_adaptive(double criteria);
	void updateVariables_y_adaptive();
	void addConstraintVI_adaptive(vector<size_t>& reclusteredTimeSteps, unordered_map<size_t,vector<size_t>>& newClusters);
	void initializeNextMILP_adaptive(bool averageClustering, bool problemChange);
	double getClusteringTotalError();

	////GET-SET ATTRIBUTES////
	bool getIsSolutionFeasible();
	string getGurobiStatus();
	double getFirstTermObjective();
	double getSecondTermObjective();
	double getModelObjectiveBound();
	double getModelObjectiveValue();
	double getModelMipGap();
	double getModelBuildTime();
	double getModelComputationTime();
	double getMaxObjectiveValueRecoveryTime();
	double getRecoveredObjectiveValue();
	int getExclusionCallbackLazyCuts();
	double getExclusionsCallbackLazyCutsTime();
};
