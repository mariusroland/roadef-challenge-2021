#pragma once

#include "Common.h"

using namespace std;

class Intervention {

private:

	//ATTRIBUTES
	/* id, name, maximum time, feasibleStartingTimes (set H(i)), processing times (one for each starting time), 
	inProcess (one for each time instant, set H(i,t))*/
	int id;
	string name;
	int maximumTime;
	vector<int> feasibleStartingTimes;
	vector<double> processingTimes;
	vector<vector<int>> inProcess;

public:

	//CONSTRUCTORS AND DESTRUCTORS
	Intervention(int id, string name, int maximumTime, vector<double> processingTimes);
	Intervention(const Intervention& intervention);
	~Intervention();

	//METHODS
	string toString();

	//GET-SET ATTRIBUTES
	int getId();
	string getName();
	int getMaximumTime();
	vector<int>& getFeasibleStartingTimes();
	vector<double>& getProcessingTimes();
	vector<vector<int>>& getInProcess();

};


