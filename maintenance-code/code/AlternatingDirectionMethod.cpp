#include "AlternatingDirectionMethod.h"

///////////////////////CONSTRUCTORS AND DESTRUCTORS//////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////

AlternatingDirectionMethod::AlternatingDirectionMethod(Instance* instance, double ADMTimeLimit, double ADMPercentage, double givenTimeLimit, bool viInFirstMILP, bool useCuts, bool useOldVI, bool branchingPriorityX, bool fixThreads, int numThreads) {

	//Model
	this->instance = instance;
	this->milpModel = new MilpModel(this->instance, false, false, false, false, branchingPriorityX, fixThreads, numThreads);

	//Time
	this->startTime = this->instance->getStartTimeInitialization();
	this->ADMTimeLimit = ADMTimeLimit;
	this->givenTimeLimit = givenTimeLimit;

	//Percentage
	this->ADMPercentage = ADMPercentage;

	//Solution
	this->bestXSolution = vector<unordered_map<int, double>> (this->instance->getInterventions().size());
	this->admObjectiveValue = maxConstant;
	this->bestObjectiveValue = maxConstant;
	this->bestObjectiveBound = 0.0;
	this->enteredImprovement = false;

	//Computational parameters
	this->viInFirstMILP = viInFirstMILP;
	this->useCuts = useCuts;
	this->useOldVI = useOldVI;

}

AlternatingDirectionMethod::AlternatingDirectionMethod(const AlternatingDirectionMethod& alternatingDirectionMethod)  {

	this->instance = alternatingDirectionMethod.instance;
	this->milpModel = alternatingDirectionMethod.milpModel;

}

AlternatingDirectionMethod::~AlternatingDirectionMethod() {
	delete this->milpModel;
}

//////////////////////////////METHODS///////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////
////////////////////////ALTERNATING DIRECTION METHOD////////////////////////////////////

void AlternatingDirectionMethod::solve() {

		//Best solution saving
		vector<unordered_map<int, double>> xValues(this->instance->getInterventions().size());
		vector<unordered_map<size_t, bool>> yValues(this->instance->getTimeHorizon(),unordered_map<size_t, bool> {});
		double currentObjectiveValue;

		//Initialization: solve the model without the quantile.
		this->milpModel->initializeFirstMILP_ADM(viInFirstMILP);

		//StartADMTimer
		chrono::time_point<chrono::system_clock> start1,end1;
		start1 = chrono::system_clock::now();
		this->ADMStartTime = start1;

		cout << "FIRST ONLY EXPECTATION VERSION" << "\n";
		this->milpModel->solveModel(this->availableComputationTime(true),1e-2);

		//Save x values
		this->milpModel->getXValues(xValues);
		this->bestXSolutionADM = xValues;
		this->bestXSolution = xValues;
		this->milpModel->initializeSecondMILP_ADM(viInFirstMILP);


		int admIterationCounter = 1;
		bool isEnd = false;
		bool endedInXSolving = true;
		double percentage = 1.0;

		//One iteration per variable type
		while (!isEnd) {

			//If the iteration count is uneven
			//Do y solving
			if (admIterationCounter % 2) {
				cout << endl << "Y SOLVING" << endl;

				if (admIterationCounter > 1){
					//Remove remaining constraints
					this->milpModel->removeConstraints_yVariableFixing();
				}

				//Remove y cons and add x fixing
				this->milpModel->ySolving_ADM(xValues,yValues,currentObjectiveValue);

				//Saving the solution of the computation
				this->bestXSolutionADM = xValues;
				this->bestXSolution = xValues;
				this->bestYSolutionADM = yValues;
				this->bestYSolution = yValues;

				//Saving in X or Y solving
				endedInXSolving = false;

			}

			//Otherwise, if the iteration count is even
			//Do x solving
			else {
				cout << endl << "X SOLVING" << endl;

				// //Remove x cons and add y fixing
				this->milpModel->warmStartYVariable(yValues);
				this->milpModel->warmStartXVariable(xValues);
				this->milpModel->addConstraints_yVariableFixing(yValues);

				//Solve with reference param and time limit
				this->milpModel->solveModel(this->availableComputationTime(true),this->requiredOptimalityGap(admIterationCounter));

				//Get objective value and xValues
				this->milpModel->getXValues(xValues);
				this->milpModel->extractSolutionAttributes();
				currentObjectiveValue = this->milpModel->getModelObjectiveValue();

				//Saving the solution of the computation
				this->bestXSolutionADM = xValues;
				this->bestXSolution = xValues;
				this->bestYSolutionADM = yValues;
				this->bestYSolution = yValues;

				//Saving in X or Y solving
				endedInXSolving = true;

			}

			percentage = (this->bestObjectiveValue - currentObjectiveValue)/this->bestObjectiveValue;

			//If the solution has been improved save otherwise stop
			if (percentage > epsilonValue) {

				this->bestObjectiveValue = currentObjectiveValue;

			}
			else{

				isEnd = true;
			}

			//If more than two iterations stop
			if (admIterationCounter > 2) {

				isEnd = true;

			}

			admIterationCounter++;

		}

		//Computing ADMTime and save objective value
		this->admObjectiveValue = this->bestObjectiveValue;
		end1 = chrono::system_clock::now();

		this->realADMTime = (chrono::duration_cast<chrono::nanoseconds>	(end1 - start1).count())*1e-9;

		if (endedInXSolving){
			//Remove remaining constraints
			this->milpModel->removeConstraints_yVariableFixing();
		}

		start1 = end1;
		if (this->availableComputationTime(false) > 0){
			if (this->useCuts){
				//Setting callback for cuts
				this->milpModel->initializeCutCallback(this->useOldVI);
			}
			start1 = end1;
			std::cout << "IMPROVEMENT PHASE" << "\n";

			//WarmStarting
			this->milpModel->warmStartXVariable(xValues);
			this->milpModel->warmStartYVariable(yValues);


		}
		if (this->availableComputationTime(false) > 0){

			//Setting params and solving
			this->milpModel->solveModel(this->availableComputationTime(false),0.0);
			//Save best solution and objective value
			this->milpModel->getXValues(xValues);
			this->milpModel->getYValues(yValues);
			this->milpModel->extractSolutionAttributes();
			this->bestObjectiveValue = this->milpModel->getModelObjectiveValue();
			this->enteredImprovement = true;

			//Saving the solution of the computation
			this->bestXSolution = xValues;
			this->bestYSolution = yValues;
		}

		end1 = chrono::system_clock::now();
		this->improvementPhaseTime = (chrono::duration_cast<chrono::nanoseconds>	(end1 - start1).count())*1e-9;



}

double AlternatingDirectionMethod::availableComputationTime(bool inLoop){

	chrono::time_point<chrono::system_clock> currentTime = chrono::system_clock::now();
	if (inLoop){

		return min(this->givenTimeLimit - (chrono::duration_cast<chrono::seconds>	(currentTime - this->startTime).count()), ADMTimeLimit - (chrono::duration_cast<chrono::nanoseconds>	(currentTime - this->ADMStartTime).count())*1e-9);

	}
	else{

		return (this->givenTimeLimit - (chrono::duration_cast<chrono::nanoseconds>	(currentTime - this->startTime).count())*1e-9);

	}

}

double AlternatingDirectionMethod::requiredOptimalityGap(int iteration){

	if (iteration % 2) {
		//Y iteration
		return 0.0;
	}
	else{
		//X iteration
		return 5e-2;
	}

}

//////////////////////////GET-SET ATTRIBUTES/////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////GET/////////////////////////////////////////////////////

double AlternatingDirectionMethod::roundNumber(double value){
  return round(value * 100.0)/100.0;
}

void AlternatingDirectionMethod::writeStatistics(string fileNumber, string fileName){

	//Printing ADM solution files if available
	if (this->admObjectiveValue < maxConstant - epsilonValue){
		this->milpModel->solutionPrinting(this->bestXSolutionADM,this->bestYSolutionADM,fileName+"-solution-oadm");
	}
	//Printing MILP^OADM solution files if available
	if (this->bestObjectiveValue < maxConstant - epsilonValue){
		this->milpModel->solutionPrinting(this->bestXSolution,this->bestYSolution,fileName+"-solution");
	}

	ofstream mystream;
	mystream.open(fileName, ofstream::out | ofstream::trunc);
	mystream << fileNumber << ",";
	mystream << this->ADMTimeLimit << ",";
	mystream << this->requiredOptimalityGap(1) << ",";
	mystream << this->requiredOptimalityGap(2) << ",";
	mystream << this->roundNumber(this->givenTimeLimit) << ",";
	mystream << fixed << setprecision(2);
	mystream << this->instance->getReadTime() << ",";
	mystream << this->milpModel->getModelBuildTime() << ",";
	mystream << this->instance->getBigMComputationTime() << ",";

	if (this->roundNumber(this->realADMTime) > 0){
		mystream << this->realADMTime << ",";
	}
	else {
		mystream << "-" << ",";
	}
	if (this->improvementPhaseTime > 0){
		mystream << this->improvementPhaseTime << ",";
	}
	else {
		mystream << "-" << ",";
	}
	if (this->admObjectiveValue < maxConstant - epsilonValue){
		mystream << this->admObjectiveValue << ",";

	}
	else{
		mystream << "-" << ",";
	}
	if (this->bestObjectiveValue < maxConstant - epsilonValue){
		mystream << this->bestObjectiveValue << ",";
	}
	else{
		mystream << "-" << ",";
	}

	bool boundAvailable = false;

	if (this->enteredImprovement){

		if (this->milpModel->getModelObjectiveBound() > 0){
			boundAvailable = true;
			this->bestObjectiveBound = this->milpModel->getModelObjectiveBound();
			mystream << this->bestObjectiveBound << ",";
		}
		else{
			mystream << "-" << ",";
		}

		if (boundAvailable){
			mystream << ((this->bestObjectiveValue - this->bestObjectiveBound)/this->bestObjectiveValue*100.0);
		}
		else{
			mystream << "-";
		}

	}
	else {
		mystream << "-" << ","<< "-";
	}

	mystream << "\n";

	mystream.close();

}

vector<unordered_map<int, double>> AlternatingDirectionMethod::getBestX(){
	this->milpModel->getXValues(this->bestXSolution);
	return this->bestXSolution;
}

double AlternatingDirectionMethod::getBestObjectiveValue(){
	return this->milpModel->objectiveValueRecovery(this->bestXSolution);
}
