#pragma once

#include "Common.h"

using namespace std;

class Risk {

private:

	//ATTRIBUTES
	/*intervention id, time instant, starting time, scenario id, value */
	int interventionId;
	int timeInstant;
	int startingTime;
	int scenarioId;
	double value;

public:

	//CONSTRUCTORS AND DESTRUCTORS
	Risk(int interventionId, int timeInstant, int startingTime, int scenarioId, double value);
	Risk(const Risk& risk);
	~Risk();

	//METHODS
	string toString();

	//GET-SET ATTRIBUTES
	int getInterventionId();
	int getTimeInstant();
	int getStartingTime();
	int getScenarioId();
	double getValue();

};


