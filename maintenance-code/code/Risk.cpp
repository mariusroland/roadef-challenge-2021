#include "Risk.h"

///////////////////////CONSTRUCTORS AND DESTRUCTORS//////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////

Risk::Risk(int interventionId, int timeInstant, int startingTime, int scenarioId, double value) {

	this->interventionId = interventionId;
	this->timeInstant = timeInstant;
	this->startingTime = startingTime;
	this->scenarioId = scenarioId;
	this->value = value;

}

Risk::Risk(const Risk& risk) {

	this->interventionId = risk.interventionId;
	this->timeInstant = risk.timeInstant;
	this->startingTime = risk.startingTime;
	this->scenarioId = risk.scenarioId;
	this->value = risk.value;

}

Risk::~Risk() {

}

///////////////////////////METHODS//////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////toString////////////////////////////////////////////////////

string Risk::toString() {

	string str;

	str.append("\tintervention: ");
	str.append(to_string(interventionId));

	str.append("\ttime instant: ");
	str.append(to_string(timeInstant));

	str.append("\t starting time: ");
	str.append(to_string(startingTime));

	str.append("\t scenarioId: ");
	str.append(to_string(scenarioId));

	str.append("\t value: ");
	str.append(to_string(value));

	return str;

}

////////////////////////////GET-SET ATTRIBUTES//////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////GET///////////////////////////////////////////////////

int Risk::getInterventionId() {

	return this->interventionId;

}

int Risk::getTimeInstant() {

	return this->timeInstant;

}

int Risk::getStartingTime() {

	return this->startingTime;

}

int Risk::getScenarioId() {

	return this->scenarioId;

}

double Risk::getValue() {

	return this->value;

}