#include "Common.h"
#include "Instance.h"
#include "gurobi_c++.h"
#include "MilpModel.h"
#include "AdaptiveAlgorithm.h"
#include "ModelSolver.h"
#include "AlternatingDirectionMethod.h"

using namespace std;

int main(int argc, char* argv[]) {

	//RUN INFORMATION//

	// First check for strange stuff
	assert(argc == 5);

	// Extract parameters
	string path = argv[1];
	string instanceFile = argv[2];
	int methodCounter = atoi(argv[3]);
	string outputFolder = argv[4];

	// Second check to see if method counter is correct
	assert(methodCounter > 0);
	assert(methodCounter < 6);

	cout<<"Solving "<< instanceFile << " with ";

	//INITIALIZING PARAMETERS//

	//Alternating direction method
	bool isAlternatingDirectionMethod = false;
	bool viInFirstMILPADM = false;
	double ADMTimeLimit = 1800.0; //1800
	double ADMPercentage = 0.0;

	//Reduction
	bool isModelReduced = false;
	double reductionPercentage = 0.25;

	//Adaptive clustering
	bool isAdaptiveAlgorithm = false;
	bool usingADMForAdaptive = false;
	double epsilonUBLB = 0.0001;

	//Solver parameters
	bool useCuts = false;
	bool useOldVI = false;
	bool branchingPriorityX = false;
	bool computeBetterBigM = false;

	//Testing setup
	bool fixThreads = true;
	int numThreads = 4;
	double timeLimit = 5400.0;


	//CREATING SETUP USING USER INPUT//

	string outputFile = outputFolder+instanceFile;
	if (methodCounter == 1) {
		cout<<"MILP"<<endl<<endl;
		outputFile = outputFile + "-milp";
	}
	if (methodCounter == 2) {
		cout<<"MILP_VI"<<endl<<endl;
		outputFile = outputFile + "-milp-vi";
		useCuts = true;
	}
	if (methodCounter == 3) {
		cout<<"MILP_VI^OADM"<<endl<<endl;
		outputFile = outputFile + "-milp-vi-oadm";
		isAlternatingDirectionMethod = true;
		useCuts = true;
	}
	if (methodCounter == 4) {
		cout<<"ASCA"<<endl<<endl;
		outputFile = outputFile + "-asca";
		isAdaptiveAlgorithm = true;
		usingADMForAdaptive = true;
	}


	//RUN THE SCRIPT//

	//Read Instance and start timer after reading//
	Instance instance(path, instanceFile, isAdaptiveAlgorithm, computeBetterBigM, isModelReduced, reductionPercentage, fixThreads, numThreads);
	instance.readFile();

	//Run the problem depending on the input
	if (isAdaptiveAlgorithm){

		AdaptiveAlgorithm adaptiveAlgorithm (&instance,timeLimit,epsilonUBLB,usingADMForAdaptive,useCuts,useOldVI,branchingPriorityX,fixThreads, numThreads);
		try {
			adaptiveAlgorithm.solve();
		}
		catch (GRBException& e) {
			cerr << "Concert exception caught: " << e.getErrorCode() << endl;
			cerr << e.getMessage() << endl;
		}
		catch (...) {
			cerr << "Unknown exception caught" << endl;
		}
		adaptiveAlgorithm.writeStatistics(instanceFile,outputFile);

	}
	else if (isAlternatingDirectionMethod){

		AlternatingDirectionMethod alternatingDirectionMethod(&instance,ADMTimeLimit,ADMPercentage,timeLimit,viInFirstMILPADM,useCuts,useOldVI,branchingPriorityX,fixThreads,numThreads);
		try {
			alternatingDirectionMethod.solve();
		}
		catch (GRBException& e) {
			cerr << "Concert exception caught: " << e.getErrorCode() << endl;
			cerr << e.getMessage() << endl;
		}
		catch (...) {
			cerr << "Unknown exception caught" << endl;
		}
		alternatingDirectionMethod.writeStatistics(instanceFile,outputFile);


	}
	else {

		ModelSolver modelSolver (&instance,timeLimit,useCuts,useOldVI,branchingPriorityX,fixThreads,numThreads);
		modelSolver.solve();
		modelSolver.writeStatistics(instanceFile,outputFile);


	}


	return 0;

}
