# Exact and Heuristic Solution Techniques for Mixed-Integer Quantile Minimization Problems

This repository contains code for solving two different models
discussed in the paper: the Portfolio Optimization Problem and the
Maintenance Planning Problem.

## Portfolio Optimization

### Running the Code

All code files are located in the *portfolio-code/code* folder. To run
the code apply the following instructions:

1. Navigate to the *portfolio-code/code* folder.
2. Edit the `Makefile` to match your system configuration.
3. Adjust `main.cpp` according to your testing requirements.
4. Build the executable by running `make clean; make`.
5. Execute the program using `./exec`.
6. The results will be printed to the output file specified in
   `main.cpp`.

### Makefile Configuration

To configure the `Makefile`:

1. Update the `LIBFLAGS` variable to point to your Gurobi library
   flags.
2. Adjust the `GRBPATH` variable to point to your Gurobi installation
   directory.

### Instance Files ###

All instances files are located in the *portfolio-code/src*
folder. Two instance types are tested, synthetic instances and
realistic instances. The synthetic instance names all start with the
word *Wolsey* whereas the realistic instances are called *DWJA,
EuroStoxx50, FTSE100, SP500*. The instances are structured in the
following way:

1. The first line contains: nb. of assets, nb. of scenarios, $\rho$
   (if not there it is set to 1.1)
2. The other lines contain the return of one asset for each scenario.


<!-- ### main.cpp Configuration -->

<!-- To configure `main.cpp`: -->

<!-- 1. Set the `path` variable to the exact path of the instance -->
<!--    variables. -->
<!-- 2. Use the `isRealData` flag to choose between realistic data -->
<!--    instances (`true`) or synthetic instances (`false`). -->
<!-- 3. Define the `nameArray` to store instance names. -->
<!-- 4. Specify the `timeLimit` variable to set the time limit in seconds. -->
<!-- 5. Populate the `alphas` array with different values for the parameter -->
<!--    alpha. -->
<!-- 6. Populate the `tau` array with different values for the parameter -->
<!--    tau. -->
<!-- 7. Use the `isAdaptiveAlgorithm` flag to choose between ASCA (`true`) -->
<!--    and generic MILP (`false`). -->
<!-- 8. Use the `areWeakVI` and `areStrongVI` flag arrays to indicate the -->
<!--    use of weaker valid inequalities (Eq. (10) in [1]) or stronger -->
<!--    valid inequalities (Eq. (15) in [1]). -->
<!-- 9. Specify the `outputFileName` to set the path and name of the output -->
<!--    file containing computational information. -->

### Output Files

Different output files are generated depending on whether ASCA or the
MILP method is used:

1. ASCA: Each row corresponds to a single instance, alpha, and
   tau combination. Columns contain instance name, number of
   assets, nb. of scenarios, alpha, tau, v<sup>LB</sup>,
   v<sup>UB</sup>,
   gap (%).
2. MILP: Each row corresponds to a single instance, alpha, tau,
   isWeakVI or isStrongVI combination. Columns contain instance name,
   nb. of assets, nb. of scenarios, alpha, tau, isWeakVI (0 if false and
   1 otherwise), isStrongVI (0 if false and 1 otherwise), v<sup>LB</sup>,
   v<sup>UB</sup>, gap (%).

## Maintenance Planning

### Running the Code

All code files are located in the *maintenance-code/code* folder. To run
the code apply the following instructions:

1. Navigate to the *maintenance-code/code* folder.
2. Edit the `Makefile` to match your system configuration.
3. Adjust `main.cpp` according to your testing requirements.
4. Build the executable by running `make clean; make`.
5. Execute the program using `./exec your-input-path
   your-instance-file your-selected-method your-output-folder`.<br>
   Example: `./exec ../src/ A08 1 ./results/ `
6. Every integer inside the range 1 to 4 is associated to a specific
   method. In order we have, MILP:1, MILP<sub>VI</sub>:2,
   MILP<sub>VI</sub><sup>OADM</sup>:3, ASCA:4.

<!-- ### main.cpp Configuration -->

<!-- Here, the `main.cpp` file has a similar configuration to the -->
<!--    `main.cpp` file inside of the portfolio problem. We do not provide -->
<!--    details on every parameter  -->


### Makefile Configuration

To configure the `Makefile`:

1. Update the `LIBFLAGS` variable to point to your Gurobi library
   flags.
2. Adjust the `GRBPATH` variable to point to your Gurobi installation
   directory.

### Instance Files

All instance files are located in the *scheduling-code/src*
folder. All instance were created for the [Roadef Challenge
2020](https://www.roadef.org/challenge/2020/en/). Details about the
instance structure is available in this
[document](https://github.com/rte-france/challenge-roadef-2020/raw/master/Challenge_Rules.pdf).

### Output Files

Different output files are generated depending on whether MILP,
MILP<sub>VI</sub>,
MILP<sub>VI</sub><sup>OADM</sup>, ASCA is used. Exactly one output file
is created for each instance/method combination. The name of the
output file is the concatenation of the instance name and the
method. Example: `A08-asca`. The
output files are structured in the following way:

1. MILP, MILP<sub>VI</sub>,
   MILP<sub>VI</sub><sup>OADM</sup>: instance name, requested
   gap, time limit, read time, model build time, big M computation
   time, gurobi solver time, v<sup>LB</sup>, v<sup>UB</sup>, gap (%).
2. ASCA: instance name, requested
   gap x direction OADM, requested gap z direction OADM,
   requested gap,  time limit, read time, model
   build time, big M computation time, ASCA time, nb. of ASC
   iterations, nb. of MSC
   iterations, v<sup>LB</sup>, v<sup>UB</sup>, gap (%).

In addition, the solutions x<sup>LB</sup> and x<sup>UB</sup> are
printed to a file. The name of the
solution file is the concatenation of the instance name, the
method, the word *solution*, and *lb* or *ub* if the chosen method
computes a lower and an upper bound solution. Example:
`A08-asca-solution-lb` and `A08-asca-solution-ub`. The
structure of the solution files is explained in the file
itself. Therefore we refrain to explain the solution file structure.


## References

[1]: Diego Cattaruzza, Martine Labbé, Matteo Petris, Marius Roland, Martin Schmidt (2023). Exact and Heuristic Solution Techniques for Mixed-Integer Quantile Minimization Problems. *Informs Journal On Computing*. [Link to the Preprint](https://optimization-online.org/2021/11/8673/)


## License

This project is licensed under the [MIT License](https://opensource.org/license/mit/) - see the [LICENSE](LICENSE) file for details.

